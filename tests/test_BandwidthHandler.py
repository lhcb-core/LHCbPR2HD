import testHandlers


def test_BandwidthHandler():
    testHandlers.main([
    "--results=tests/data/output_lhcb-master.1971_Moore_hlt2_bandwidth",
    "--list-handlers=BandwidthTestHandler",
    "--app-name=Moore",
    "--app-version=lhcb-master.1971",
    "--opt=Moore_hlt2_bandwidth",
    ])