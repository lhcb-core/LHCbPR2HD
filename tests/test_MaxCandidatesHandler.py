import testHandlers


def test_MaxCandidatesHandler():
    testHandlers.main([
        "--results=tests/data/output.Moore_max_candidates_n100000",
        "--list-handlers=MaxCandidatesHandler",
        "--app-name=Moore",
        "--app-version=lhcb-2024-patches.103",
        "--opt=Moore_max_candidates_test",
    ])
