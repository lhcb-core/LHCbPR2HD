from handlers.utils import lhcbpr


def test_get_job_ids():
    ids = lhcbpr.get_job_ids(
        "lhcb-master-ref", 1033, "Moore", "Moore_hlt2_reco_baseline"
    )
    assert ids == [154200]

    ids = lhcbpr.get_job_ids(
        "lhcb-master-ref", 103, "Moore", "Moore_hlt2_reco_baseline"
    )
    assert ids == []
