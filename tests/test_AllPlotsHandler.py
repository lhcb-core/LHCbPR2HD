import os
import unittest
import subprocess as sp
import tempfile
import json

import ROOT
from handlers.AllPlotsHandler import AllPlotsHandler, all_plots_in_files, sanitize

class AllPlotsHandlerTestCase(unittest.TestCase):
    def test_all_plots_in_files_return_None(self):
        with tempfile.NamedTemporaryFile(suffix=".root") as f:
            try:
                root_file = ROOT.TFile.Open(f.name, "recreate")
                root_file.cd()
                h = ROOT.TH1D("h1", "h1", 100, -3, 3)
                h.FillRandom("gaus", 1000)
                h.Write()
                newdir = root_file.mkdir("newdir")
                newdir.cd()
                h = ROOT.TH1D("h2", "h2", 100, -3, 3)
                h.FillRandom("gaus", 1000)
                h.Write()
                root_file.Write()
            finally:
                if root_file:
                    root_file.Close()

            for i in all_plots_in_files([f.name]):
                self.assertFalse(i[1].GetName() == "foobar")
                assert \
                    (i[1].GetName() == "h1"
                     and i[0] == "_TFile_"+sanitize(f.name)+"_TH1D_h1") or\
                    (i[1].GetName() == "h2"
                     and i[0] == "_TFile_"+sanitize(f.name)+"_TDirectoryFile_newdir_TH1D_h2"),\
                     "Got {}".format(i)


    def test_AllPlotsHandler_default_return_None(self):
        with tempfile.NamedTemporaryFile(dir=".", suffix=".root") as f:
            try:
                root_file = ROOT.TFile.Open(f.name, "recreate")
                root_file.cd()
                h = ROOT.TH1D("h1", "h1", 100, -3, 3)
                h.FillRandom("gaus", 1000)
                h.Write()
                newdir = root_file.mkdir("newdir")
                newdir.cd()
                h = ROOT.TH1D("h2", "h2", 100, -3, 3)
                h.FillRandom("gaus", 1000)
                h.Write()
                root_file.Write()
            finally:
                if root_file:
                    root_file.Close()

            with tempfile.NamedTemporaryFile(dir="handlers", prefix="Test", suffix=".py") as pyfile:
                basename = os.path.basename(pyfile.name)
                test_class_file_contents = """
from .AllPlotsHandler import AllPlotsHandler

class {}(AllPlotsHandler):
    files_to_search = ['{}']

""".format(os.path.splitext(basename)[0], os.path.basename(f.name))

                pyfile.write(test_class_file_contents.encode())
                pyfile.flush() # (To *really* write the file)

                sp.check_call(
                    "python -B ./testHandlers.py -l {}".format(
                        os.path.splitext(basename)[0]), shell=True)

            # The file "json_results" is produced by ./testHandlers.py
            json_results = json.load(open("json_results"))
            jobattr = json_results["JobAttributes"]
            for i in jobattr:
                if i["name"] == "allplots_keys" and i["group"] == "_allplots_keys":
                    break
            else:
                assert False, "Got {}".format(jobattr)


if __name__ == "__main__":
    unittest.main()
