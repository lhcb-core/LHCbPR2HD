import testHandlers


def test_ThroughputProfileHandler():
    testHandlers.main([
    "--results=tests/data/output_lhcb-master-mr_1594_Moore_hlt2_reco_baseline",
    "--list-handlers=ThroughputProfileHandler",
    "--app-name=Moore",
    "--app-version=lhcb-master-mr.1594",
    "--opt=Moore_hlt2_reco_baseline",
    ])
