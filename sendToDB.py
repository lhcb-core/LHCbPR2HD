#!/usr/bin/env python

import sys
import os
import json
import logging
import zipfile
import subprocess
import time
import shutil
from optparse import OptionParser

diracStorageElementName = "StatSE"
diracStorageElementFolder = "/lhcb/prdata/results"
logger = logging.getLogger("sendToDB.py")


def storeBackupOnEOS(zipFile):
    eos_path = os.path.join(
        "/eos/lhcb/storage/lhcbpr/lhcbprdata/zips_backup",
        time.strftime("%Y"),
        time.strftime("%b-%Y"),
    )

    if not os.path.exists(eos_path):
        logger.info(
            "EOS backup path does not exist, creating: {0}".format(eos_path))
        try:
            os.makedirs(eos_path)
        except:
            logger.info("Failed to create: {0}".format(eos_path))

    try:
        shutil.copy(zipFile, eos_path)
    except:
        logger.info("Failed to copy {0} to {1}".format(zipFile, eos_path))


def sendViaDiracStorageElement(zipFile):
    head, tailzipFile = os.path.split(zipFile)
    # from DIRAC.Core.Base.Script import initialize
    # initialize(ignoreErrors=True, enableCommandLine=False)

    # from DIRAC.Resources.Storage.StorageElement import StorageElement
    # statSE = StorageElement(diracStorageElementName)

    # log = statSE.putFile(
    #     {os.path.join(diracStorageElementFolder, tailzipFile): zipFile})
    # logger.info('{0}'.format(log))
    lfn = os.path.join(diracStorageElementFolder, time.strftime("%b-%Y"),
                       tailzipFile)

    try:
        result = subprocess.check_output([
            "dirac-dms-add-file", "-ddd", lfn, zipFile, diracStorageElementName
        ])
        if result.decode().count("Successfully uploaded ") == 1:
            logger.info("Uploaded {0}".format(lfn))
        else:
            logger.error("Fail to upload {0}".format(lfn))
    except subprocess.CalledProcessError as e:
        logger.error("Fail to upload {0}: {1}".format(lfn, e))


def run(zipFile, ssss, delzip):
    ch = logging.StreamHandler()
    ch.setLevel(level=logging.WARNING)
    if os.path.exists("output/collect.log"):
        fh = logging.FileHandler(os.path.join("output", "collect.log"))
        fh.setLevel(level=logging.WARNING)

    if not ssss:
        logging.root.setLevel(logging.INFO)
        ch.setLevel(logging.INFO)
        if fh:
            fh.setLevel(logging.INFO)

    formatter = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    ch.setFormatter(formatter)
    if fh:
        fh.setFormatter(formatter)

    logger.addHandler(ch)
    if fh:
        logger.addHandler(fh)

    if not zipfile.is_zipfile(zipFile):
        logger.error("Given object is not a valid zip file,"
                     " please give a valid one, aborting...")
        return

    # checking if the zip contains what it should contains
    try:
        unzipper = zipfile.ZipFile(zipFile)
        dataDict = json.loads(unzipper.read("json_results"))

        for atr in dataDict["JobAttributes"]:
            if atr["type"] == "File":
                unzipper.read(atr["filename"])

    except Exception as e:
        logger.error(e)
        logger.error("Aborting...")
        return

    logger.info("Given zip file is valid, sending to database...")

    sendViaDiracStorageElement(zipFile)
    storeBackupOnEOS(zipFile)

    # cleanup the results ZIP file once finished processing
    if delzip:
        try:
            logger.info("Deleting {0}".format(zipFile))
            os.remove(zipFile)
        except:
            logger.info("Failed to remove {0}".format(zipFile))

    return


def main():
    # this is used for checking
    needed_options = 3

    description = """The program needs all the input arguments
                  (options in order to run properly)"""

    parser = OptionParser(
        usage="usage: %prog [options]", description=description)
    parser.add_option(
        "-s",
        "--send-results",
        action="store",
        type="string",
        dest="zipFile",
        help="Zip file with results to be pushed to database",
    )
    parser.add_option(
        "-q",
        "--quiet",
        action="store_true",
        dest="ssss",
        default=False,
        help="do not print info from logger, optional",
    )
    parser.add_option(
        "-d",
        "--delete",
        action="store_true",
        dest="delzip",
        default=False,
        help="Delete the ZIP file with results after processing, optional",
    )

    if len(sys.argv) < needed_options:
        parser.parse_args(["--help"])
        return

    options, args = parser.parse_args()

    run(options.zipFile, options.ssss, options.delzip)


if __name__ == "__main__":
    main()
