#!/usr/bin/python

# Script for accessing histograms of reconstructible and
# reconstructed tracks for different tracking categories
# created by PrCheckerEfficiencyPlots_HLT2.py, using
# PrChecker2
#
# The efficency is calculated usig TGraphAsymmErrors
# and Bayesian error bars
#
# author: Mengzhen Wang (mengzhen.wang@cern.ch)
# date:   02/2019
#

import os
import ROOT
from .BaseHandler import BaseHandler
import logging
import subprocess

log = logging.getLogger(__name__)

# from BaseHandler import BaseHandler


def getEfficiencyHistoNames():
    return ["eta", "p", "pt", "phi", "nPV", "docaz"]


def getTrackers():
    return [
        "Velo",
        "Upstream",  # Made by PrChecker2Fast
        "Forward",
        "TTrack",
        "Downstream",
        "Match",
        "Best",
        "BestDown",
        "BestLong",
    ]  # Made by PrChecker2


def getOriginFolders():
    basedict = {
        "Velo": {},
        "Upstream": {},
        "Forward": {},
        "TTrack": {},
        "Downstream": {},
        "Match": {},
        "Best": {},
        "BestDown": {},
        "BestLong": {},
    }

    basedict["Velo"]["folder"] = "VeloMCChecker/"
    basedict["Upstream"]["folder"] = "UpMCChecker/"
    basedict["Forward"]["folder"] = "ForwardMCChecker/"
    basedict["TTrack"]["folder"] = "TMCChecker/"
    basedict["Match"]["folder"] = "MatchMCChecker/"
    basedict["Best"]["folder"] = "BestMCChecker/"
    basedict["Downstream"]["folder"] = "DownMCChecker/"
    basedict["BestDown"]["folder"] = "BestDownMCChecker/"
    basedict["BestLong"]["folder"] = "BestLongMCChecker/"

    return basedict


# def PrCheckerEfficiencyHandler_HLT2(outputname):
class PrCheckerEfficiencyHandler_HLT2(BaseHandler):
    # outputname: the location and name for the output root file, which saves the efficiency plots

    def __init__(self):
        super(self.__class__, self).__init__()

    def collectResultsExt(
            self,
            directory,
            project,
            version,
            platform,
            hostname,
            cpu_info,
            memoryinfo,
            startTime,
            endTime,
            options,
    ):
        # print "directory: ", directory
        # print "project: ", project
        # print "version: ", version
        # print "platform: ", platform
        # print "hostname: ", hostname
        # print "startTime: ", startTime
        # print "endTime: ", endTime
        # print "options: ", options

        # Import LHCb style
        from .utils.LHCbStyle import setLHCbStyle

        # Import configurations, to determine which plots to draw
        from .utils.ConfigHistos_HLT2 import (
            efficiencyHistoDict,  # Include the variable in Xaxis, the X title.
            categoriesDict,  # Category of the tracks, and the corresponding title for the plot
            getCuts,
        )  # Category of tracks. Used to construct the location to find the histogram in PrCheckerPlots_HLT2.root

        # Import the method to draw TLegend in the graph
        from .utils.Legend import place_legend

        setLHCbStyle()

        efficiencyHistoDict = efficiencyHistoDict()
        efficiencyHistos = getEfficiencyHistoNames()
        categories = categoriesDict()
        cuts = getCuts()
        trackers = getTrackers()
        folders = getOriginFolders()

        # Define the location of the input root file, which contains the reconstructible & reconstructed histograms for drawing the efficiency plots.
        inputfile = os.path.join(directory, "PrCheckerPlots_HLT2.root")
        # Define the location of the output rootfile, which contains the efficiency plots
        outputfile = ROOT.TFile("./efficiency_hlt2_plots.root", "recreate")

        for tracker in trackers:  # Loop for track category
            outputfile.cd()
            trackerDir = outputfile.mkdir(tracker)
            trackerDir.cd()

            for cut in cuts[
                    tracker]:  # Loop for different selections on the tracks
                cutDir = trackerDir.mkdir(cut)
                cutDir.cd()
                folder = folders[tracker]["folder"]
                histoBaseName = "Track/" + folder + tracker + "/" + cut + "_"

                # calculate efficiency
                for histo in efficiencyHistos:  # Loop for kinematic variables
                    draw_electron = (
                        True  # Turn to False if no corresponding histograms are found
                    )
                    draw_non_electron = (
                        True  # Turn to False if no corresponding histograms are found
                    )
                    title = ("efficiency vs. " + histo + ", " +
                             categories[tracker][cut]["title"])
                    name = "efficiency vs. " + histo
                    canvas = ROOT.TCanvas(name, title)
                    ROOT.gPad.SetTicks()
                    # get efficiency plots for 'not electrons' category
                    # The base of histogram names is defined in utils.ConfigHistos_HLT2
                    histoName = (histoBaseName + "notElectrons_" +
                                 efficiencyHistoDict[histo]["variable"])
                    # read the input file and get the histograms
                    f = ROOT.TFile.Open(inputfile, "read")
                    numeratorName = histoName + "_reconstructed"
                    numerator = f.Get(numeratorName)
                    denominatorName = histoName + "_reconstructible"
                    denominator = f.Get(denominatorName)
                    print("************")
                    print(histoName)
                    if not numerator or not denominator:
                        # Print some information to tell whether a histogram is found or not
                        print(
                            "Not found in input root file: ",
                            numeratorName,
                            " or ",
                            denominatorName,
                        )
                        draw_non_electron = False
                    if draw_non_electron:
                        print(
                            "reconstructed: ",
                            numerator.GetEntries(),
                            "; reconsructible: ",
                            denominator.GetEntries(),
                        )
                    print("************")
                    if draw_non_electron:
                        numerator.Sumw2()
                        denominator.Sumw2()
                        # Define the TGraphAsymmErrors which shows the efficiency distributions
                        g_efficiency_notElectrons = ROOT.TGraphAsymmErrors()
                        g_efficiency_notElectrons.Divide(
                            numerator, denominator, "cl=0.683 b(1,1) mode")
                        g_efficiency_notElectrons.SetTitle("not electrons")

                    # get efficiency plots for electrons category. The structure of script is the same as the non-electron one.
                    histoName = (histoBaseName + "electrons_" +
                                 efficiencyHistoDict[histo]["variable"])
                    f = ROOT.TFile.Open(inputfile, "read")
                    numeratorName = histoName + "_reconstructed"
                    numerator = f.Get(numeratorName)
                    denominatorName = histoName + "_reconstructible"
                    denominator = f.Get(denominatorName)
                    if not numerator or not denominator:
                        print(
                            "Not found in input root file: ",
                            numeratorName,
                            " or ",
                            denominatorName,
                        )
                        draw_electron = False
                    if draw_electron:
                        print(
                            "reconstructed: ",
                            numerator.GetEntries(),
                            "; reconsructible: ",
                            denominator.GetEntries(),
                        )
                    print("************")

                    if draw_electron:
                        numerator.Sumw2()
                        denominator.Sumw2()
                        g_efficiency_electrons = ROOT.TGraphAsymmErrors()
                        g_efficiency_electrons.Divide(numerator, denominator,
                                                      "cl=0.683 b(1,1) mode")
                        g_efficiency_electrons.SetTitle("electrons")
                        g_efficiency_electrons.SetMarkerColor(ROOT.kAzure - 3)
                        g_efficiency_electrons.SetLineColor(ROOT.kAzure - 3)

                    cutDir.cd()

                    # draw the plots in the TMultiGraph if the entry is not zero
                    mg = ROOT.TMultiGraph()
                    if draw_non_electron:
                        mg.Add(g_efficiency_notElectrons)
                    if draw_electron:
                        mg.Add(g_efficiency_electrons)
                    if draw_electron or draw_non_electron:
                        mg.Draw("ap")
                        xtitle = efficiencyHistoDict[histo]["xTitle"]
                        mg.GetXaxis().SetTitle(xtitle)
                        mg.GetYaxis().SetTitle("efficiency")
                        mg.GetYaxis().SetRangeUser(0, 1)

                        canvas.PlaceLegend()
                    f.Close()
                    if draw_electron or draw_non_electron:
                        canvas.Write()

                        # Make the plots to be sent to eos
                        if (tracker == "BestLong" and cut == "Long_eta25"
                                and histo == "pt"):
                            latex = ROOT.TLatex()
                            latex.SetNDC()
                            latex.SetTextSize(0.04)
                            latex.DrawLatex(0.45, 0.3, "BestLong, 2 < eta < 5")
                            canvas.SaveAs("BestLong_eta25_effpt.png")

        outputfile.Write()
        outputfile.Close()

        self.saveFile("PrCheckerEfficiency_HLT2",
                      "./efficiency_hlt2_plots.root")

        # send plot to EOS
        wwwDirEos = os.environ.get("LHCBPR_WWW_EOS")
        if wwwDirEos == None:
            raise Exception(
                "No web dir on EOS defined, will not run extraction")
        else:
            time = (str(startTime).replace(" +0200", "").replace(
                " +0100", "").replace(" ", "_").replace("-", "").replace(
                    ":", ""))
            dirname = "efficiency"
            targetRootEosDir = os.path.join(wwwDirEos, dirname)
            print("targetRootEosDir: ", targetRootEosDir)
            output_png_list = ["BestLong_eta25_effpt"]
            for output_png in output_png_list:
                try:
                    subprocess.call([
                        "xrdcp",
                        "-f",
                        output_png + ".png",
                        targetRootEosDir + "/" + output_png + "_" +
                        str(version) + "_" + str(platform) + "_" + time +
                        ".png",
                    ])
                except Exception as ex:
                    log.warning("Error copying to eos: %s", ex)
                self.saveString(
                    output_png,
                    "cern.ch/lhcbpr-hlt/PerfTests/UpgradeVelo/" + dirname + "/"
                    + output_png + "_" + str(version) + "_" + str(platform) +
                    "_" + time + ".png",
                    description="link to " + output_png + "  plot",
                    group="prchecker_efficiency_plots",
                )
