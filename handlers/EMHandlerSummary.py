##############################################################################################
## Handler for EM test. The input are the rootfiles that contain the histograms of interest.
## perfroms and analysis on these histograms and produced summary plots.
## @Author: G. Chatzikonstantinidis
## @date:   Last modified 2018-10-15
## the code is adaptation of the code found in:
## Gauss /Sim/SimChecks/scripts/EmValidation/
## from the scripts  landau*.C and analysis.py
##############################################################################################

import ROOT
import math
import json, os, re
from glob import glob
from .BaseHandler import BaseHandler
from array import array


class landau:
    ############################################################################################
    def landau_fit(self, fileName, histoName, betagamma):
        """
        FIT LANDAU : Get the ROOT file that contains the energy deposit in VELO
                     and get MPV and FWHM. The root file that contains the histogram
                     with the energy deposits is loaded. In this method a parabolic fit
                     around the peak using up to second orders of polynomials is performed.
                     The method returns the MPV and FHW.
        """
        file = ROOT.TFile(fileName)
        histo = file.Get(histoName)

        histo.Rebin(1)
        fpol = ROOT.TF1("f1", "pol2", 0, 2000)
        # this limit is set in order to avoid getting inverted parabola around the peak due to
        # low statistics
        fpol.SetParLimits(2, -100, 0)
        xmax, xval = self.landau_getMPV(histo, fpol, fileName, betagamma)
        right, left = self.landau_getFWHM(histo, xval)
        scale = right - left
        return xmax, scale

    ##############################################################################################
    def landau_getMPV(self, histo, pol, fileName, betagamma):
        """
        FIT LANDAU : Find the MPV from the landau distribution. A search for the MPV is performed
                     around the peak using a parabolic fit. The function returns the position and
                     the value of the peak
        """
        x = histo.GetBinCenter(histo.GetMaximumBin())
        for time in range(3):
            # the +/- is addhoc but in general the larger the statistics in the histogram
            # the smaller the range that we need.
            histo.Fit(pol, "QB", "", x - 8, x + 8)
            x = self.landau_getMax(pol, x - 8, x + 8)
        return x, pol.Eval(x)

    ###############################################################################################
    def landau_fitgen(self, fileName, histo, betagamma):
        """
        FIT LANDAU GEN : Fit the histogram with the landau distribution generated. The function
                         returns MPV and width of the landau distribution.
        """
        fpol = ROOT.TF1("f1", "pol2", 0, 2000)
        xmax, xval = self.landau_getMPV(histo, fpol, fileName, betagamma)

        right, left = self.landau_getFWHM(histo, xval)
        scale = right - left
        return xmax, scale

    ##############################################################################################
    def landau_getMax(self, pol, min, max):
        """
        FIT LANDAU : Find maximum value of distribution. Takes a range and the parabolic fit result
                     around the peak and scans for the maximum value form the fit. The function returns
                     the position of the maximum
        """
        start = min
        end = max
        step = (max - min) / 1000
        maxVal = pol.Eval(start)
        maxX = start
        while start < end:
            if pol.Eval(start) > maxVal:
                maxX = start
                maxVal = pol.Eval(start)
            start = start + step
        return maxX

    ################################################################################################
    def landau_getFWHM(self, histo, mpv):
        """
        FIT LANDAU : Find FWHM
        """
        maxBin = histo.GetMaximumBin()
        reqLevel = 0.5 * mpv

        ileft = maxBin
        while ileft > 0 and (histo.GetBinContent(ileft) > reqLevel):
            ileft = ileft - 1
            left = self.landau_interpolate(ileft, reqLevel, -1, histo)

        iright = maxBin
        while iright < histo.GetNbinsX() and histo.GetBinContent(
                iright) > reqLevel:
            iright = iright + 1
            right = self.landau_interpolate(iright, reqLevel, 1, histo)
        return right, left

    ######################################################################################
    def landau_interpolate(self, bin, req, off, histo):
        """
        FIT LANDAU : Interpolate. The functions performs and interpolation of the parabolic
                     fit.
        """
        tmp1 = histo.GetBinContent(bin) - histo.GetBinContent(bin + off)
        tmp2 = histo.GetBinCenter(bin) - histo.GetBinCenter(bin + off)
        step = off
        while tmp1 == 0 or tmp2 == 0:
            off = off + 1
            tmp1 = histo.GetBinContent(bin) - histo.GetBinContent(bin + off)
            tmp2 = histo.GetBinCenter(bin) - histo.GetBinCenter(bin + off)

        slope = tmp1 / tmp2
        const = histo.GetBinContent(bin) - slope * histo.GetBinCenter(bin)
        return (req - const) / slope

    #######################################################################################
    def landau_genlandau(self, beta, gamma):
        """
        FIT LANDAU : Generate landau distribution. The function generates a landau distribution
                     including smearing effects. The function returns a histogram with the
                     energy deposits.
        """
        thikness = 300
        b = self.landau_genlandauWidth(beta, thikness)
        a = self.landau_genlandauMPV(b, beta, gamma) + (0.226 * b)

        fL = ROOT.TF1(
            "fL",
            "landau",
            0.0,
            600,
        )
        fL.SetParameters(100000, a, b)

        fG = ROOT.TF1("fG", "gaus", -50, 50)
        bindingEffect = self.landau_atomicBinding(thikness)
        fG.SetParameters(1, 0.0, bindingEffect)

        fGN = ROOT.TF1("fGN", "gaus", -50, 50)
        fGN.SetParameters(1, 0.0, 3.8)

        fhisto = ROOT.TH1F("fhisto", "", 1000, 0, 500)
        for i in range(100000):
            val = fL.GetRandom() + fG.GetRandom()
            fhisto.Fill(val)

        return fhisto

    ##########################################################################################
    def landau_genlandauWidth(self, beta, t):
        """
        FIT LANDAU : Generate landau width. The function generates the width of the landau
                     distribution. The functions returns the with of the landau
        """
        return 0.017825 * t / (beta**2)

    #########################################################################################
    def landau_densityEffect(self, beta, gamma):
        """
        FIT LANDAU : Generate density effects. The function calculate density effects. The
                     function returns density effects.
        """
        x = math.log(beta * gamma, 10)
        dEffect = 0
        if x > 0.09666 and x < 2.5:
            dEffect = 4.606 * x - 4.435 + (0.3755 * pow(2.5 - x, 2.75))
        else:
            dEffect = 4.606 * x - 4.435
        return dEffect

    ##############################################################################################
    def landau_genlandauMPV(self, b, beta, gamma):
        """
        FIT LANDAU :  Generate MPV. The function generates the MPV. The function returns MPV.
        """
        return b * (math.log(2.0 * 511.0 *
                             (beta * gamma)**2 / 0.174) + math.log(b / 0.174) +
                    0.2 - beta**2 - self.landau_densityEffect(beta, gamma))

    ##############################################################################################
    def landau_atomicBinding(self, t):
        """
        FIT LANDAU :  Generate atomic binding effects. The function generaters atomic binding
                      effects. The function returns atomic binding effects.
        """
        return math.sqrt(0.18 * t)


class toolsPlots:
    ###############################################################################################
    def getParams(self, id, p):
        """
        BETA - GAMMA : Get beta expresion. The Function returns beta and gamma
        """
        mass = 0
        if id == 11.0:
            mass = 0.000511
        if id == 13.0:
            mass = 0.10566
        if id == 211.0:
            mass = 0.13498

        energy = math.sqrt(p**2 + mass**2)
        gamma = energy / mass
        beta = p / (gamma * mass)
        return beta, gamma

    ###############################################################################################
    def loadFiles(self, path):
        """
        LOAD Files :  The function loads all root files and returns a list of them.
        """
        return glob(path + "*.root")

    ###############################################################################################
    """
    LOAD Files : Only files with more thatn 1000 entries will be considered. Root Files with less
                 entries will not be used, since the fit in the peak does not make much sence any
                 more. The function returns a binary decision.
    """

    def checkhisto(self, file, histo):
        f = ROOT.TFile(file)
        goodHisto = False
        if ROOT.gROOT.FindObject("energy_deposit"):
            h = f.Get("energy_deposit")
            if h.GetEntries() > 1000:
                goodHisto = True
            else:
                goodHisto = False
        else:
            goodHisto = False
        return goodHisto

    ##########################################################################################################################
    """
    Make Graphs : The function makes the graphs that will be presented, most of the style is alread attached. Returns a list
                  of tuples that contain the TGraphs and info used for the finall massage.
    """

    def getGraphs(self, objects, MPV, FWHM):
        tg = []
        book = dict()
        book = {
            11.0: {
                "NoCuts": (array("d"), array("d")),
                "Opt1": (array("d"), array("d")),
                "Opt2": (array("d"), array("d")),
                "Opt3": (array("d"), array("d")),
                "LHCb": (array("d"), array("d")),
            },
            13.0: {
                "NoCuts": (array("d"), array("d")),
                "Opt1": (array("d"), array("d")),
                "Opt2": (array("d"), array("d")),
                "Opt3": (array("d"), array("d")),
                "LHCb": (array("d"), array("d")),
            },
            211.0: {
                "NoCuts": (array("d"), array("d")),
                "Opt1": (array("d"), array("d")),
                "Opt2": (array("d"), array("d")),
                "Opt3": (array("d"), array("d")),
                "LHCb": (array("d"), array("d")),
            },
            0.0: {
                "theory": (array("d"), array("d"))
            },
        }

        for key in objects:
            book[objects[key]["ID"]][objects[key]["PL"]][0].append(
                objects[key]["BETAGAMMA"])
            book[objects[key]["ID"]][objects[key]["PL"]][1].append(
                objects[key]["MPV"] if MPV else objects[key]["MPV"] * 1.0 /
                objects[key]["FWHM"])

        for key in book:
            for kkey in book[key]:
                if len(book[key][kkey][0]) != 0:
                    name = " "
                    if key == 11.0:
                        name = "e^{#pm}"
                    if key == 13.0:
                        name = "#mu^{#pm}"
                    if key == 211.0:
                        name = "#pi^{#pm}"

                    tg.append((
                        ROOT.TGraph(
                            len(book[key][kkey][0]),
                            book[key][kkey][0],
                            book[key][kkey][1],
                        ),
                        (name, kkey),
                    ))

        if MPV:
            tg[0][0].GetYaxis().SetRangeUser(70, 220)
            tg[0][0].GetXaxis().SetLimits(0.9, 1000000)
            tg[0][0].SetTitle("")
        else:
            tg[0][0].GetYaxis().SetRangeUser(2, 8)
            tg[0][0].GetXaxis().SetLimits(0.9, 1000000)
            tg[0][0].SetTitle("")

        tg[0][0].GetXaxis().SetTitle("#beta#gamma")
        tg[0][0].GetYaxis().SetTitle("MPV [eV]" if MPV else "MPV/FWHM")
        tg[0][0].SetMarkerStyle(24)
        tg[0][0].SetMarkerSize(2)
        tg[0][0].SetMarkerColor(ROOT.kBlack)

        index = 4
        for i in range(0, len(tg)):
            tg[i][0].SetMarkerStyle(24)
            tg[i][0].SetMarkerSize(1)
            tg[i][0].SetMarkerColor(1 + index)
            index = index + 4

        return tg

    ###########################################################################################################################
    def makePlots(self, objects, opt, nameFile, operType):
        """
        Make the final canvas. The function makes the final canvas that is used in the LHCbPR. The function returns a canvas.
        """
        gr = self.getGraphs(objects, opt[0], opt[1])
        c1 = ROOT.TCanvas("", "")
        c1.SetTitle(" ")
        c1.SetName("MPV" if opt[0] else "MPV_FWHM")
        c1.SetLogx()
        c1.SetGrid()

        gr[0][0].Draw("AP")

        for i in range(0, len(gr)):
            gr[i][0].Draw("P SAME")

        leg = ROOT.TLegend(0.6, 0.4, 0.8, 0.8)
        for i in gr:
            if i[1][1] != "theory":
                leg.AddEntry(i[0], (i[1][0] + " " + i[1][1]), "P")
            else:
                i[0].SetMarkerStyle(7)
                i[0].SetMarkerColor(1)
                leg.AddEntry(i[0], i[1][1], "P")
        leg.Draw("SAME")
        File = ROOT.TFile(nameFile, operType)
        c1.Write()
        File.Close()


class EMHandlerSummary(BaseHandler):
    def __init__(self):
        super(self.__class__, self).__init__()

    def collectResults(self, directory):
        ROOT.gROOT.SetBatch(ROOT.kTRUE)

        rootFiles = glob(directory + "*.root" if directory.
                         endswith("/") else directory + "/*.root")

        tools = toolsPlots()
        fit = landau()
        obsInfo = dict()

        for id in [11.0, 13.0, 211.0]:
            for pl in ["NoCuts", "Opt1", "Opt2", "Opt3", "LHCb"]:
                for en in [
                        0.1,
                        0.2,
                        0.4,
                        1.0,
                        5.0,
                        10.0,
                        16.8,
                        50.0,
                        100.0,
                        120.0,
                        168.0,
                ]:
                    stringFile = (
                        "RootFileSimMonitor_%s_%s_%s_velo-histos_LHCbPR.root" %
                        (pl, id, en))
                    if directory.endswith("/"):
                        stringFile = directory + stringFile
                    else:
                        stringFile = directory + "/" + stringFile
                    for strFile in rootFiles:
                        if stringFile in strFile:
                            print(stringFile)
                            if tools.checkhisto(strFile, "energy_deposit"):
                                b, g = tools.getParams(id, en)
                                mpv, fwhm = fit.landau_fit(
                                    stringFile, "energy_deposit", b * g)
                                obsInfo[stringFile] = {
                                    "ID": id,
                                    "PL": pl,
                                    "EN": en,
                                    "MPV": mpv,
                                    "FWHM": fwhm,
                                    "BETAGAMMA": b * g,
                                }

        energy = [
            0.1,
            0.15,
            0.2,
            0.25,
            0.3,
            0.35,
            0.4,
            0.45,
            0.5,
            0.55,
            0.6,
            0.65,
            0.7,
            0.75,
            0.8,
            0.85,
            0.9,
            0.95,
            1.0,
        ]

        index = 0.1
        for i in range(100):
            index = index + 1.0 / 100
            energy.append(index)

        index = 0
        for i in range(100):
            index = index + 250.0 / 50
            energy.append(index)

        for id in [11.0, 13.0]:
            for pl in ["theory"]:
                for en in energy:
                    b, g = tools.getParams(id, en)
                    stringFile = (pl + "_" + str(id) + "_" + str(en) + "_" +
                                  str(b * g) + ".root")
                    mpv, fwhm = fit.landau_fitgen(stringFile,
                                                  fit.landau_genlandau(b, g),
                                                  tools.getParams(id, en))
                    obsInfo[stringFile] = {
                        "ID": 0.0,
                        "PL": pl,
                        "EN": en,
                        "MPV": mpv,
                        "FWHM": fwhm,
                        "BETAGAMMA": b * g,
                    }

        tools.makePlots(
            obsInfo,
            [True, False],
            (directory + "EMSummary.root"
             if directory.endswith("/") else directory + "/EMSummary.root"),
            "RECREATE",
        )
        tools.makePlots(
            obsInfo,
            [False, True],
            (directory + "EMSummary.root"
             if directory.endswith("/") else directory + "/EMSummary.root"),
            "UPDATE",
        )
        self.saveFile(
            "summary",
            (directory + "EMSummary.root"
             if directory.endswith("/") else directory + "/EMSummary.root"),
        )
