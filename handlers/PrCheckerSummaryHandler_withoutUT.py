# Handler for summarizing the output of PrChecker2.
#
# It extracts the most interesting histograms and also
# produces efficiency histograms of reconstructed vs
# reconstructible tracks for different tracking categories.
#
# The 1D efficiencies are calculated usig TGraphAsymmErrors
# and Bayesian error bars
# Aim is to check reconstruction without UT
#
# Based on: PrCheckerSummaryhandler
# author: Miroslav Saur (miroslav.saur@cern.ch) & Suzanne Klaver (suzanne.klaver@cern.ch)
# Version:
# 	   - 0.1 (2021/12): version of PrCheckerSummaryhandler without downstream and forward tracks

import os, sys
import argparse
import ROOT
from ROOT import gStyle
from ROOT import gROOT
from ROOT import TStyle
from ROOT import gPad
from .BaseHandler import BaseHandler
from .gaussValidation import grepPattern
from .utils import HashRemover
from collectRunResults import urlopen
import logging
import subprocess

from collections import defaultdict


def effPlotsDict_long():
    basedict = {
        "Pt": {},
        "P": {},
        "Eta": {},
        "nPV": {},
        "docaz": {},
    }

    basedict["Pt"]["path"] = "Track/BestLongTrackChecker/BestLong/01_long_Pt"
    basedict["P"]["path"] = "Track/BestLongTrackChecker/BestLong/01_long_P"
    basedict["Eta"]["path"] = "Track/BestLongTrackChecker/BestLong/01_long_Eta"
    basedict["nPV"]["path"] = "Track/BestLongTrackChecker/BestLong/01_long_nPV"
    basedict["docaz"][
        "path"] = "Track/BestLongTrackChecker/BestLong/01_long_docaz"
    basedict["Pt"]["unit"] = "pT [MeV]"
    basedict["P"]["unit"] = "p [MeV]"
    basedict["Eta"]["unit"] = "#eta [-]"
    basedict["nPV"]["unit"] = "nPV [-]"
    basedict["docaz"]["unit"] = "docaz [mm]"
    basedict["Pt"]["unit_y"] = "Effciency [-]"
    basedict["P"]["unit_y"] = "Effciency [-]"
    basedict["Eta"]["unit_y"] = "Effciency [-]"
    basedict["nPV"]["unit_y"] = "Effciency [-]"
    basedict["docaz"]["unit_y"] = "Effciency [-]"
    return basedict


def getEffPlots_long():
    return ["Pt", "P", "Eta", "nPV", "docaz"]


def effPlotsDict_velo():
    basedict = {
        "Pt": {},
        "P": {},
        "Eta": {},
        "nPV": {},
        "docaz": {},
    }

    basedict["Pt"][
        "path"] = "Track/VeloFullTrackChecker/VeloFull/01_notElectron_Velo_Pt"
    basedict["P"][
        "path"] = "Track/VeloFullTrackChecker/VeloFull/01_notElectron_Velo_P"
    basedict["Eta"][
        "path"] = "Track/VeloFullTrackChecker/VeloFull/01_notElectron_Velo_Eta"
    basedict["nPV"][
        "path"] = "Track/VeloFullTrackChecker/VeloFull/01_notElectron_Velo_nPV"
    basedict["docaz"][
        "path"] = "Track/VeloFullTrackChecker/VeloFull/01_notElectron_Velo_docaz"
    basedict["Pt"]["unit"] = "pT [MeV]"
    basedict["P"]["unit"] = "p [MeV]"
    basedict["Eta"]["unit"] = "#eta [-]"
    basedict["nPV"]["unit"] = "nPV [-]"
    basedict["docaz"]["unit"] = "docaz [mm]"
    basedict["Pt"]["unit_y"] = "Effciency [-]"
    basedict["P"]["unit_y"] = "Effciency [-]"
    basedict["Eta"]["unit_y"] = "Effciency [-]"
    basedict["nPV"]["unit_y"] = "Effciency [-]"
    basedict["docaz"]["unit_y"] = "Effciency [-]"
    return basedict


def getEffPlots_velo():
    return ["Pt", "P", "Eta", "nPV", "docaz"]


def effPlotsDict_ttrack():
    basedict = {
        "Pt": {},
        "P": {},
        "Eta": {},
        "nPV": {},
        "docaz": {},
    }

    basedict["Pt"]["path"] = "Track/SeedTrackChecker/Seed/01_hasT_Pt"
    basedict["P"]["path"] = "Track/SeedTrackChecker/Seed/01_hasT_P"
    basedict["Eta"]["path"] = "Track/SeedTrackChecker/Seed/01_hasT_Eta"
    basedict["nPV"]["path"] = "Track/SeedTrackChecker/Seed/01_hasT_nPV"
    basedict["docaz"]["path"] = "Track/SeedTrackChecker/Seed/01_hasT_docaz"
    basedict["Pt"]["unit"] = "pT [MeV]"
    basedict["P"]["unit"] = "p [MeV]"
    basedict["Eta"]["unit"] = "#eta [-]"
    basedict["nPV"]["unit"] = "nPV [-]"
    basedict["docaz"]["unit"] = "docaz [mm]"
    basedict["Pt"]["unit_y"] = "Effciency [-]"
    basedict["P"]["unit_y"] = "Effciency [-]"
    basedict["Eta"]["unit_y"] = "Effciency [-]"
    basedict["nPV"]["unit_y"] = "Effciency [-]"
    basedict["docaz"]["unit_y"] = "Effciency [-]"
    return basedict


def getEffPlots_ttrack():
    return ["Pt", "P", "Eta", "nPV", "docaz"]


class PrCheckerSummaryHandler_withoutUT(BaseHandler):
    def __init__(self):
        super(self.__class__, self).__init__()

    def collectResultsExt(
            self,
            directory,
            project,
            version,
            platform,
            hostname,
            cpu_info,
            memoryinfo,
            startTime,
            endTime,
            options,
    ):
        logfile = os.path.join(directory, "run.log")
        rootfile = grepPattern(
            "INFO Writing ROOT histograms to: (\S+)",
            open(logfile, "r", encoding="ISO-8859-1").read(),
        )
        """
        # This should be modified to at first check if required files are available, commenting out for now
        # combining the root files produces from old style and new style histograms
        # this will be obsolete once all monitors and checkers are converted.
        rootfile_new = os.path.splitext(rootfile)[0]+"_new.root"
        subprocess.run(
            "hadd -f comb.root {} {}; mv comb.root {}".
            format(os.path.join(directory, rootfile),
                   os.path.join(directory, rootfile_new),
                   os.path.join(directory, rootfile)),
            shell=True
        )
        """

        modifier = HashRemover.HashRemover(os.path.join(directory, rootfile))
        modifier.modify_root_file()
        inputfile = ROOT.TFile.Open(os.path.join(directory, rootfile), "read")
        outputfile = ROOT.TFile("PrCheckerSummary.root", "recreate")

        from .utils.LHCbStyle import setLHCbStyle
        from .utils.Legend import place_legend

        setLHCbStyle()

        latex = ROOT.TLatex()
        latex.SetNDC()
        latex.SetTextSize(0.03)

        outputfile.mkdir("long")
        outputfile.mkdir("velo")
        outputfile.mkdir("ttrack")

        effPlots_long = getEffPlots_long()
        eff_dict_long = effPlotsDict_long()
        effPlots_velo = getEffPlots_velo()
        eff_dict_velo = effPlotsDict_velo()
        effPlots_ttrack = getEffPlots_ttrack()
        eff_dict_ttrack = effPlotsDict_ttrack()

        for effPlot in effPlots_long:
            outputfile.cd("long")
            effNom = inputfile.Get(eff_dict_long[effPlot]["path"] +
                                   "_reconstructed")
            effDenom = inputfile.Get(eff_dict_long[effPlot]["path"] +
                                     "_reconstructible")
            canvas_eff = ROOT.TCanvas(effPlot, effPlot)
            efficiency = ROOT.TGraphAsymmErrors()
            efficiency.SetName(effPlot + "_tracking_efficiency")
            efficiency.Divide(effNom, effDenom, "w")

            efficiency.SetMarkerSize(0.5)
            efficiency.SetLineWidth(1)
            efficiency.SetMarkerColor(ROOT.kBlack)
            efficiency.SetMarkerStyle(5)
            efficiency.GetXaxis().SetLabelFont(132)
            efficiency.GetXaxis().SetLabelSize(0.04)
            efficiency.GetXaxis().SetTitleFont(132)
            efficiency.GetXaxis().SetTitleSize(0.06)
            efficiency.GetXaxis().SetTitleOffset(0.8)
            efficiency.GetYaxis().SetLabelFont(132)
            efficiency.GetYaxis().SetLabelSize(0.04)
            efficiency.GetYaxis().SetTitleFont(132)
            efficiency.GetYaxis().SetTitleSize(0.06)
            efficiency.GetYaxis().SetDecimals()
            efficiency.GetYaxis().SetTitleOffset(0.7)
            efficiency.GetXaxis().SetTitle(eff_dict_long[effPlot]["unit"])
            efficiency.GetYaxis().SetTitle(eff_dict_long[effPlot]["unit_y"])
            efficiency.SetTitle(
                "{path} reconstructed / reconstructible".format(
                    path=eff_dict_long[effPlot]["path"]))

            efficiency.Draw("AP")
            efficiency.Write(effPlot + "_tgraph")
            canvas_eff.Write(effPlot + "_canvas")

        for effPlot in effPlots_velo:
            outputfile.cd("velo")
            effNom = inputfile.Get(eff_dict_velo[effPlot]["path"] +
                                   "_reconstructed")
            effDenom = inputfile.Get(eff_dict_velo[effPlot]["path"] +
                                     "_reconstructible")
            canvas_eff = ROOT.TCanvas(effPlot, effPlot)
            efficiency = ROOT.TGraphAsymmErrors()
            efficiency.SetName(effPlot + "_tracking_efficiency")
            efficiency.Divide(effNom, effDenom, "w")

            efficiency.SetMarkerSize(0.5)
            efficiency.SetLineWidth(1)
            efficiency.SetMarkerColor(ROOT.kBlack)
            efficiency.SetMarkerStyle(5)
            efficiency.GetXaxis().SetLabelFont(132)
            efficiency.GetXaxis().SetLabelSize(0.04)
            efficiency.GetXaxis().SetTitleFont(132)
            efficiency.GetXaxis().SetTitleSize(0.06)
            efficiency.GetXaxis().SetTitleOffset(0.8)
            efficiency.GetYaxis().SetLabelFont(132)
            efficiency.GetYaxis().SetLabelSize(0.04)
            efficiency.GetYaxis().SetTitleFont(132)
            efficiency.GetYaxis().SetTitleSize(0.06)
            efficiency.GetYaxis().SetDecimals()
            efficiency.GetYaxis().SetTitleOffset(0.7)
            efficiency.GetXaxis().SetTitle(eff_dict_velo[effPlot]["unit"])
            efficiency.GetYaxis().SetTitle(eff_dict_velo[effPlot]["unit_y"])
            efficiency.SetTitle(
                "{path} reconstructed / reconstructible".format(
                    path=eff_dict_velo[effPlot]["path"]))

            efficiency.Draw("AP")
            efficiency.Write(effPlot + "_tgraph")
            canvas_eff.Write(effPlot + "_canvas")

        for effPlot in effPlots_ttrack:
            outputfile.cd("ttrack")
            effNom = inputfile.Get(eff_dict_ttrack[effPlot]["path"] +
                                   "_reconstructed")
            effDenom = inputfile.Get(eff_dict_ttrack[effPlot]["path"] +
                                     "_reconstructible")
            canvas_eff = ROOT.TCanvas(effPlot, effPlot)
            efficiency = ROOT.TGraphAsymmErrors()
            efficiency.SetName(effPlot + "_tracking_efficiency")
            efficiency.Divide(effNom, effDenom, "w")

            efficiency.SetMarkerSize(0.5)
            efficiency.SetLineWidth(1)
            efficiency.SetMarkerColor(ROOT.kBlack)
            efficiency.SetMarkerStyle(5)
            efficiency.GetXaxis().SetLabelFont(132)
            efficiency.GetXaxis().SetLabelSize(0.04)
            efficiency.GetXaxis().SetTitleFont(132)
            efficiency.GetXaxis().SetTitleSize(0.06)
            efficiency.GetXaxis().SetTitleOffset(0.8)
            efficiency.GetYaxis().SetLabelFont(132)
            efficiency.GetYaxis().SetLabelSize(0.04)
            efficiency.GetYaxis().SetTitleFont(132)
            efficiency.GetYaxis().SetTitleSize(0.06)
            efficiency.GetYaxis().SetDecimals()
            efficiency.GetYaxis().SetTitleOffset(0.7)
            efficiency.GetXaxis().SetTitle(eff_dict_ttrack[effPlot]["unit"])
            efficiency.GetYaxis().SetTitle(eff_dict_ttrack[effPlot]["unit_y"])
            efficiency.SetTitle(
                "{path} reconstructed / reconstructible".format(
                    path=eff_dict_ttrack[effPlot]["path"]))

            efficiency.Draw("AP")
            efficiency.Write(effPlot + "_tgraph")
            canvas_eff.Write(effPlot + "_canvas")

        outputfile.Write()
        outputfile.Close()
        inputfile.Close()
        self.saveFile("PrCheckerSummary", "PrCheckerSummary.root")
