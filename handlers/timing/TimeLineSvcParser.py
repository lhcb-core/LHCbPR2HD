#!/usr/bin/env python

import csv
import json
import os
"""TimeLineSvcParser will read a timeline.csv file and return a json object out
of the rsulting data. """


class TimeLineSvcParser:
    def __init__(self, directory):
        csv.register_dialect("timelinesvc", delimiter=" ")
        self.directory = directory
        self.data = []

    def collectData(self):
        csvfile = self.directory + os.sep + "timeline.csv"
        csvh = open(csvfile)
        firstline = csvh.readlines()[0]
        while firstline[0] == "#":
            firstline = firstline[1:]
        firstline.strip()
        fieldnames = tuple(firstline.split())
        csvh.close()

        csvh = open(csvfile)
        reader = csv.DictReader(csvh, fieldnames, dialect="timelinesvc")
        for row in reader:
            if list(row.keys())[0] != row[list(row.keys())[0]]:
                self.data.append(row)
        self.data = json.loads(json.JSONEncoder().encode(self.data))
        csvh.close()

    def getData(self):
        return self.data
