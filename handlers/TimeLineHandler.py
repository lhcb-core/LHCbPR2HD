#!/usr/bin/env python

import os
import numpy
from .BaseHandler import BaseHandler
from .timing import TimeLineSvcParser

# lb-run --nightly-cvmfs --nightly=lhcb-future Brunel/future gaudirun.py \
# \${BRUNEL_PROJECT_ROOT}/Rec/Brunel/options/MiniBrunel.py
""" TimeLineHandler will parse a timeline.csv file (via TimeLineSvcParser) and
extract different metrics out of the resulting json data, such as total
execution time (end of last algo - start of first algo), the minimum, maximum,
mean and standard deviations of the processing time per event and for each
algorithm. """


class TimeLineHandler(BaseHandler):
    def __init__(self, directory=""):
        super(self.__class__, self).__init__()
        self.directory = directory
        if not self.directory:
            self.directory = os.path.realpath(os.curdir)

    def collectResults(self, directory):
        # collect data from the timeline.csv output file and return a json
        tsp = TimeLineSvcParser.TimeLineSvcParser(directory)
        tsp.collectData()
        data = tsp.getData()

        evts = {}
        algs = {}
        psta = int(data[0]["start"])
        pend = int(data[0]["end"])

        for x in data:
            evt = int(x["event"])
            sta = int(x["start"])
            end = int(x["end"])
            alg = x["algorithm"]
            psta = min(psta, sta)
            pend = max(pend, end)

            if evt not in list(evts.keys()):
                evts[evt] = [sta, end]
            else:
                evts[evt] = [min(sta, evts[evt][0]), max(end, evts[evt][1])]

            if alg not in list(algs.keys()):
                algs[alg] = [end - sta]
            else:
                algs[alg].append(end - sta)

        durs = []
        for x in list(evts.keys()):
            dur = evts[x][1] - evts[x][0]
            durs.append(dur)

        for algo in list(algs.keys()):
            self.saveInt(
                "min_" + algo,
                int(round(min(algs[algo]))),
                "minimum execution time " + algo,
                "algorithm timing",
            )
            self.saveInt(
                "max_" + algo,
                int(round(max(algs[algo]))),
                "minimum execution time " + algo,
                "algorithm timing",
            )
            self.saveInt(
                "mean_" + algo,
                int(round(numpy.mean(algs[algo]))),
                "minimum execution time " + algo,
                "algorithm timing",
            )
            self.saveInt(
                "sigma_" + algo,
                int(round(numpy.std(algs[algo]))),
                "minimum execution time " + algo,
                "algorithm timing",
            )

        self.saveInt(
            "min_evt_time",
            int(round(min(durs))),
            "minimum event execution time",
            "event timing",
        )
        self.saveInt(
            "max_evt_time",
            int(round(max(durs))),
            "maximum event execution time",
            "event timing",
        )
        self.saveInt(
            "mean_evt_time",
            int(round(numpy.mean(durs))),
            "mean event execution time",
            "event timing",
        )
        self.saveInt(
            "sigma_evt_time",
            int(round(numpy.std(durs))),
            "event execution time standard deviation",
            "event timing",
        )

        totaltime = pend - psta
        self.saveFloat(
            "total_execution_time",
            totaltime,
            "the total execution time",
            "total timing",
        )

    def run(self):
        self.collectResults(self.directory)


if __name__ == "__main__":
    TimeLineHandler().run()

# fieldnames = ("FirstName","LastName","IDNumber","Message")
# reader = csv.DictReader( csvfile, fieldnames)
# for row in reader:
#    json.dump(row, jsonfile)
#    jsonfile.write('\n')
