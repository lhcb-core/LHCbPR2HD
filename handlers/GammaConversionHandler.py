#########################################################################
##    Collects results for Gamma to DiLepton Conversion Test           ##
##                                                                     ##
##    @author  :   K. Zarebski                                         ##
##    @date    :   last modified 2018-03-23                            ##
#########################################################################

import os
import re
import json
from .BaseHandler import BaseHandler
import logging


class GammaConversionHandler(BaseHandler):
    def __init__(self, debug="INFO"):
        super(self.__class__, self).__init__()

    def collectResults(self, directory):
        _gamma_dilepton_file = (
            "GammaToDiLeptonConversionTest.root",
            os.path.join(
                directory,
                "G4GammaCVTestROOTFiles",
                "G4GammaToDiLeptonConversionTest.root",
            ),
        )
        self.saveFile(*_gamma_dilepton_file)
