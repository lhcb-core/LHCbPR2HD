import json
import os
import logging as log

from .BaseHandler import BaseHandler


class BasicPrmonMetrics(BaseHandler):
    def __init__(self):
        super(self.__class__, self).__init__()

    def collectResults(self, directory):
        prmon_json_path = os.path.join(directory, "prmon.json")
        prmon_txt_path = os.path.join(directory, "prmon.txt")

        json_data = self.loadPrmonJson(prmon_json_path)
        txt_data = self.loadPrmonTxt(prmon_txt_path)

        # store json data
        self.saveJSON(name="summary", data=json_data, group="summary")
        self.saveJSON(name="profile", data=txt_data, group="profile")

        # store files
        self.saveFile("prmon_json", prmon_json_path)
        self.saveFile("prmon_txt", prmon_txt_path)

    def loadPrmonJson(self, file_path: str):
        try:
            with open(file_path, "r") as file:
                return json.load(file)
        except FileNotFoundError:
            log.error(f"failed to locate {file_path}")
            raise
        except json.JSONDecodeError:
            log.error(f"failed to decode json {file_path}")
            raise

    def loadPrmonTxt(self, file_path: str):
        data = {}
        try:
            with open(file_path, "r") as file:
                lines = file.readlines()
                # ignore the first data column which is a time stamp
                headers = lines[0].lower().strip().split()[1:]
                num_cols = len(headers)

                for h in headers:
                    data[h] = []

                for line in lines[1:]:
                    vals = line.strip().split()[1:]
                    if len(vals) != num_cols:
                        raise ValueError(f"inconsistent data: {line.strip()}")
                    for i, h in enumerate(headers):
                        data[h].append(vals[i])

            return data

        except FileNotFoundError:
            log.error(f"failed to locate {file_path}")
            raise
        except ValueError as e:
            log.error(f"{e}")
            raise
