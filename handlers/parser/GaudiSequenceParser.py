import os
"""
The GaudiSequenceParser parses a Gaudi log file generated with the
'--printsequence' option. The data structure contains the function class,
function name and 'indentation level'. The selectAlgorithms function takes a
list of full algorithm names as option. It then goes through the sequence and
returns those algorithms which are 'below' those of the arguments passed.
"""


class GaudiSequenceParser:
    def __init__(
            self,
            dir,
            file="run.log",
            startpattern="*" * 30 + " Algorithm Sequence " + "*" * 28,
            endpattern="*" * 78,
    ):
        self.directory = dir
        self.file = file
        self.startpattern = startpattern
        self.endpattern = endpattern
        self.sequence = []
        self.algorithms = []

    def parseGaudiSequence(self):
        # parse a gaudi log produced with --printsequence option
        fh = open(self.directory + os.sep + self.file)
        startsequence = False
        for line in fh.readlines():
            if startsequence:
                if line.find(self.endpattern) != -1:
                    break
                algo = line.split("SUCCESS")[-1]
                indent = (algo.count(" ") - 1) / 5
                algo = algo.strip()
                (algocl, algoname) = algo.split("/")
                self.sequence.append({
                    "indent": indent,
                    "algo_class": algocl,
                    "algo_name": algoname
                })
            elif line.find(self.startpattern) != -1:
                startsequence = True

    def selectAlgorithms(self, algoselect):
        if not self.sequence:
            self.parseGaudiSequence()

        indent = 0
        startselect = False
        for algo in self.sequence:
            algofull = "%s/%s" % (algo["algo_class"], algo["algo_name"])
            if startselect:
                if algo["indent"] <= indent:
                    startselect = False
                else:
                    self.algorithms.append(algo)
            if not startselect and algofull in algoselect:
                indent = algo["indent"]
                startselect = True
        return self.algorithms

    def getSequence(self):
        return self.sequence

    def getAlgorithms(self):
        return self.algorithms

    def run(self):
        self.parseGaudiSequence()
        print(self.sequence)


if __name__ == "__main__":
    GaudiSequenceParser().run()
