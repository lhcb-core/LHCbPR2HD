import os
import json

from .BaseHandler import BaseHandler


class GeantTestEm5Handler(BaseHandler):
    def __init__(self):
        super(self.__class__, self).__init__()

    def CheckFile(self, _file):
        if not os.path.exists(_file):
            raise Exception("File %s does not exist" % _file)

    def ReadTable(self, _file):
        with open(_file, mode="r") as f:
            AllLines = f.readlines()
            AllLines.pop(0)
            for Line in AllLines:
                Column = Line.split(",")
                self.saveFloat(
                    "TESTEM5_{energy}MeV RMS".format(energy=Column[0]),
                    float(Column[1]),
                    description=
                    "Multiple Scattering test: RMS of electron scattering angle at {energy} MeV"
                    .format(energy=Column[0]),
                )
                self.saveFloat(
                    "TESTEM5_{energy}MeV RMS Uncertainty".format(
                        energy=Column[0]),
                    float(Column[2].strip("\n")),
                    description=
                    "Multiple Scattering test: Uncertainty on RMS of electron scattering angle at {energy} MeV"
                    .format(energy=Column[0]),
                )

    def collectResults(self, directory):
        ResultsTable = os.path.join(directory, "G4MScInThinLayerTestOutput",
                                    "Results_Table.txt")

        self.CheckFile(ResultsTable)

        self.ReadTable(ResultsTable)

        ResultsFile = os.path.join(directory, "G4MScInThinLayerTestOutput",
                                   "RMSResults.root")

        self.saveFile(
            "TESTEM5_RMSResults.root",
            ResultsFile,
            description=
            "Multiple Scattering test: File containing Distributions of RMS at each energy and graph of RMS vs. Energy",
        )
