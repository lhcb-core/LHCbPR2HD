import re, sys, os, shutil, json, glob
from .BaseHandler import BaseHandler
from xml.etree.ElementTree import ElementTree
import lxml.etree as etree

# True or False
global DEBUG
DEBUG = True


#################################################################################
# search "pattern" on "line", if not found return "default"
# if DEBUG and "name" are define print: "name: value"
def grepPattern(pattern, line, default=None, name=""):
    result = default
    resultobject = re.search(pattern, line)
    if resultobject != None:
        tmp = resultobject.groups()
        if len(tmp) == 1:
            result = tmp[0]
        else:
            result = tmp
        if DEBUG and name:
            print("[grepPattern] %s: %s" % (name, result))
    else:
        print("WARNING: attribute %s was not found!" % name)
    return result


#################################################################################


class booleValidation(BaseHandler):
    def __init__(self):
        super(self.__class__, self).__init__()

    def collectResults(self, directory):
        logfile = os.path.join(directory, "run.log")
        rootfile = grepPattern(
            "RootHistSvc\s.*INFO Writing ROOT histograms to: (\S+)",
            open(logfile, "r", encoding="ISO-8859-1").read(),
        )
        rootfullname = os.path.join(directory, rootfile)

        if os.path.isfile(rootfullname) == 0:
            raise Exception(
                "Could not locate histo file: %s in the given directory" %
                rootfile)

        fileName, fileExtension = os.path.splitext(rootfile)
        self.saveFile(fileName, rootfullname)
