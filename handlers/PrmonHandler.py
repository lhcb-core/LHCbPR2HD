import os
import json
import subprocess
from .BaseHandler import BaseHandler


class PrmonHandler(BaseHandler):
    def __init__(self):
        super(self.__class__, self).__init__()

    def collectResults(self, directory):
        self.saveFile("prmon.json", os.path.join(directory, "prmon.json"))

        with open(os.path.join(directory, "prmon.json")) as infile:
            prmon_values = json.load(infile)
            self.saveInt(
                "vmem_max",
                prmon_values["Max"]["vmem"],
                description="maximum of virtual memory in kb",
                group="vmem",
            )
            self.saveInt(
                "vmem_avg",
                prmon_values["Avg"]["vmem"],
                description="average of virtual memory in kb",
                group="vmem",
            )
            self.saveInt(
                "rss_max",
                prmon_values["Max"]["rss"],
                description="maximum of resident set size in kb",
                group="rss",
            )
            self.saveInt(
                "rss_avg",
                prmon_values["Avg"]["rss"],
                description="average of resident set size in kb",
                group="rss",
            )
            self.saveInt(
                "pss_max",
                prmon_values["Max"]["pss"],
                description="maximum of proportional set size in kb",
                group="pss",
            )
            self.saveInt(
                "pss_avg",
                prmon_values["Avg"]["pss"],
                description="average of proportional set size in kb",
                group="pss",
            )

        subprocess.call(
            "prmon_plot.py --input  " + os.path.join(directory, "prmon.txt") +
            " --xvar wtime --yvar vmem,pss,rss,swap",
            shell=True,
        )
        self.saveFile(
            "PrMon_wtime_vs_vmem_pss_rss_swap.png",
            "PrMon_wtime_vs_vmem_pss_rss_swap.png",
        )

        subprocess.call(
            "prmon_plot.py --input  " + os.path.join(directory, "prmon.txt") +
            " --xvar wtime --yvar vmem,pss,rss,swap --diff",
            shell=True,
        )
        self.saveFile(
            "PrMon_wtime_vs_diff_vmem_pss_rss_swap.png",
            "PrMon_wtime_vs_diff_vmem_pss_rss_swap.png",
        )

        subprocess.call(
            "prmon_plot.py --input  " + os.path.join(directory, "prmon.txt") +
            " --xvar wtime --yvar utime,stime --diff --stacked",
            shell=True,
        )
        self.saveFile("PrMon_wtime_vs_diff_utime_stime.png",
                      "PrMon_wtime_vs_diff_utime_stime.png")
