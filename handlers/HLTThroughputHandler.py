import os, sys, re
from .BaseHandler import BaseHandler
from .hlt.HLTPerfParser import process_directory


class HLTThroughputHandler(BaseHandler):
    def __init__(self):
        super(self.__class__, self).__init__()
        self.finished = False
        self.results = []

    RE_NBWERROR = "\s*(\w+\s*\w+).*\s+([0-9\.]+)\s\+\-\s([0-9\.]+)"

    def collectResults(self, directory):
        with open(os.path.join(directory, "averages.log")) as f:
            for line in f.readlines():
                metric = re.match(self.RE_NBWERROR, line)
                if metric != None:
                    self.saveFloat(
                        metric.group(1).replace(" ", "_"),
                        metric.group(2),
                        group="HLTThroughput",
                    )
                    self.saveFloat(
                        metric.group(1).replace(" ", "_") + "_error",
                        metric.group(3),
                        group="HLTThroughput",
                    )

        try:
            collected_results = process_directory(directory)

            if "Hlt1" in collected_results["timing_results"]:
                prefix = "HLTPerfHlt1_"
            else:
                prefix = "HLTPerfHlt2_"

            for k, v in list(collected_results.items()):
                if k == "timing_results":
                    for k2, v2 in list(v.items()):
                        self.saveJSON(prefix + str(k2), v2)
                else:
                    self.saveJSON(prefix + str(k), v)

        except Exception as err:
            print("ERROR: in new '%s' code:\n%s\n" % (self.__class__.__name__,
                                                      str(err)))
            pass
