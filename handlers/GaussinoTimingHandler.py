import re
from os import path

from .BaseHandler import BaseHandler


class PatternItem:
    def __init__(self, pattern: str, name=""):
        self.pattern = pattern
        self.name = name
        self.found = False


class GaussinoTimingHandler(BaseHandler):
    def __init__(self):
        super(self.__class__, self).__init__()

    def collectResults(self, directory):
        data = self.parseLog(log_path=path.join(directory, "run.log"))
        for key, val in data.items():
            self.saveFloat(name=key, data=val, group="Timing")

    def parseLog(self, log_path: str):
        """parses Gauss-on-Gaussino output log file and regex-matches timing values"""
        patterns = [
            PatternItem(
                pattern=
                r"^HiveSlimEventLo...\s+INFO[\s\w\W]+?total time ([\d\.]+?)$",
                name="HiveSlimEventLoopMgr_totaltime",
            ),
            PatternItem(
                pattern=r"^GenRndInit\s+INFO ([\d\.]+?) events processed$",
                name="GenRndInit_events",
            ),
            PatternItem(
                pattern=
                r"^GenRndInit\s+INFO ([\d\.]+?) Gaudi threads were used$",
                name="GenRndInit_threads",
            ),
            PatternItem(
                pattern=
                r"^GenRndInit\s+INFO Measured event loop time \[ns\]: (\d.+?\d)$",
                name="GenRndInit_looptime",
            ),
            PatternItem(
                pattern=r"^GenRndInit\s+INFO Time per event \[s\]: (\d.+?\d)$",
                name="GenRndInit_eventtime",
            ),
            PatternItem(
                pattern=
                r"^TimingAuditor.T...\s+INFO ([\s\w]+?)\s*\|([\d\s\.]+?)\|([\d\s\.]+?)\|([\d\s\.]+?)\|([\d\s\.]+?)\|([\d\s\.]+?)\|.*",
                name="",
            ),
        ]

        data = {}
        with open(log_path, "r", errors="ignore") as f:
            for ln in f:
                for p in patterns:
                    if p.found:
                        continue

                    mtch = re.match(p.pattern, ln)
                    if not mtch:
                        continue

                    if len(mtch.groups()) == 1:
                        data[p.name] = float(mtch.group(1).strip())
                        p.found = True
                        break
                    elif len(mtch.groups()) > 1:
                        key = mtch.group(1).strip().replace(" ", "_")
                        if key == "GiGaAlg":
                            key = "MainSimulation"
                        data[key] = float(mtch.group(2).strip())

        data["overall_timing"] = round(data[patterns[0].name] / 10**9 / 60, 1)

        return data
