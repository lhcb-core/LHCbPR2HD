import os
import re

import subprocess
import logging

from .BaseHandler import BaseHandler
from collectRunResults import send_notification_mattermost

log = logging.getLogger(__name__)


class ThroughputScalingHandler(BaseHandler):
    def __init__(self):
        super(self.__class__, self).__init__()

    def collectResultsExt(
            self,
            directory,
            project,
            version,
            platform,
            hostname,
            cpu_info,
            memoryinfo,
            startTime,
            endTime,
            options,
    ):
        regex = re.compile(
            "Max reached throughput ([\d.]+) at ([\d]+) jobs with ([\d]+) threads"
        )

        if os.path.exists("lhcb-benchmark-scripts"):
            output = subprocess.check_output([
                "python",
                "./lhcb-benchmark-scripts/plotScaling.py",
                "--directory",
                directory,
            ])
        else:
            raise RuntimeError("./lhcb-benchmark-scripts does not exist,"
                               " can't run plotScaling.py")

        match = regex.search(output.decode())
        if match:
            maxThroughput = match.group(1)
            mtJobs = match.group(2)
            mtThreads = match.group(3)
        else:
            raise RuntimeError("can't find match in regex")

        self.saveFloat(
            "max_throughput",
            maxThroughput,
            description="maximum throughput",
            group="throughput",
        )

        # send plot to eos  as html
        wwwDirEos = os.environ.get("LHCBPR_WWW_EOS")
        if wwwDirEos is None:
            raise Exception("No web dir on EOS defined,"
                            " will not run extraction")
        else:
            dirname = ("Throughput_" + str(version) + "_" + str(options) + "_"
                       + str(platform) + "_" + startTime.replace(" ", "_"))

            html_code = (
                "<html>"
                "<head></head> "
                "<body> "
                "<p>"
                "slot.build_id: " + str(version) + "<br>"
                "platform: " + str(platform) + "<br>"
                "hostname: " + str(hostname) + "<br>"
                "options file: <a href='https://gitlab.cern.ch/lhcb-nightlies/Brunel/blob/"
                + str(version).replace(".", "/") +
                "/Rec/Brunel/python/upgrade_options/" + str(options).replace(
                    "Scaling", "") + ".py'>" + str(options) + "</a></p>"
                "<ul>"
                "  <li>Maximum throughput at " + mtJobs + " jobs with " +
                mtThreads + " threads = " + str(maxThroughput) + " Events/s" +
                "</li>"
                "</ul>"
                "<img src="
                "scalingTest.png"
                ">"
                "<p>Here's the <a href='scalingTest.pdf'>pdf</a> version.</p>"
                "</body>"
                "</html>")

            with open("index.html", "w") as html_file:
                html_file.write(html_code)

            targetRootEosDir = os.path.join(wwwDirEos, dirname)
            try:
                subprocess.call([
                    "xrdcp", "scalingTest.png",
                    targetRootEosDir + "/scalingTest.png"
                ])
                subprocess.call([
                    "xrdcp", "scalingTest.pdf",
                    targetRootEosDir + "/scalingTest.pdf"
                ])
                subprocess.call(
                    ["xrdcp", "index.html", targetRootEosDir + "/index.html"])
            except Exception as ex:
                log.warning("Error copying html files to eos: %s", ex)

            self.saveString(
                "throughput",
                "cern.ch/lhcbpr-hlt/PerfTests/UpgradeVelo/" + dirname +
                "scalingTest.png",
                description="link to throughput vs parallelisation plot",
                group="performance",
            )

            # harcoded info about machines
            # to be replaced by using cpu_info set in lbpr-get-command
            hostname = hostname.replace("lbhltperf01",
                                        "lbhltperf01 (Xeon E5-2630, avx2)")
            hostname = hostname.replace("lblhcbpr14",
                                        "lblhcbpr14 (Xeon Gold 6130, avx512)")

            # send notification on mattermost channel
            if "MATTERMOST_HOOK" in os.environ:
                content = (
                    "The results of latest throughput test [" +
                    str(options).replace(
                        "_", " ") + " " + str(platform) + " " + hostname +
                    "](https://cern.ch/lhcbpr-hlt/PerfTests/UpgradeVelo/" +
                    dirname + "):\n" + "`Maximum throughput = " +
                    str(maxThroughput) + " Events/s`")
                send_notification_mattermost(os.environ["MATTERMOST_HOOK"],
                                             content)
            else:
                log.warning("notifications not sent"
                            " because MATTERMOST_HOOK not set")
