import os
import fnmatch
from .utils import HashRemover
from .BaseHandler import BaseHandler


class HashRemoveHandler(BaseHandler):
    def __init__(self):
        super(self.__class__, self).__init__()

    def collectResults(self, directory):
        ext = "*.root"
        for root, _, files in os.walk(directory):
            for file in files:
                if fnmatch.fnmatch(file, ext):
                    modifier = HashRemover.HashRemover(
                        os.path.join(root, file))
                    modifier.modify_root_file()
