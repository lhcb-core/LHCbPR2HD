# -*- coding: utf-8 -*-

import os
import re
import json
from .BaseHandler import BaseHandler


class DetailedTimingInVolumesHandler(BaseHandler):
    def __init__(self):
        super(self.__class__, self).__init__()
        self.resulting_data = {}

    # Read in the log file and return its content as a string
    def read_logfile(self, log_file=None):
        if not os.path.exists(log_file):
            raise Exception("File %s does not exist" % log_file)

        with open(log_file, mode="r") as log_file_handler:
            log_data = ""
            for line in log_file_handler:
                log_data += line
            return log_data

    # Parse the log data and return an array of lists of tuples
    def parse_log_data(self, log_data=None):
        resulting_data = {}

        # Match the blocks of data in the log
        matched_log_blocks = re.finditer(
            "\*(.+?)\n\n", log_data, flags=re.DOTALL)
        if matched_log_blocks:
            for match in matched_log_blocks:
                data_block_title = ""
                output_data_block = []

                imput_data_block = match.group(1).split("\n")
                for block_line in imput_data_block:
                    # A new block with a title
                    if block_line.startswith("*"):
                        match_block_title = re.search(
                            "\*\s(.+?)\s\*", block_line, flags=re.IGNORECASE)
                        if match_block_title:
                            data_block_title = (
                                match_block_title.group(1).lower().replace(
                                    " ", "_").split("_(", 1)[0])
                    # Data within a block
                    else:
                        # Volume or Process section
                        if block_line.lower().startswith(
                                "volume") or block_line.lower().startswith(
                                    "process"):
                            block_contents_match = re.search(
                                ".+?: (.+?) cumulated time (.+?) seconds",
                                block_line,
                                flags=re.IGNORECASE,
                            )
                            if block_contents_match:
                                output_data_block.append([
                                    block_contents_match.group(1).lower(),
                                    block_contents_match.group(2),
                                ])
                        # Timing per particle in specific detectors section
                        elif data_block_title.startswith(
                                "timing_per_particle"):
                            block_contents_match = re.search(
                                "(.+?)\s+?: cumulated time (.+?) seconds \((.+?)\)",
                                block_line,
                                flags=re.IGNORECASE,
                            )
                            if block_contents_match:
                                output_data_block.append([
                                    block_contents_match.group(1).lower(),
                                    block_contents_match.group(2),
                                    block_contents_match.group(3),
                                ])
                            # Cumulative record of the section
                            elif block_line.lower().startswith("time in"):
                                block_contents_match = re.search(
                                    "Time in (.+?): (.+?) seconds \((.+?)\s",
                                    block_line,
                                    flags=re.IGNORECASE,
                                )
                                if block_contents_match:
                                    output_data_block.append([
                                        block_contents_match.group(1).lower(),
                                        block_contents_match.group(2),
                                        block_contents_match.group(3),
                                    ])
                        # Summary section
                        elif data_block_title.startswith("summary"):
                            block_contents_match = re.search(
                                "Total time in (.+?): (.+?) seconds \((.+?)\s",
                                block_line,
                                flags=re.IGNORECASE,
                            )
                            if block_contents_match:
                                output_data_block.append([
                                    block_contents_match.group(1).lower(),
                                    block_contents_match.group(2),
                                    block_contents_match.group(3),
                                ])
                        # Other volumes section
                        elif data_block_title.startswith("other"):
                            block_contents_match = re.search(
                                "(.+?): (.+?)$",
                                block_line,
                                flags=re.IGNORECASE)
                            if block_contents_match:
                                output_data_block.append([
                                    block_contents_match.group(1).lower(),
                                    block_contents_match.group(2).lower(),
                                ])

                    resulting_data[data_block_title] = output_data_block

        return resulting_data

    def collectResults(self, directory):
        log_file = os.path.join(directory, "Timing.log")
        resulting_data = self.parse_log_data(self.read_logfile(log_file))
        self.saveJSON(
            "detailed_timing_in_volumes",
            resulting_data,
            "Full information on detailed timing in volumes",
            "detailed_timing_in_volumes",
        )


if __name__ == "__main__":
    dtvh = DetailedTimingInVolumesHandler()

# EOF
