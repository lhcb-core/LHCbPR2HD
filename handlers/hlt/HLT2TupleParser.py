#!/bin/env python
import os, sys, math, re, ROOT, json
from array import array


def rate(scale, numer, denom):
    eff = float(numer) / float(denom)
    err = math.sqrt((eff - eff**2) / denom)
    return [scale * eff, scale * err]


def isTurbo(name):
    if re.match("Hlt2.(?!.*?TurboCalib).*Turbo", name, flags=0):
        return 1
    else:
        return 0


def isTurboCalib(name):
    if re.match("Hlt2.*TurboCalib", name, flags=0):
        return 1
    else:
        return 0


def isFull(name):
    if re.match("Hlt2.(?!.*?Turbo).(?!.*?Calib)", name, flags=0):
        return 1
    else:
        return 0


from .StreamDefs import _Streams

USED_STREAMS = ["AllStreams", "Modules"]


def populateDicts(Streams, decisions, branches, branch_names, persistRecoLines,
                  nHlt1, nHlt2):
    for OPT in USED_STREAMS:
        Streams[OPT] = {}
        for Mod in _Streams[OPT]:
            Streams[OPT][Mod[0]] = {
                "name": Mod[1],
                "pass_current": 0,
                "rate_unique": 0,
                "passed_unique": 0,
                "passed_TurboCalib": 0,
                "passed_Turbo": 0,
                "passed_Full": 0,
                "Bytes_incl": 0,
                "lines": 0,
                "lines_Turbo": 0,
                "lines_TurboCalib": 0,
                "lines_Full": 0,
                "rate": 0,
                "rate_err": 0,
                "rate_Turbo": 0,
                "rate_Full": 0,
                "rate_TurboCalib": 0,
                "passed": 0,
            }

    for branch in branches:
        branchName = branch.GetName()
        if not branchName.startswith("Hlt2"):
            continue  ## since we now have branches other than just HLT2 lines

        branchName = branchName.replace("Decision", "")

        branch_names.append(branchName)
        decisions[branchName] = {
            "passed": 0,
            "passed_unique": 0,
            "hlt1processed": nHlt1,
            "hlt2processed": nHlt2,
            "Bytes_incl": 0.0,
            "BytesPR_incl": 0.0,
            "persistReco": int(branchName in persistRecoLines),
            "rate": 0,
            "rate_err": 0,
            "rate_unique": 0,
            "rate_unique_err": 0,
        }

    #### overlap between lines and streams
    for k, v in list(decisions.items()):
        for Stream in [
                "Turbo",
                "Full",
                "TurboCalib",
                "TurboAndFull",
                "TurboAndTurboCalib",
                "FullAndTurboCalib",
                "TurboAndNotFull",
        ]:
            v["passed_" + Stream] = 0.0
            v["rate_" + Stream] = 0.0
            v["rate_err_" + Stream] = 0.0
            v["passed_unique_" + Stream] = 0.0
            v["rate_unique_" + Stream] = 0.0
            v["rate_unique_err_" + Stream] = 0.0

    ##############################################################
    ## remove turbo duplicates ##
    ##############################################################
    # Get Duplicates
    TurboDuplicates = []
    for line in list(decisions.keys()):
        for other_line in list(decisions.keys()):
            if not line == other_line:
                if other_line == line + "Turbo":
                    TurboDuplicates.append(line)
    # Remove Duplicates
    for TurboDuplicate in TurboDuplicates:
        del decisions[TurboDuplicate]


def prepareMatrix(RateMatrix, RateMatrixDict, RateMatrixLines,
                  RateMatrixLineNames):
    ##############################################################
    ## prepare the 2D matrix ##
    ##############################################################
    OPTION = "AllStreams"
    for regex in _Streams[OPTION]:
        RateMatrixLines.append(regex[0])
        RateMatrixLineNames.append(regex[1])

    for ModuleX in RateMatrixLines:
        row = []
        for ModuleY in RateMatrixLines:
            row.append(0)
        RateMatrix.append(row)


def determineUnaccounted(Streams, decisions):
    ##############################################################
    ## how many lines not accounted for in regex ##
    ##############################################################
    unaccounted = {}
    for S in USED_STREAMS:
        accountedLines = []
        for line in list(decisions.keys()):
            accounted = False
            for Module, Props in list(Streams[S].items()):
                if re.match(Module, line, flags=0):
                    Streams[S][Module]["lines"] += 1
                    Streams[S][Module]["lines_Turbo"] += isTurbo(line)
                    Streams[S][Module]["lines_Full"] += isFull(line)
                    Streams[S][Module]["lines_TurboCalib"] += isTurboCalib(
                        line)
                    if not Props["name"] == "ALL":
                        accounted = True
                        accountedLines.append(line)
        unaccounted[S] = set(decisions.keys()) - set(accountedLines)

    return unaccounted


def countEvents(
        Streams,
        decisions,
        RateMatrix,
        RateMatrixDict,
        RateMatrixLines,
        RateMatrixLineNames,
        treeHlt2,
        nHlt1,
):
    ##########################################################
    ######### EVENT LOOP #####################################
    ##########################################################

    # Set Addresses so we can read events in from TTree
    variables = {}
    # Loop over HLT2 Lines
    for line in list(decisions.keys()):
        variables[line] = array("I", [0])
        treeBranchName = line
        treeHlt2.SetBranchAddress(treeBranchName, variables[line])
    # variables["TurboEventSize"] = array('f',[0])
    # variables["FullEventSize"] = array('f',[0])
    # variables["persistRecoEventSize"] = array('f',[0])
    for b in ["TurboEventSize", "FullEventSize", "persistRecoEventSize"]:
        variables[b] = array("f", [0])
        treeHlt2.SetBranchAddress(b, variables[b])

    # Global counters
    iEntry = 0
    TotalBytes = 0.0
    TotalBytesTurbo = 0.0
    TotalBytesTurboCalib = 0.0
    TotalBytesFull = 0.0
    passedTurboAndNotFull = 0
    passedTurboAndFull = 0
    passedTurboAndTurboCalib = 0
    passedFullAndTurboCalib = 0
    passedGlobal = 0
    passedTurbo = 0
    passedTurboCalib = 0
    passedFull = 0

    while treeHlt2.GetEntry(iEntry):
        iEntry += 1
        if iEntry % 1000 == 0:
            print("processed %s/%s" % (iEntry, treeHlt2.GetEntries()))

        ### work with this
        persistRecoThisEvent = False

        for S in USED_STREAMS:
            for k, Module in list(Streams[S].items()):
                Module["pass_current"] = 0
                Module["pass_current_Turbo"] = 0
                Module["pass_current_Full"] = 0
                Module["pass_current_TurboCalib"] = 0

        linesFired = 0
        linesFired_Turbo = 0
        linesFired_TurboCalib = 0
        linesFired_Full = 0

        for decision, this_decision in list(decisions.items()):
            decThisLine = variables[decision][0]
            if decThisLine == 1:
                if "Turbo" in decision:
                    this_decision["Bytes_incl"] += variables["TurboEventSize"][
                        0]
                    if this_decision["persistReco"]:
                        persistRecoThisEvent = True
                        this_decision["Bytes_incl"] += variables[
                            "persistRecoEventSize"][0]
                        this_decision["BytesPR_incl"] += variables[
                            "persistRecoEventSize"][0]
                else:
                    this_decision["Bytes_incl"] += variables["FullEventSize"][
                        0]
                this_decision["passed"] += 1
                linesFired += 1

                if not decision == "Hlt2Lumi" and not "Global" in decision:
                    linesFired_Turbo += isTurbo(decision)
                    linesFired_TurboCalib += isTurboCalib(decision)
                    linesFired_Full += isFull(decision)

                for S in USED_STREAMS:
                    for key, value in list(Streams[S].items()):
                        if key[1] == "ALL":
                            value["pass_current"] = 1
                    # matched = False
                    for Module, ThisModule in list(Streams[S].items()):
                        if re.match(Module, decision, flags=0):
                            ThisModule["pass_current"] = 1
                            if isTurbo(decision):
                                ThisModule["pass_current_Turbo"] = 1
                            if isFull(decision):
                                ThisModule["pass_current_Full"] = 1
                            if isTurboCalib(decision):
                                ThisModule["pass_current_TurboCalib"] = 1

        ### end loop over decisions
        if linesFired > 0:
            passedGlobal += 1
        if linesFired_Turbo > 0:
            passedTurbo += 1
        if linesFired_TurboCalib > 0:
            passedTurboCalib += 1
        if linesFired_Full > 0:
            passedFull += 1
        ### data rate

        if linesFired_Full > 0:
            TotalBytesFull += variables["FullEventSize"][0]
            TotalBytes += variables["FullEventSize"][0]
        if linesFired_TurboCalib > 0:
            ### probably need to count the cost of the turbo banks too
            TotalBytesTurboCalib += variables["FullEventSize"][0]
            TotalBytes += variables["FullEventSize"][0]
        if linesFired_Turbo > 0:
            TotalBytesTurbo += variables["TurboEventSize"][0]
            TotalBytes += variables["TurboEventSize"][0]
            if persistRecoThisEvent:
                TotalBytesTurbo += variables["persistRecoEventSize"][0]
                TotalBytes += variables["persistRecoEventSize"][0]

        ### overlaps
        if linesFired_Turbo > 0 and linesFired_Full == 0:
            passedTurboAndNotFull += 1
        if linesFired_Turbo > 0 and linesFired_Full > 0:
            passedTurboAndFull += 1
        if linesFired_Turbo > 0 and linesFired_TurboCalib > 0:
            passedTurboAndTurboCalib += 1
        if linesFired_Full > 0 and linesFired_TurboCalib > 0:
            passedFullAndTurboCalib += 1

        ### count unique decisions for this line
        for decision in decisions:
            decThisLine = variables[decision][0]
            if decThisLine == 1 and linesFired == 1:
                decisions[decision]["passed_unique"] += 1

        ### count decisisions overlaping with streams
        passedStreams = {
            "Turbo":
            linesFired_Turbo > 0,
            "TurboCalib":
            linesFired_TurboCalib > 0,
            "Full":
            linesFired_Full > 0,
            "TurboAndTurboCalib":
            linesFired_Turbo > 0 and linesFired_TurboCalib > 0,
            "FullAndTurboCalib":
            linesFired_Full > 0 and linesFired_TurboCalib > 0,
            "TurboAndFull":
            linesFired_Turbo > 0 and linesFired_Full > 0,
            "TurboAndNotFull":
            linesFired_Turbo > 0 and linesFired_Full == 0,
        }

        for decision in decisions:
            decThisLine = variables[decision][0]
            for k, v in list(passedStreams.items()):
                if decThisLine == 1 and v == True:
                    decisions[decision]["passed_" + k] += 1

        for S in USED_STREAMS:
            for Module, ThisModule in list(Streams[S].items()):
                ThisModule["passed"] += ThisModule["pass_current"]
                ThisModule["passed_Turbo"] += ThisModule["pass_current_Turbo"]
                ThisModule["passed_Full"] += ThisModule["pass_current_Full"]
                ThisModule["passed_TurboCalib"] += ThisModule[
                    "pass_current_TurboCalib"]

        for S in USED_STREAMS:
            for X in list(Streams[S].keys()):
                UNIQUE = Streams[S][X]["pass_current"] == 1
                for Y in list(Streams[S].keys()):
                    if (not Streams[S][Y]["name"] == "ALL" and X != Y
                            and Streams[S][Y]["pass_current"] == 1):
                        UNIQUE = False
                if UNIQUE:
                    Streams[S][X]["passed_unique"] += 1

        for S in ["AllStreams"
                  ]:  ## since the rate matrix is only set up for this
            for X in range(0, len(RateMatrixLines)):
                name = RateMatrixLineNames[X]
                line = RateMatrixLines[X]
                for Y in range(0, len(RateMatrixLines)):
                    name2 = RateMatrixLineNames[Y]
                    line2 = RateMatrixLines[Y]
                    if name not in RateMatrixDict:
                        RateMatrixDict[name] = {}
                    if name2 not in RateMatrixDict[name]:
                        RateMatrixDict[name][name2] = 0.0
                    if all([
                            True
                            if Streams[S][_]["pass_current"] == 1 else False
                            for _ in (line, line2)
                    ]):
                        RateMatrixDict[name][name2] += 1.0

                RateMatrixDict["lines"] = line

            for name in RateMatrixDict:
                if name in ["All", "Rate", "Unique", "lines"]:
                    continue
                for name2 in RateMatrixDict[name]:
                    if name2 in ["All", "Rate", "Unique", "lines"]:
                        continue
                    rate = float(
                        RateMatrixDict[name][name2]) * 1.0e3 / float(nHlt1)
                    norm = float(
                        RateMatrixDict[name][name]) * 1.0e3 / float(nHlt1)
                    if norm <= 0.0:
                        rate = 0.0
                    else:
                        rate /= norm
                    RateMatrixDict[name][name2] = rate * 100.0

            for X in range(0, len(RateMatrixLines)):
                for Y in range(0, len(RateMatrixLines)):
                    if (Streams[S][RateMatrixLines[X]]["pass_current"] == 1
                            and Streams[S][
                                RateMatrixLines[Y]]["pass_current"] == 1):
                        RateMatrix[X][Y] += 1

    ##########################################################
    ######### END EVENT LOOP #####################################
    ##########################################################

    stats = {}
    stats["passedGlobal"] = passedGlobal
    stats["passedTurbo"] = passedTurbo
    stats["passedTurboAndNotFull"] = passedTurboAndNotFull
    stats["passedTurboAndFull"] = passedTurboAndFull
    stats["passedTurboAndTurboCalib"] = passedTurboAndTurboCalib
    stats["passedFullAndTurboCalib"] = passedFullAndTurboCalib
    stats["passedTurboCalib"] = passedTurboCalib
    stats["passedFull"] = passedFull
    stats["TotalBytes"] = TotalBytes
    stats["TotalBytesTurbo"] = TotalBytesTurbo
    stats["TotalBytesTurboCalib"] = TotalBytesTurboCalib
    stats["TotalBytesFull"] = TotalBytesFull

    return stats


def initRateDict(decisions, stats, nHlt1, nHlt2):
    for G in [
        ["Total", stats["passedGlobal"]],
        ["Total_Turbo", stats["passedTurbo"]],
        ["Total_TurboAndNotFull", stats["passedTurboAndNotFull"]],
        ["Total_TurboAndFull", stats["passedTurboAndFull"]],
        ["Total_TurboAndTurboCalib", stats["passedTurboAndTurboCalib"]],
        ["Total_FullAndTurboCalib", stats["passedFullAndTurboCalib"]],
        ["Total_TurboCalib", stats["passedTurboCalib"]],
        ["Total_Full", stats["passedFull"]],
    ]:
        decisions[G[0]] = {
            "passed": G[1],
            "passed_unique": 0,
            "hlt1processed": nHlt1,
            "hlt2processed": nHlt2,
            "Bytes_incl": 0.0,
            "BytesPR_incl": 0.0,
            "rate": 0,
            "rate_err": 0,
            "rate_unique": 0,
            "rate_unique_err": 0,
        }

        this_decision = decisions[G[0]]

        for S in [
                "Turbo",
                "Full",
                "TurboCalib",
                "TurboAndFull",
                "TurboAndTurboCalib",
                "FullAndTurboCalib",
                "TurboAndNotFull",
        ]:
            this_decision["passed_" + S] = 0.0
            this_decision["rate_" + S] = 0.0
            this_decision["rate_err_" + S] = 0.0
            this_decision["passed_unique_" + S] = 0.0
            this_decision["rate_unique_" + S] = 0.0
            this_decision["rate_err_unique_" + S] = 0.0


def calculateRates(Streams, decisions, stats, nHlt1, nHlt2, inputrate):
    initRateDict(decisions, stats, nHlt1, nHlt2)

    decisions["Total"]["Bytes_incl"] = stats["TotalBytes"]
    decisions["Total_Turbo"]["Bytes_incl"] = stats["TotalBytesTurbo"]
    decisions["Total_TurboCalib"]["Bytes_incl"] = stats["TotalBytesTurboCalib"]
    decisions["Total_Full"]["Bytes_incl"] = stats["TotalBytesFull"]

    nHlt1 = float(nHlt1)
    nHlt2 = float(nHlt2)

    for key, value in list(decisions.items()):
        nPassed = float(value["passed"])

        MegaBytes_total = 1.0e-6 * value["Bytes_incl"]
        MegaBytesPR_total = 1.0e-6 * value["BytesPR_incl"]  # PersistReco part
        value["MBs_incl"] = inputrate * MegaBytes_total / nHlt1
        value["kBe_incl"] = 0.0  ## average kB/event
        value["kBePR_incl"] = 0.0  ## average kB/event (persistReco)
        if nPassed > 0.0:
            value["kBe_incl"] = 1.0e3 * MegaBytes_total / nPassed
            value["kBePR_incl"] = 1.0e3 * MegaBytesPR_total / nPassed

        for S in [
                "Turbo",
                "Full",
                "TurboCalib",
                "TurboAndFull",
                "TurboAndTurboCalib",
                "FullAndTurboCalib",
                "TurboAndNotFull",
        ]:
            eff = rate(inputrate, float(value["passed_" + S]), nHlt1)
            value["rate_" + S] = eff[0]
            value["rate_err_" + S] = eff[1]

        eff = nPassed / nHlt1
        eff_err = math.sqrt(eff * (1 - eff) / nHlt1)
        value["rate"] = inputrate * eff
        value["rate_err"] = inputrate * eff_err
        eff = float(value["passed_unique"]) / nHlt1
        eff_err = math.sqrt(eff * (1 - eff) / nHlt1)
        value["rate_unique"] = inputrate * eff
        value["rate_unique_err"] = inputrate * eff_err

    for S in USED_STREAMS:
        for key, value in list(Streams[S].items()):
            rate_incl = rate(inputrate, value["passed"], nHlt1)
            rate_excl = rate(inputrate, value["passed_unique"], nHlt1)

            value["rate"] = rate_incl[0]
            value["rate_err"] = rate_incl[1]
            value["rate_unique"] = rate_excl[0]
            value["rate_unique_err"] = rate_excl[1]

            for PhysicalStream in ["Turbo", "Full", "TurboCalib"]:
                R = rate(inputrate, value["passed_" + PhysicalStream], nHlt1)
                value["rate_%s" % PhysicalStream] = R[0]
                value["rate_%s_err" % PhysicalStream] = R[1]


def ParseHLT2Output(filename, filename2, inputrate=1.0e6):
    rootFile1 = ROOT.TFile(filename)
    rootFile2 = ROOT.TFile(filename2)

    treeHlt1 = rootFile1.Get("TupleHlt1")
    treeHlt2 = rootFile2.Get("TupleHlt2")
    branches = treeHlt2.GetListOfBranches()
    persistRecoLines = [
        l.GetName() for l in rootFile1.Get("TuplePR").GetListOfBranches()
    ]
    decisions = {}
    branch_names = []

    Streams = {}

    populateDicts(
        Streams,
        decisions,
        branches,
        branch_names,
        persistRecoLines,
        treeHlt1.GetEntries(),
        treeHlt2.GetEntries(),
    )

    RateMatrix = []
    RateMatrixDict = {}
    RateMatrixLines = []
    RateMatrixLineNames = []
    prepareMatrix(RateMatrix, RateMatrixDict, RateMatrixLines,
                  RateMatrixLineNames)

    unaccounted = determineUnaccounted(Streams, decisions)

    stats = countEvents(
        Streams,
        decisions,
        RateMatrix,
        RateMatrixDict,
        RateMatrixLines,
        RateMatrixLineNames,
        treeHlt2,
        treeHlt1.GetEntries(),
    )

    calculateRates(
        Streams,
        decisions,
        stats,
        treeHlt1.GetEntries(),
        treeHlt2.GetEntries(),
        inputrate,
    )

    results = {}
    results["unaccounted"] = unaccounted
    results["decisions"] = decisions
    results["persistRecoLines"] = persistRecoLines
    results["Streams"] = Streams
    results["RateMatrix"] = RateMatrix
    results["RateMatrixDict"] = RateMatrixDict
    results["RateMatrixLines"] = RateMatrixLines
    results["RateMatrixLineNames"] = RateMatrixLineNames

    return results


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print(
            "Usage:   %s fileNameHLT1.root (fileNameHLT2.root)" % sys.argv[0])
        sys.exit()

    filename = sys.argv[1]
    JSONDIR = os.path.abspath(os.path.dirname(filename))

    if len(sys.argv) == 2:
        results = ParseHLT2Output(filename, filename)
    elif len(sys.argv) == 3:
        filename2 = sys.argv[2]
        results = ParseHLT2Output(filename, filename2)

    for k, v in list(results["unaccounted"].items()):
        filename = os.path.join(JSONDIR, "%s.unaccounted.list" % k)
        with open(filename, "w") as _file:
            print("Writing: " + filename)
            for line in v:
                _file.write(line + "\n")

    ##### write all line rates to file
    with open(os.path.join(JSONDIR, "rates.list"), "w") as output:
        print("writing: " + output.name)
        AllRates = {}
        decisions = results["decisions"]
        for key, value in list(decisions.items()):
            AllRates[key] = value["rate"]

        for key, value in sorted(
                iter(list(AllRates.items())),
                key=lambda v_k: (v_k[1], v_k[0]),
                reverse=True):
            output.write(
                "%s\t%.0f+-%.0f\n" %
                (key, value, decisions[key]["rate_err"]))  # ["rate"]))

    ############ finally, write out everything to json files
    with open(os.path.join(JSONDIR, "persistRecoLines.json"), "w") as _file:
        print("writing: " + _file.name)
        json.dump(results["persistRecoLines"], _file)

    with open(os.path.join(JSONDIR, "decisions.json"), "w") as _file:
        print("writing: " + _file.name)
        json.dump(results["decisions"], _file)

    with open(os.path.join(JSONDIR, "Streams.json"), "w") as _file:
        print("writing: " + _file.name)
        json.dump(results["Streams"], _file)

    with open(os.path.join(JSONDIR, "RateMatrix.json"), "w") as _file:
        print("writing: " + _file.name)
        json.dump(results["RateMatrix"], _file)

    with open(os.path.join(JSONDIR, "RateMatrixDict.json"), "w") as _file:
        print("writing: " + _file.name)
        json.dump(results["RateMatrixDict"], _file)

    with open(os.path.join(JSONDIR, "RateMatrixLines.json"), "w") as _file:
        print("writing: " + _file.name)
        json.dump(results["RateMatrixLines"], _file)

    with open(os.path.join(JSONDIR, "RateMatrixLineNames.json"), "w") as _file:
        print("writing: " + _file.name)
        json.dump(results["RateMatrixLineNames"], _file)
    ############# the end
