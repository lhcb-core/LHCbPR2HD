#!/bin/env python
import os, sys, math, re, ROOT, json
from array import array

from .StreamDefs import _Streams

RegexList = [
    "Hlt1.*",
    "Hlt1LowMult.*",
    "Hlt1(Di|Multi)Muon.*",
    "Hlt1SingleMuon.*",
    "Hlt1.*Electron.*",
    "Hlt1B2.*",
    "Hlt1IncPhi.*",
    "Hlt1TrackMuon.*",
    "Hlt1.*TrackMVA.*",
    "Hlt1CalibTracking.*",
    "Hlt1DiProton.*",
    "OTHER",
]


def populateDicts(ByRegex, Hlt1Lines, treeHlt1):
    allHLT1Branches = treeHlt1.GetListOfBranches()

    ### POPULATE ByRegex

    # Add all regex groups to dict and populate branches for use later
    for RE in RegexList:
        ByRegex[RE] = {
            "hlt1processed": treeHlt1.GetEntries(),
            "PassEvent": False,
            "MatchingLines": [],
            ####
            "passed_inclusive": 0,
            "rate_inclusive": 0.0,
            "rate_inclusive_err": 0.0,
            "MBs_inclusive": 0,
            "kBe_inclusive": 0,
            ###
            "passed_exclusive": 0,
            "rate_exclusive": 0.0,
            "rate_exclusive_err": 0.0,
            "MBs_exclusive": 0,
            "kBe_exclusive": 0,
        }

    # Find all Lines which match each Regex expression and store their names
    for RE in list(ByRegex.keys()):
        for branch in allHLT1Branches:
            branchName = branch.GetName()
            # Skip branches non containing HLT1 Data
            if not branchName.startswith("Hlt1"):
                continue
            # Add branch name to this regex if it matches
            if re.match(RE, branchName, flags=0):
                ByRegex[RE]["MatchingLines"].append(branchName)

    # Make a set of all lines
    ByRegex["OTHER"]["MatchingLines"] = set(ByRegex["Hlt1.*"]["MatchingLines"])
    # Remove the lines we've seen elsewhere
    for RE in list(ByRegex.keys()):
        if not RE == "Hlt1.*" and not RE == "OTHER":
            ByRegex["OTHER"]["MatchingLines"] -= set(
                ByRegex[RE]["MatchingLines"])

    # Create a list of non matched lines
    ByRegex["OTHER"]["MatchingLines"] = list(ByRegex["OTHER"]["MatchingLines"])

    ### NOW POPULATE Hlt1Lines

    for branch in allHLT1Branches:
        branchName = branch.GetName()
        if not branchName.startswith("Hlt1"):
            continue
        Hlt1Lines[branchName] = {
            "passed": 0,
            "passed_unique": 0,
            "hlt1processed": treeHlt1.GetEntries(),
            "Bytes_incl": 0.0,
            "rate": 0,
            "rate_unique": 0,
            "rate_unique_err": 0,
            "rate_err": 0,
        }


def countEvents(ByRegex, Hlt1Lines, treeHlt1):
    ##########################################################
    ######### EVENT LOOP #####################################
    ##########################################################

    scaling_factor = 1

    # Set addresses for reading in data from TTree
    variables = {}
    # Do this for all HLT1 lines in the tuple
    for line in list(Hlt1Lines.keys()):
        variables[line] = array("I", [0])
        treeBranchName = line
        treeHlt1.SetBranchAddress(treeBranchName, variables[line])
    # Add special FullEventSize
    variables["FullEventSize"] = array("f", [0])
    treeHlt1.SetBranchAddress("FullEventSize", variables["FullEventSize"])

    # Global counters
    iEntry = 0
    TotalBytes = 0.0
    pass_any = 0

    # Read the next entry
    while treeHlt1.GetEntry(iEntry):
        iEntry += 1

        if iEntry % 1000 == 0:
            print("processed %s/%s" % (iEntry, treeHlt1.GetEntries()))

        this_pass_any = 0
        for RE in list(ByRegex.keys()):
            ByRegex[RE]["PassEvent"] = False

        # Check for any
        for Hlt1Line in list(Hlt1Lines.keys()):
            decThisLine = variables[Hlt1Line][0]
            # If this HLT line triggered
            if decThisLine == 1:
                # Add to the total event size counter
                Hlt1Lines[Hlt1Line]["Bytes_incl"] += variables[
                    "FullEventSize"][0]

                # Set PassEvent in the Regex group(s) that this HLT line matches
                for RE in list(ByRegex.keys()):
                    if re.match(RE, Hlt1Line, flags=0):
                        ByRegex[RE]["PassEvent"] = True

                # This event passed the trigger
                this_pass_any = 1
                Hlt1Lines[Hlt1Line]["passed"] += 1

                # Test to see if this event is unique to this HLT line
                is_unique = 1
                for other_Hlt1Line in list(Hlt1Lines.keys()):
                    if (not other_Hlt1Line == Hlt1Line
                            and variables[other_Hlt1Line][0] == 1):
                        is_unique = 0
                Hlt1Lines[Hlt1Line]["passed_unique"] += is_unique

        # Did this event pass any trigger?
        pass_any += this_pass_any
        if this_pass_any != 0:
            TotalBytes += variables["FullEventSize"][0]

        ###############
        ## REGEX #
        ###############
        ## how many of our regex fire in this event, excluding Hlt1.*?
        nRegexFired = 0
        for RE in list(ByRegex.keys()):
            if ByRegex[RE]["PassEvent"] == True and not RE == "Hlt1.*":
                nRegexFired += 1

        for RE in list(ByRegex.keys()):
            # is the event exclusive and/or inclusive?
            opts = {
                "inclusive":
                ByRegex[RE]["PassEvent"] == True,
                "exclusive":
                ByRegex[RE]["PassEvent"] == True and nRegexFired == 1
                and not RE == "Hlt1.*",
            }

            if RE == "OTHER":
                ## inclusive and exclusive are the same *by definition*
                opts["inclusive"] = nRegexFired == 0 and ByRegex["Hlt1.*"][
                    "PassEvent"]
                opts["exclusive"] = opts["inclusive"]
            # if RE == "Hlt1.*":
            #    print opts
            for opt in list(opts.keys()):
                if opts[opt] is True:
                    ByRegex[RE]["passed_" + opt] += 1
                    ByRegex[RE]["MBs_" + opt] += (
                        variables["FullEventSize"][0] / 2.0**20)
                    ByRegex[RE]["kBe_" + opt] += (
                        variables["FullEventSize"][0] / 2.0**10)

    ##########################################################
    ######### END EVENT LOOP #####################################
    ##########################################################

    stats = {}
    stats["pass_any"] = pass_any
    stats["TotalBytes"] = TotalBytes

    return stats


def binomErr(eff, N):
    return math.sqrt((eff * (1 - eff)) / N)


def calculateRates(ByRegex, Hlt1Lines, treeHlt1, stats, inputrate):
    pass_any = stats["pass_any"]
    TotalBytes = stats["TotalBytes"]

    Hlt1Lines["All"] = {
        "passed": pass_any,
        "passed_unique": 0,
        "hlt1processed": treeHlt1.GetEntries(),
        "Bytes_incl": TotalBytes,
        "rate": 0,
        "rate_unique": 0,
        "rate_unique_err": 0,
        "rate_err": 0,
    }

    nHlt1 = float(treeHlt1.GetEntries())

    for Hlt1LineName, Hlt1Line in list(Hlt1Lines.items()):
        MegaBytes_total = 1.0e-6 * Hlt1Line["Bytes_incl"]
        Hlt1Line["MBs_incl"] = MegaBytes_total * inputrate / nHlt1
        Hlt1Line["kBe_incl"] = 0.0
        if Hlt1Line["passed"] > 0:
            Hlt1Line["kBe_incl"] = (1.0e-3 * MegaBytes_total * inputrate /
                                    float(Hlt1Line["passed"]))
        this_rate = Hlt1Line["passed"] / nHlt1
        Hlt1Line["rate"] = inputrate * this_rate
        Hlt1Line["rate_err"] = inputrate * binomErr(this_rate, nHlt1)
        this_unique_rate = Hlt1Line["passed_unique"] / nHlt1
        Hlt1Line["rate_unique"] = inputrate * this_unique_rate
        Hlt1Line["rate_unique_err"] = inputrate * binomErr(
            this_unique_rate, nHlt1)

    for RE, ThisRegex in list(ByRegex.items()):
        for opt in ["inclusive", "exclusive"]:
            if ThisRegex["passed_" + opt] > 0:
                ThisRegex["kBe_" + opt] = ThisRegex["kBe_" + opt] / float(
                    ThisRegex["passed_" + opt])
            ThisRegex["MBs_" +
                      opt] = inputrate * ThisRegex["MBs_" + opt] / nHlt1

        this_rate = ThisRegex["passed_inclusive"] / nHlt1
        ThisRegex["rate_inclusive"] = inputrate * this_rate
        ThisRegex["rate_inclusive_err"] = inputrate * binomErr(
            this_rate, nHlt1)
        this_unique_rate = ThisRegex["passed_exclusive"] / nHlt1
        ThisRegex["rate_exclusive"] = inputrate * this_unique_rate
        ThisRegex["rate_exclusive_err"] = inputrate * binomErr(
            this_unique_rate, nHlt1)


def ParseHLT1Output(hlt1_filename, inputrate=1.0e6):
    rootFile = ROOT.TFile(hlt1_filename)
    treeHlt1 = rootFile.Get("TupleHlt1")

    Hlt1Lines = {}
    ByRegex = {}

    populateDicts(ByRegex, Hlt1Lines, treeHlt1)

    stats = countEvents(ByRegex, Hlt1Lines, treeHlt1)

    calculateRates(ByRegex, Hlt1Lines, treeHlt1, stats, inputrate)

    results = {}
    results["Hlt1Lines"] = Hlt1Lines
    results["ByRegex"] = ByRegex

    return results


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage:   %s fileNameHLT1.root" % sys.argv[0])
        sys.exit()

    filename = sys.argv[1]

    results = ParseHLT1Output(filename)

    JSONDIR = os.path.abspath(os.path.dirname(filename))

    with open(os.path.join(JSONDIR, "Hlt1Decisions.json"), "w") as _file:
        print("writing json file: " + _file.name)
        json.dump(results["Hlt1Lines"], _file)

    with open(os.path.join(JSONDIR, "Hlt1ByRegex.json"), "w") as _file:
        print("writing json file: " + _file.name)
        json.dump(results["ByRegex"], _file)
