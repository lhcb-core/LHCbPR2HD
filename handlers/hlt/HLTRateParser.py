#!/usr/bin/env python

from pyparsing import (
    LineEnd,
    Literal,
    Word,
    Suppress,
    LineStart,
    Optional,
    ZeroOrMore,
    OneOrMore,
    Group,
)

# Common literals and words
NL = LineEnd().suppress()
plus = Literal("+").suppress()
minus = Literal("-").suppress()
pipe = Literal("|").suppress()
kHz = Literal("kHz").suppress()
lb = Literal("(").suppress()
rb = Literal(")").suppress()
eq = Literal("=").suppress()
nbwitherr = Word(nums + ".") + plus + minus + Word(nums + ".")


def getPropertyMatcher(name, valType=None):
    """Returns pyparsing structure to match a line:
    Turbo rate = 0.01 kHz
    or
    Hlt2Global rate = (30.0+-17.0587221092)kHz
    """
    expr = literalMatcher(name)
    return expr + valType + Suppress(restOfLine)


def literalMatcher(sentence):
    tokens = sentence.split(" ")
    ltokens = [Suppress(Literal(l)) for l in tokens if l != ""]
    expr = None
    for l in ltokens:
        if expr == None:
            expr = l
        else:
            expr += l
    return expr


def getHLTRateParser():
    # Preparing the header
    commentLine = LineStart() + Suppress(Literal("-")) + restOfLine
    headerLine = restOfLine
    header = (Optional(ZeroOrMore("=")) +
              literalMatcher("HLT rate summary starts here") + Optional(
                  ZeroOrMore("=")))
    trailer = (Optional(ZeroOrMore("=")) +
               literalMatcher("HLT rate summary ends here") + Optional(
                   ZeroOrMore("=")))
    removedLines = (Literal("removed").suppress() +
                    Literal("lines").suppress() + restOfLine.suppress())
    nbevents = (
        Literal("processed:").suppress() +
        Word(nums).setResultsName("nbevents") + Literal("events").suppress())
    ratesAssume = (Literal("rates assume").suppress() +
                   Word(nums + ".").setResultsName("ratesAssume") +
                   Literal("Hz from level-0").suppress())

    getLinesnbMatcher = lambda x: Word(nums).setResultsName(x) + Literal(x).suppress()
    hlt1Lines = getLinesnbMatcher("Hlt1Lines")
    hlt2Lines = getLinesnbMatcher("Hlt2Lines")

    linesSep = Group(Literal("|***|") + restOfLine).suppress()
    lineslist = OneOrMore(
        Group(pipe + Word(nums, min=3) + pipe +
              Word(alphanums + "_:&.*()|?!") + pipe + nbwitherr + pipe +
              nbwitherr + pipe))
    linesHlt1Global = Group(
        Literal("|---|Hlt1Global") + pipe + nbwitherr + pipe + Literal("--") +
        pipe)
    linesHlt2Global = Group(
        Literal("|---|Hlt2Global") + pipe + nbwitherr + pipe + Literal("--") +
        pipe)
    linesHlt2Turbo = Group(
        Literal("|---|Hlt2.*Turbo") + pipe + nbwitherr + pipe + Literal("--") +
        pipe)
    linesHlt2Full = Group(
        Literal("|---|Hlt2.*Full") + pipe + nbwitherr + pipe + Literal("--") +
        pipe)
    linesHlt2Turcal = Group(
        Literal("|---|Hlt2.*Turcal") + pipe + nbwitherr + pipe +
        Literal("--") + pipe)

    # Assemble grammar
    bnf = (Suppress(header) + hlt1Lines + hlt2Lines + removedLines +
           commentLine + nbevents + ratesAssume + linesSep +
           linesHlt1Global.setResultsName("Hlt1GlobalRate") +
           linesHlt2Global.setResultsName("Hlt2GlobalRate") +
           linesHlt2Turbo.setResultsName("TurboRate") +
           linesHlt2Full.setResultsName("FullRate") +
           linesHlt2Turcal.setResultsName("TurcalRate") + commentLine +
           linesSep + lineslist.setResultsName("Hlt1RegexStats") + commentLine
           + linesSep + lineslist.setResultsName("Hlt2RegexStats") +
           commentLine + linesSep + lineslist.setResultsName("Hlt1Stats") +
           linesSep + lineslist.setResultsName("Hlt2Stats") + trailer)

    return bnf


def parseHLTRateList(logtxt):
    """Tool to parse the HLT rates table"""
    grammar = getHLTRateParser()
    result = grammar.parseString(logtxt)
    return dict(result)
