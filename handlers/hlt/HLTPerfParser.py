#!/bin/env python

import csv
import fnmatch
from os import listdir, path
import shelve
from contextlib import closing
from math import sqrt
import re
import json


def _parse_log_file(this_log):
    output_dict = {}
    timing_list = []

    re_timing = re.compile(r"(?:\|?\s*(\d+(?:\.\d+)?)\s*)")
    re_name = re.compile(r"TIMER\.TIMER:(\s*)([\w0-9]+(?:\s\w+)?)\s*\|")
    with open(this_log, "r") as _logFile:
        for _line in _logFile:
            if _line.find("TIMER.TIMER:") == -1:
                continue
            number_line = "".join(_line.split("|")[1:])
            numbers = [float(n) for n in re_timing.findall(number_line)]

            r = re_name.search(_line)
            if r and numbers:
                alg_name = r.group(2).strip().rstrip()
                depth = len(r.group(1))

                timing_list.append((alg_name, depth))

                output_dict[alg_name] = {
                    "depth": depth,
                    "<user>": numbers[0],
                    "<clock>": numbers[1],
                    "min": numbers[2],
                    "max": numbers[3],
                    "entries": numbers[4],
                    "total (s)": numbers[5],
                    "total2": numbers[6],
                }

    i = 0
    for name, depth in timing_list:
        if "parent" not in output_dict[name]:
            output_dict[name]["parent"] = ""
        if "children" not in output_dict[name]:
            output_dict[name]["children"] = []
        j = i
        while j >= 0:
            parent_depth = timing_list[j][1]
            if parent_depth == (depth - 1):
                output_dict[name]["parent"] = timing_list[j][0]
                output_dict[timing_list[j][0]]["children"].append(name)
                break
            j -= 1
        i += 1

    return output_dict


def correct_times(total_events, total_time, norm_rate, events, time, n_cpu,
                  n_workers):
    rate = total_events / total_time
    if norm_rate == None:
        norm_rate = rate
    else:
        norm_rate /= float(n_workers)
    norm = rate / norm_rate

    tpe = time / events if time != 0.0 else 0.0
    tpe /= norm
    return tpe * float(n_cpu) / float(n_workers)


def filter_logs(log_file):
    if not hasattr(filter_logs, "regex"):
        filter_logs.regex = re.compile(r"^%s_(\d(\d+)).*" % "moores")
    r = filter_logs.regex.match(log_file)
    return int(r.group(2)) if r else r


def process_directory(log_dir):
    files = [
        path.join(log_dir, lf) for lf in listdir(log_dir) if filter_logs(lf)
    ]

    total_input = {}

    for _file in files:
        total_input[_file] = _parse_log_file(_file)

    moore_metadata = None
    with open(path.join(log_dir, "output.json"), "r") as _file:
        moore_metadata = json.load(_file)

    timing_results = calc_times(total_input, moore_metadata)

    throughput = moore_metadata["av_inst"]
    throughput_err = moore_metadata["av_inst_s"]
    throughput_farm = scale_to_farm(throughput)
    throughput_farm_err = scale_to_farm(throughput_err)

    return {
        "timing_results": timing_results,
        "throughput": throughput,
        "throughput_err": throughput_err,
        "throughput_farm": throughput_farm,
        "throughput_farm_err": throughput_farm_err,
    }


def scale_to_farm(throughput):
    nFaster = 296
    nFast = 792
    nMedium = 96
    nSlow = 372
    return throughput * (nFaster + 0.8 * nFast + 0.5 *
                         (nMedium + nSlow)) / 1000.0


def calc_times(total_input, moore_metadata):
    temp_result = {}

    n_cpu = moore_metadata["cpu_count"]
    n_workers = moore_metadata["n_nodes"] * moore_metadata["n_slaves"]

    for k, v in list(total_input.items()):
        total_events = v["EVENT LOOP"]["entries"]
        total_time = v["EVENT LOOP"]["total (s)"]
        for AlgKey, Alg in list(v.items()):
            for attr_key, attr_val in list(Alg.items()):
                if AlgKey not in temp_result:
                    temp_result[AlgKey] = {}
                try:
                    temp_val = float(attr_val)
                except:
                    temp_result[AlgKey][attr_key] = attr_val
                    continue
                if attr_key not in temp_result[AlgKey]:
                    temp_result[AlgKey][attr_key] = {}
                    temp_result[AlgKey][attr_key]["values"] = []

                if attr_key == "events":
                    true_value = 1000 * correct_times(
                        total_events,
                        total_time,
                        moore_metadata["av_inst"],
                        attr_val,
                        Alg["events"],
                        n_cpu,
                        n_workers,
                    )
                else:
                    true_value = attr_val

                temp_result[AlgKey][attr_key]["values"].append(attr_val)

    for AlgKey, Alg in list(temp_result.items()):
        for attr_key, attr_val in list(Alg.items()):
            if not isinstance(attr_val, dict):
                continue
            values = attr_val["values"]
            attr_val["min"] = min(values)
            attr_val["max"] = max(values)
            attr_val["total"] = sum(values)
            total2 = sum([_ * _ for _ in values])
            attr_val["total_sq"] = total2
            num = float(len(values))
            avg = attr_val["total"] / num
            attr_val["average"] = avg
            try:
                attr_val["error"] = sqrt((total2 / num) - avg * avg)
            except ValueError:
                attr_val["error"] = 0.0

    for AlgKey, Alg in list(temp_result.items()):
        for attr_key, attr_val in list(Alg.items()):
            if not isinstance(attr_val, dict):
                continue
            for i in "values", "total", "total_sq":
                del attr_val[i]

    return temp_result


def main():
    import sys

    if len(sys.argv) == 2:
        _dir = sys.argv[1]
    else:
        _dir = "."

    output = process_directory(_dir)

    print("throughput = %s +/- %s" % (output["throughput"],
                                      output["throughput_err"]))


if __name__ == "__main__":
    main()

#    print("%s" % str(final_result))
