#!/bin/env python
_Streams = {}

# _PersistRecoList = ['Hlt2DiMuonJPsiTurbo',
#                    'Hlt2CharmHadDstp2D0Pip_D02KmPipTurbo',
#                    'Hlt2Dstp2D0Pip_D02KmPimPipPipTurbo',
#                    'Hlt2D02KmPipTurbo',
#                    'Hlt2DpToKmPipPipTurbo',
#                    'Hlt2DspToKmKpPipTurbo',
#                    'Hlt2LcpToPpKmPipTurbo',
#                    'Hlt2Xic0ToPpKmKmPipTurbo',
#                    'Hlt2XicpToPpKmPipTurbo']

_Streams["AllStreams"] = [
    ["Hlt2.*", "ALL"],
    ["Hlt2CharmHad.(?!.*?Turbo)", "CharmFull"],
    ["Hlt2CharmHad.*Turbo", "CharmTurbo"],
    ["Hlt2Topo.*", "Topo"],
    ["Hlt2(DiMuon|SingleMuon|TriMuon|DiElectron|LFV).*", "Leptons"],
    ["Hlt2.*(TrackEff|TurboCalib)", "TurboCalib"],
    ["Hlt2(EW|Exotica|Jets).*", "EW"],
    ["Hlt2LowMult.*", "LowMult"],
    # ["Hlt2(Bc2JpsiX|Bottomonium|CcDiHadron||XcMuX|RareCharm|B2|Lb2|Bd2|Bs2|Bu2|DisplVert|DiPhi|DiProt|Radiative|Phi2KsKs|RareStrange|IncPhi|DPS|BHad).*","Other"],
    [
        "Hlt2(B2Kpi0|B2HH|Majorana|Bc2JpsiX|Bottomonium|CcDiHadron|XcMuX|RareCharm|DisplVert|Radiative|RareStrange|Strange|Phi|DPS|BHad).*",
        "Other",
    ],
    [
        "Hlt2(Lumi|ErrorEvent|PassThrough|Forward|DebugEvent|Transparent|NoBiasNonBeamBeam)",
        "Hlt2Technical",
    ],
]

_Streams["Modules"] = [
    ["Hlt2.*", "ALL"],
    ["Hlt2CcDiHadron.*", "Hlt2CcDiHadron"],
    ["Hlt2DPS.*", "Hlt2DPS"],
    ["Hlt2Bc2JpsiX.*", "Hlt2Bc2JpsiX"],
    ["Hlt2DiMuon.*", "Hlt2DiMuon"],
    ["Hlt2Majorana.*", "Hlt2Majorana"],
    ["Hlt2Bottomonium.*", "Hlt2Bottomonium"],
    ["Hlt2Strange.*", "Hlt2Strange"],
    ["Hlt2DisplVertices.*", "Hlt2DisplVertices"],
    ["Hlt2TrackEff_D0.*", "Hlt2TrackEff"],
    ["Hlt2TrackEffDiMuon.*", "Hlt2TrackEffDiMuon"],
    [
        "Hlt2(Lumi|ErrorEvent|PassThrough|Forward|DebugEvent|Transparent|NoBiasNonBeamBeam).*",
        "Hlt2Technical",
    ],
    ["Hlt2RareStrange.*", "Hlt2RareStrange"],
    ["Hlt2TriMuon.*", "Hlt2TriMuon"],
    # ["Hlt2(Phi2|Bs2PhiPhi|IncPhi|DiPhi)","Hlt2Phi"],
    ["Hlt2Phi.*", "Hlt2Phi"],
    ["Hlt2XcMuXForTau.*", "Hlt2XcMuXForTau"],
    ["Hlt2SingleMuon.*", "Hlt2SingleMuon"],
    ["Hlt2B2HH.*", "Hlt2B2HH"],
    ["Hlt2B2Kpi0.*", "Hlt2B2Kpi0"],
    ["Hlt2BHad.*", "Hlt2BHad"],
    ["Hlt2CharmHad.*", "Hlt2CharmHad"],
    ["Hlt2DiElectron.*", "Hlt2DiElectron"],
    ["Hlt2Exotica.*", "Hlt2Exotica"],
    ["Hlt2Jets.*", "Hlt2Jets"],
    ["Hlt2LFV.*", "Hlt2LFV"],
    ["Hlt2LowMult.*", "Hlt2LowMult"],
    ["Hlt2PID.*", "Hlt2PID"],
    ["Hlt2Radiative.*", "Hlt2Radiative"],
    ["Hlt2RareCharm.*", "Hlt2RareCharm"],
    ["Hlt2EW.*", "Hlt2EW"],
    ["Hlt2Topo.*", "Hlt2Topo"],
]

_Streams["Spectroscopy"] = [
    ["Hlt2.*", "ALL"],
    [
        "Hlt2(DiMuonJPsiTurbo|CharmHadInclDst2PiD02HHXBDT|CharmHadSpec_D0ToKPi_PiTurbo|CharmHadDpToKmPipPipTurbo|CharmHadDspToKpPimPipTurbo|CharmHadLcpToPpKmPipTurbo|CharmHadXicpToPpKmPip).*",
        "Spec",
    ],
]

_Streams["Paper"] = [
    ["Hlt2.*", "ALL"],
    ["Hlt2CharmHadIncl.*", "Incl_D"],
    ["Hlt2Topo.*", "Incl_B"],
    # ["Hlt2.*SingleMuon.*","SingleMuon"],
    # ["Hlt2.*(SingleMuon.*","SingleMuon"],
    ["Hlt2(?!.*?LowMult).*Muon.*", "Muon"],
    ["Hlt2(DiProtonLowMult|LowMult).*", "LowMult"],
    ["Hlt2.*TurboCalib", "Calibration"],
    [
        "Hlt2(?!.*?CharmHadIncl)(?!.*?Topo)(?!.*?SingleMuon)(?!.*?Muon)(?!.*?LowMult)(?!.*?TurboCalib)",
        "Exclusives",
    ],
]

_Streams["Bandwidth"] = [
    ["Hlt2.*", "ALL"],
    ["Hlt2(?!.*?LowMult).(?!.*?Turbo).(?!.*?Calib)", "Full (rest)"],
    ["Hlt2LowMult.*", "Full (LowMult)"],
    ["Hlt2.(?!.*?TurboCalib).*Turbo", "Turbo"],
    ["Hlt2.*TurboCalib", "TurboCalib"],
]

_Streams["CharmFull"] = [
    [[x for x in _Streams["AllStreams"] if x[1] == "CharmFull"][0][0], "ALL"],
    ["Hlt2CharmHad.*(Eta|Pi0|ee|gamma)(?!.*?Turbo)", "Neutrals"],
    ["Hlt2CharmHad.*HHXBDT", "InclDstar"],
    ["Hlt2CharmHad(Omega|Xi).(?!.*?Turbo)", "Hyperons"],
    ["Hlt2CharmHadDstD02Ksh.(?!.*?Turbo)", "NeutralD_KsHH_Tagged"],
    ["Hlt2CharmHad.*ForKPiAsym", "ChargedD_HHH_AKpi"],
]

_Streams["CharmTurbo"] = [
    [[x for x in _Streams["AllStreams"] if x[1] == "CharmTurbo"][0][0], "ALL"],
    ### D0/D*
    ["Hlt2CharmHadDst_2D0.(?!.*?LTUNB)", "D0_HH_Tagged_LTBiased"],
    ["Hlt2CharmHadDst_2D0.*LTUNB", "D0_HH_Tagged_LTUB"],
    ["Hlt2CharmHadDst_2D0Pi_D02(KK|PiPi|KPi).*", "NeutralD_HH_Tagged"],
    ["Hlt2CharmHadD02(KPi|KK|PiPi)Turbo", "NeutralD_HH_Untagged"],
    [
        "Hlt2CharmHad(?!.*?Spec).*D02.*(PiPiPiPi|KPiPiPi|KKPiPi|KKKPi).*",
        "NeutralD_HHHH_Tagged",
    ],
    ["Hlt2CharmHadD2KS0KS0.*", "NeutralD_KsKs_Untagged"],
    #### D+
    ["Hlt2CharmHadD2KS0(K_|Pi_).*", "ChargedD_KsH"],
    ["Hlt2CharmHad(DspTo|DpTo).*", "ChargedD_HHHandHHHHH"],
    ["Hlt2CharmHadD(p2|s2|2)(KKPi|PiPiPi|KPiPi|KKK)Ks.*", "ChargedD_HHHKs"],
    #### other
    ["Hlt2CharmHadSpec.*", "CharmHadSpec"],
    ["Hlt2CharmHadLc2Lambda.*", "Lambdac_LambdaH"],
    ["Hlt2CharmHadLcpTo.*", "Lambdac_phh"],
]

_Streams["Leptons"] = [
    [[x for x in _Streams["AllStreams"] if x[1] == "Leptons"][0][0], "ALL"],
    ["Hlt2DiMuonSoft", "DiMuonSoft"],
    ["Hlt2DiMuon(?!.*?Soft).*", "DiMuon"],
    ["Hlt2TriMuon.*", "TriMuon"],
    ["Hlt2SingleMuon.*", "SingleMuon"],
]

_Streams["Other"] = [
    [[x for x in _Streams["AllStreams"] if x[1] == "Other"][0][0], "ALL"],
    ["Hlt2(B2|Lb2|Bd2|Bs2|Bu2).*", "B_HH"],
    ["Hlt2RareCharm.*", "RareCharm"],
    ["Hlt2DisplVert.*", "DisplVert"],
    ["Hlt2.*(DiPhi|DiProt).*", "DiPhiDiProt"],
    ["Hlt2IncPhi.*", "IncPhi"],
    ["Hlt2Radiative.*", "Radiative"],
    ["Hlt2Phi2KsKs.*", "PhiToKsKs"],
    ["Hlt2DPS.*", "DPS"],
    # ["Hlt2LowMult.*","LowMult"],
    ["Hlt2RareStrange.*", "RareStrange"],
]

_Streams["EW"] = [
    [[x for x in _Streams["AllStreams"] if x[1] == "EW"][0][0], "ALL"],
    ["Hlt2EWSingleTau.*", "HighpT_tau"],
    ["Hlt2EWSingleMuon.*", "Single_mu"],
    ["Hlt2EWSingle.*Electron.*", "Single_e"],
    ["Hlt2EWDiMuon.*", "DiMuon"],
    ["Hlt2EWDiElectron.*", "DiEle"],
]

_Streams["Topo"] = [
    [[x for x in _Streams["AllStreams"] if x[1] == "Topo"][0][0], "ALL"],
    ["Hlt2Topo2Body.*", "2Body"],
    ["Hlt2Topo3Body.*", "3Body"],
    ["Hlt2Topo4Body.*", "4Body"],
    ["Hlt2TopoMu2Body.*", "Mu2Body"],
    ["Hlt2TopoMu3Body.*", "Mu3Body"],
    ["Hlt2TopoMu4Body.*", "Mu4Body"],
]

_Streams["TurboCalib"] = [
    [[x for x in _Streams["AllStreams"] if x[1] == "TurboCalib"][0][0], "ALL"],
    ["Hlt2PID.*", "PID"],
    ["Hlt2TrackEffDiMuon", "TrackEff(psi)"],
    ["Hlt2TrackEff_D0", "TrackEff(D*)"],
]

############# obsolete
# _Streams["Regex"] = [["Hlt2.*","ALL"],
#                     ["Hlt2CharmHad.(?!.*?HHX).*","CharmExcl"],
#                     ["Hlt2CharmHad.*HHXBDT","CharmIncl"],
#                     ["Hlt2Topo.*","Topo"],
#                     ["Hlt2(B2|Lb2|Bd2|Bs2|Bu2).*","B2X"],
#                     ["Hlt2DiMuonSoft.*","DiMuSoft"],
#                     ["Hlt2DiMuon(?!.*?Soft).*","DiMu"],
#                     ["Hlt2PID.*","PID"],
#                     ["Hlt2EW.*","EW"],
#                     ["Hlt2RareCharm.*","RaCharm"],
#                     ["Hlt2.*(Bc2J).*","Bc"],
#                     ["Hlt2DisplVert.*","DisplV"],
#                     ["Hlt2TrackEff.*","TrEff"],
#                     ["Hlt2.*(DiPhi|DiProt).*","DiP"],
#                     ["Hlt2SingleMuon.*","SiMu"],
#                     ["Hlt2TriMuon.*","TriMu"],
#                     ["Hlt2IncPhi.*","IncPhi"],
#                     ["Hlt2Radiative.*","Rad"],
#                     ["Hlt2Phi2KsKs.*","KsKs"],
#                     ["Hlt2DPS.*","DPS"],
#                     ["Hlt2LowMult.*","LowMult"],
#                     ["Hlt2RareStrange.*","RareStr"]]
