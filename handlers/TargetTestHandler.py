import os
import re
import ROOT as rt

from .BaseHandler import BaseHandler


class TargetTestHandler(BaseHandler):
    # init is implicit

    def _create_comp_plot(self, name, plots_file, components, out_file,
                          titles):
        plots_file = rt.TFile.Open(plots_file)
        out_file = rt.TFile.Open(out_file, "UPDATE")
        canvas = rt.TCanvas(name, name, 800, 600)
        legend = rt.TLegend(0.1, 0.7, 0.48, 0.9)
        first = True
        color_list = [rt.kRed, rt.kGreen, rt.kBlue, rt.kViolet]
        style_list = [1, 7, 1, 7]

        for plot, color, title, style in zip(components, color_list, titles,
                                             style_list):
            plot = plots_file.Get(plot)
            plot.SetLineColor(color)
            plot.SetLineStyle(style)
            plot.SetMarkerColor(color)
            plot.SetMarkerStyle(20)
            legend.AddEntry(plot, title, "lp")
            try:
                if first:
                    plot.Draw("ALP")
                    first = False
                else:
                    plot.Draw("LP SAME")
                    canvas.Update()
            except:
                raise AssertionError
        legend.Draw()
        canvas.Update()
        canvas.Write()
        plots_file.Close()
        out_file.Close()

    def _make_desc_string(self, name, out_file, plot_type, thickness, mat,
                          part):
        out_file = rt.TFile.Open(out_file, "UPDATE")
        _root_str = rt.TNamed()
        _root_str.SetName(name + "__description")
        _root_str.SetTitle(
            "Comparison of {} for {} fired at various energies into a {}mm target of {}"
            + " using different physics lists in Geant4.".format(
                plot_type, part, thickness, mat))
        _root_str.Write()
        out_file.Close()

    def make_comp_plots(self, plots_file, output_file):
        gr_1_template = "InElastic_CrossSection_Thickness-10mm_Mat-Al_Mod-{}_PGun-{}"
        gr_1_name = "InElastic_XSec_Kaon_10mm_Al"
        self._create_comp_plot(
            gr_1_name,
            plots_file,
            [
                gr_1_template.format("QGSP_BERT", "Kplus"),
                gr_1_template.format("QGSP_BERT", "Kminus"),
                gr_1_template.format("FTFP_BERT", "Kplus"),
                gr_1_template.format("FTFP_BERT", "Kminus"),
            ],
            output_file,
            [
                "K^{+} QGSP_BERT",
                "K^{-} QGSP_BERT",
                "K^{+} FTFP_BERT",
                "K^{-} FTFP_BERT",
            ],
        )

        self._make_desc_string(gr_1_name, output_file,
                               "inelastic cross sections", 10, "aluminium",
                               "kaons")

        gr_2_template = "Multiplicity_Thickness-10mm_Mat-Be_Mod-{}_PGun-{}"
        gr_2_name = "Multiplicity_Proton_10mm_Be"
        self._create_comp_plot(
            gr_2_name,
            plots_file,
            [
                gr_2_template.format("QGSP_BERT", "p"),
                gr_2_template.format("QGSP_BERT", "pbar"),
                gr_2_template.format("FTFP_BERT", "p"),
                gr_2_template.format("FTFP_BERT", "pbar"),
            ],
            output_file,
            [
                "p^{+} QGSP_BERT",
                "p^{-} QGSP_BERT",
                "p^{+} FTFP_BERT",
                "p^{-} FTFP_BERT",
            ],
        )

        self._make_desc_string(gr_2_name, output_file, "multiplicities", 10,
                               "beryllium", "protons")

        gr_3_template = "Elastic_CrossSection_Thickness-10mm_Mat-Si_Mod-{}_PGun-{}"
        gr_3_name = "Elastic_XSec_Pion_10mm_Si"
        self._create_comp_plot(
            gr_3_name,
            plots_file,
            [
                gr_2_template.format("QGSP_BERT", "Piplus"),
                gr_2_template.format("QGSP_BERT", "Piminus"),
                gr_2_template.format("FTFP_BERT", "Piplus"),
                gr_2_template.format("FTFP_BERT", "Piminus"),
            ],
            output_file,
            [
                "#pi^{+} QGSP_BERT",
                "#pi^{-} QGSP_BERT",
                "#pi^{+} FTFP_BERT",
                "#pi^{-} FTFP_BERT",
            ],
        )

        self._make_desc_string(gr_3_name, output_file,
                               "elastic cross sections", 10, "silicon",
                               "pions")

    def collectResults(self, directory):
        plots_file = os.path.join(directory, "TargetOutput", "ROOTGraphs",
                                  "TargetTestAllPlots.root")
        comp_file = os.path.join(directory, "TargetOutput", "ROOTGraphs",
                                 "TargetTestCompPlots.root")
        self.make_comp_plots(plots_file, comp_file)
        self.saveFile("TargetTestAllPlots.root",
                      plots_file)  # All plots now contained in single file
        self.saveFile("TargetTestCompPlots.root",
                      comp_file)  # All plots now contained in single file
