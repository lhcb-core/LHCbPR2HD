import os
from .BaseHandler import BaseHandler
from .parser.GaudiSequenceParser import GaudiSequenceParser
from .timing.CallgrindLogParser import CallgrindLogParser
"""
The CallGrindHandler module extracts callgrind metrics from a data structure
which is passed to it from the timing.CallgrindLogParser. The function numbers
to extract from the CallgrindLogParser are extracted by the GaudiSequenceParser
"""


class CallgrindHandler(BaseHandler):
    def __init__(self, directory=""):
        super(self.__class__, self).__init__()
        self.directory = directory
        if not self.directory:
            self.directory = os.path.realpath(os.curdir)
        # variables used for parsing the cachegrind annotated log file
        self.algoselect = [
            "GaudiSequencer/RecoDecodingSeq",
            "GaudiSequencer/RecoTrFastSeq",
        ]

    def collectResults(self, directory):
        gsp = GaudiSequenceParser(dir=directory)
        selectedalgos = gsp.selectAlgorithms(self.algoselect)

        cgp = CallgrindLogParser(dir=directory, algos=selectedalgos)
        (callgrindmetrics, callgrindvalues) = cgp.extractCallgrindMetrics()

        for alg in callgrindmetrics:
            metr = callgrindmetrics[alg]
            btot = metr["bc"] + metr["bi"]
            itot = metr["ir"] + metr["dr"] + metr["dw"]
            l1m = metr["i1mr"] + metr["d1mr"] + metr["d1mw"]
            llm = metr["ilmr"] + metr["dlmr"] + metr["dlmw"]
            bm = metr["bim"] + metr["bcm"]
            cest = metr["ir"] + 10 * bm + 10 * l1m + 100 * llm
            fp32 = (metr["ifp32x1"] + 2 * metr["ifp32x2"] + 4 * metr["ifp32x4"]
                    + 8 * metr["ifp32x8"])
            fp64 = metr["ifp64x1"] + 2 * metr["ifp64x2"] + 4 * metr["ifp64x4"]
            vfp128 = 4 * metr["ifp32x4"] + 2 * metr["ifp64x4"]
            vfp256 = 8 * metr["ifp32x8"] + 4 * metr["ifp64x4"]
            vfp = 2 * metr["ifp32x2"] + vfp128 + vfp256
            sfp = metr["ifp32x1"] + metr["ifp64x1"]
            flop = fp32 + fp64

            rsimd = 0
            if flop:
                rsimd = float(vfp) / float(flop)

            rbm = 0
            if btot:
                rbm = float(bm) / float(btot)

            rcm = 0
            if itot:
                rcm = float(l1m + llm) / float(itot)

            for val in callgrindvalues:
                valname = val[0]
                valdesc = val[1]
                metname = "%s_%s" % (valname, alg)
                self.saveInt(metname, metr[valname], valdesc,
                             "callgrind_metric")

            self.saveInt("l1m_" + alg, l1m, "L1 Miss Sum", "callgrind_metric")
            self.saveInt("llm_" + alg, llm, "Last Levl Miss Sum",
                         "callgrind_metric")
            self.saveInt("bm_" + alg, bm, "Branch Missprediction",
                         "callgrind_metric")
            self.saveInt("cest_" + alg, cest, "Cycle Estimation",
                         "callgrind_metric")
            self.saveInt("fp32_" + alg, fp32, "fp 32 operations",
                         "callgrind_metric")
            self.saveInt("fp64_" + alg, fp64, "fp 64 operations",
                         "callgrind_metric")
            self.saveInt("vfp128_" + alg, vfp128, "simd 128 fp operations",
                         "callgrind_metric")
            self.saveInt("vfp256_" + alg, vfp256, "simd 256 fp operations",
                         "callgrind_metric")
            self.saveInt("vfp_" + alg, vfp, "simd fp operations",
                         "callgrind_metric")
            self.saveInt("sfp_" + alg, sfp, "scalar fp operations",
                         "callgrind_metric")
            self.saveInt("flop_" + alg, flop, "fp operations",
                         "callgrind_metric")
            self.saveFloat("rsimd_" + alg, rsimd, "ratio simd operations",
                           "callgrind_metric")
            self.saveFloat("rbm_" + alg, rbm, "ratio branch misspredictions",
                           "callgrind_metric")
            self.saveFloat("rcm_" + alg, rcm, "ratio cache misses",
                           "callgrind_metric")

    def run(self):
        self.collectResults()


if __name__ == "__main__":
    CallgrindHandler().run()
