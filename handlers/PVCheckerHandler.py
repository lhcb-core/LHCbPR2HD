import os
import re
import math
from .BaseHandler import BaseHandler
import ROOT as r
from collections import OrderedDict


class PVCheckerHandler(BaseHandler):
    def __init__(self):
        super(self.__class__, self).__init__()

    def getEff(self, num, denom):
        """
        Return the tuple (eff, err_eff) ginving the numerator and the denominator
        if denom == 0  returns (0, 0)
        """
        if denom == 0:
            return (0, 0)
        else:
            eff = num / denom
            eff_err = math.sqrt(eff * (1 - eff) / denom)
            return eff, eff_err

    def extractPerf(self, infile):
        """
        Extract the interesting performance numbers from the log file
        returns a dictionary {key: (value, error, explanation)}
        """

        numbers = OrderedDict()

        for line in open(infile, "r"):
            if line.find("PVChecker") > -1:
                if (
                        line.find("All") > -1
                ) and "EffPVall" not in numbers:  # Of 'All' I want only the first occurence in the log file because it appears also later
                    num, denom = re.findall(
                        "PVChecker.*?All.*?\(([ 0-9\.]*?)/([ 0-9\.]*?)\)",
                        line)[0]
                    num, denom = float(num), float(denom)
                    eff, eff_err = self.getEff(num, denom)
                    numbers["EffPVall"] = (eff, eff_err, "PV efficiency")
                elif line.find("False rate") > -1:
                    num, denom = re.findall(
                        "PVChecker.*?False rate.*?\(([ 0-9\.]*?)/([ 0-9\.]*?)\)",
                        line)[0]
                    num, denom = float(num), float(denom)
                    eff, eff_err = self.getEff(num, denom)
                    numbers["falseRate"] = (eff, eff_err, "PV fake rate")
                elif line.find("INFO dx:") > -1:
                    mean, mean_err, rms, rms_err = re.findall(
                        "PVChecker.*? mean = ([ 0-9\.\-]*?) \+/\- ([ 0-9\.\-]*?), RMS = ([ 0-9\.\-]*?) \+/\- ([ 0-9\.\-]*?)$",
                        line,
                    )[0]
                    for i in [mean, mean_err, rms, rms_err]:
                        i = float(i)
                    numbers["dx_mean"] = (
                        mean,
                        mean_err,
                        "mean x distance between reco and MC vertices",
                    )
                    numbers["dx_rms"] = (
                        rms,
                        rms_err,
                        "rms x distance between reco and MC vertices",
                    )
                elif line.find("INFO dy:") > -1:
                    mean, mean_err, rms, rms_err = re.findall(
                        "PVChecker.*? mean = ([ 0-9\.\-]*?) \+/\- ([ 0-9\.\-]*?), RMS = ([ 0-9\.\-]*?) \+/\- ([ 0-9\.\-]*?)$",
                        line,
                    )[0]
                    for i in [mean, mean_err, rms, rms_err]:
                        i = float(i)
                    numbers["dy_mean"] = (
                        mean,
                        mean_err,
                        "mean y distance between reco and MC vertices",
                    )
                    numbers["dy_rms"] = (
                        rms,
                        rms_err,
                        "rms y distance between reco and MC vertices",
                    )
                elif line.find("INFO dz:") > -1:
                    mean, mean_err, rms, rms_err = re.findall(
                        "PVChecker.*? mean = ([ 0-9\.\-]*?) \+/\- ([ 0-9\.\-]*?), RMS = ([ 0-9\.\-]*?) \+/\- ([ 0-9\.\-]*?)$",
                        line,
                    )[0]
                    for i in [mean, mean_err, rms, rms_err]:
                        i = float(i)
                    numbers["dz_mean"] = (
                        mean,
                        mean_err,
                        "mean z distance between reco and MC vertices",
                    )
                    numbers["dz_rms"] = (
                        rms,
                        rms_err,
                        "rms z distance between reco and MC vertices",
                    )
                elif line.find("INFO pullx:") > -1:
                    mean, mean_err, rms, rms_err = re.findall(
                        "PVChecker.*? mean = ([ 0-9\.\-]*?) \+/\- ([ 0-9\.\-]*?), RMS = ([ 0-9\.\-]*?) \+/\- ([ 0-9\.\-]*?)$",
                        line,
                    )[0]
                    for i in [mean, mean_err, rms, rms_err]:
                        i = float(i)
                    numbers["pullx_mean"] = (
                        mean,
                        mean_err,
                        "mean pull of the x distance between reco and MC vertices",
                    )
                    numbers["pullx_rms"] = (
                        rms,
                        rms_err,
                        "rms pull of the x distance between reco and MC vertices",
                    )
                elif line.find("INFO pully:") > -1:
                    mean, mean_err, rms, rms_err = re.findall(
                        "PVChecker.*? mean = ([ 0-9\.\-]*?) \+/\- ([ 0-9\.\-]*?), RMS = ([ 0-9\.\-]*?) \+/\- ([ 0-9\.\-]*?)$",
                        line,
                    )[0]
                    for i in [mean, mean_err, rms, rms_err]:
                        i = float(i)
                    numbers["pully_mean"] = (
                        mean,
                        mean_err,
                        "mean pull of the y distance between reco and MC vertices",
                    )
                    numbers["pully_rms"] = (
                        rms,
                        rms_err,
                        "rms pull of the y distance between reco and MC vertices",
                    )
                elif line.find("INFO pullz:") > -1:
                    mean, mean_err, rms, rms_err = re.findall(
                        "PVChecker.*? mean = ([ 0-9\.\-]*?) \+/\- ([ 0-9\.\-]*?), RMS = ([ 0-9\.\-]*?) \+/\- ([ 0-9\.\-]*?)$",
                        line,
                    )[0]
                    for i in [mean, mean_err, rms, rms_err]:
                        i = float(i)
                    numbers["pullz_mean"] = (
                        mean,
                        mean_err,
                        "mean pull of the z distance between reco and MC vertices",
                    )
                    numbers["pullz_rms"] = (
                        rms,
                        rms_err,
                        "rms pull of the z distance between reco and MC vertices",
                    )
                    break

        return numbers

    def extractHists(self, infile):
        """
        Extract the interesting histograms from the root file
        (N.B The root file should have already been opened)
        returns a dictionary with {key : (histo, explanation)}
        """

        histos = OrderedDict()

        histos["PV_dx"] = (
            infile.Get("PVChecker/1021").Clone(),
            "x distance between reco and MC vertices",
        )
        histos["PV_dy"] = (
            infile.Get("PVChecker/1022"),
            "y distance between reco and MC vertices",
        )
        histos["PV_dz"] = (
            infile.Get("PVChecker/1023"),
            "z distance between reco and MC vertices",
        )
        histos["PV_pullx"] = (
            infile.Get("PVChecker/1031"),
            "pull of x distance between reco and MC vertices",
        )
        histos["PV_pully"] = (
            infile.Get("PVChecker/1032"),
            "pull of y distance between reco and MC vertices",
        )
        histos["PV_pullz"] = (
            infile.Get("PVChecker/1033"),
            "pull of z distance between reco and MC vertices",
        )
        histos["PV_nTr4PVrec"] = (
            infile.Get("PVChecker/1041"),
            "Number of tracks per reconstructed PV",
        )
        histos["PV_dnTr4PV_MCvsRec"] = (
            infile.Get("PVChecker/1041"),
            "Difference in number of tracks between MC and reconstructed PV",
        )
        histos["PV_nPVperEvt"] = (
            infile.Get("PVChecker/1051"),
            "Number of PVs per event",
        )

        return histos

    def collectResults(self, directory):
        # get efficiency and fake rate from PVChecker log
        possible_files = [
            i for i in os.listdir(directory) if re.match("run.log", i)
        ]
        if len(possible_files) == 0:
            raise IOError("Input log file not found")

        numbers = self.extractPerf(os.path.join(directory, "run.log"))

        # save floats
        for key, (val, err, description) in list(numbers.items()):
            self.saveFloat(
                key, val, description=description, group="performance")
            self.saveFloat(
                key + "_err",
                err,
                description="Error of " + description,
                group="performance",
            )

        # get histograms
        possible_files = [
            i for i in os.listdir(directory)
            if re.match("Brunel-.*ev-histos.root", i)
        ]
        if len(possible_files) == 0:
            raise IOError("Input root file with histograms not found")

        infile_root = r.TFile(os.path.join(directory, possible_files[0]))
        histos = self.extractHists(infile_root)

        outfile = r.TFile("PVChecker.root", "RECREATE")

        # save histograms
        for key, (histo, description) in list(histos.items()):
            self.saveJSON(
                key, histo, description=description, group="performance")
            histo.Write()

        self.saveFile("PVChecker", "./PVChecker.root")
