import os
from .BaseHandler import BaseHandler


class DataChallengesROOTFileHandler(BaseHandler):
    def __init__(self):
        super(self.__class__, self).__init__()

    def collectResults(self, directory):
        files = [
            "hlt2_reco_baseline_DC.root",
            "hlt1_allen_track_reconstruction.root",
            "Hlt1SeedAndMatchTrackingResolutionAllen.root",
            "HLT1HLT2Checker_fitted_profile.root",
            "HLT1HLT2Checker_fitted_profile_new.root",
            "histos_hlt2_light_reco_pr_kf_without_UT_on_data_with_monitoring.root",
            "MCMatching_baseline_MiniBias.root",
        ]

        fileFound = False

        for f in files:
            print("Checking for ", f)
            if os.path.isfile(os.path.join(directory, f)):
                fileFound = True
                self.saveFile(f, os.path.join(directory, f))

        if not fileFound:
            raise Exception("No ROOT files found")
