import os
import subprocess
import logging
from .BaseHandler import BaseHandler
from collectRunResults import send_notification_mattermost

log = logging.getLogger(__name__)


class UpgradePrCheckerHandler(BaseHandler):
    def __init__(self):
        super(self.__class__, self).__init__()

    def extractPerf(self, infile, trackingtype="Velo", trackingcat="07"):
        filetoread = open(infile, "r")
        foundtrackingtype = False
        foundtrackingcategory = False
        foundfakestracks = False
        for line in filetoread:
            if not foundtrackingtype:
                if line.find("**** " + trackingtype) > -1:
                    foundtrackingtype = True
                    if line.find("ghosts") > -1 and not foundfakestracks:
                        tracksFound = (line.split("tracks including")[0].split(
                            trackingtype)[1].lstrip(" "))
                        fakesFound = (line.split("tracks including")[1].split(
                            "ghosts")[0].lstrip(" "))
                        fakeRate = (line.split("tracks including")[1].split(
                            "ghosts")[1].split("[")[1].split("]")[0].lstrip(
                                " ").rstrip(" %"))
                        print("Nb. Tracks Found = " + tracksFound)
                        print("Nb. Fakes  Found = " + fakesFound + " [ " +
                              fakeRate + " %]")
                        # print 'Velo tracking found =',line.split('tracks including')[0].split(trackingtype)[1].lstrip(' ')
                        foundfakestracks = True
            else:
                if line.find(trackingcat) > -1:
                    efficiency = float(
                        line.split("[")[1].split("]")[0].lstrip(" ").rstrip(
                            " %"))
                    clone_rate = 100.0 - float(
                        line.split("purity:")[1].split("%")[0].lstrip(" "))
                    print("efficiency = ", efficiency)
                    print("clone rate  = ", clone_rate)
                    foundtrackingcategory = True
                    break

        if not foundtrackingcategory or not foundfakestracks:
            raise Exception("Error getting physics performance metrics:"
                            " not found tracking category or"
                            " not found fakes tracks lines in the log file")
        else:
            return (efficiency, fakeRate, clone_rate)

    def collectResultsExt(
            self,
            directory,
            project,
            version,
            platform,
            hostname,
            cpu_info,
            memoryinfo,
            startTime,
            endTime,
            options,
    ):
        # get efficiency and fake rate from PRChecker log
        efficiency, fake_rate, clone_rate = self.extractPerf(
            os.path.join(directory, "run.log"))

        # save floats and string with path to plot
        self.saveFloat(
            "efficiency",
            efficiency,
            description="efficiency",
            group="performance")
        self.saveFloat(
            "fake_rate",
            fake_rate,
            description="fake rate",
            group="performance")
        self.saveFloat(
            "clone_rate",
            clone_rate,
            description="clone rate",
            group="performance")

        # send plot to eos  as html
        wwwDirEos = os.environ.get("LHCBPR_WWW_EOS")
        if wwwDirEos == None:
            raise Exception(
                "No web dir on EOS defined, will not run extraction")
        else:
            dirname = ("PrChecker_" + str(version) + "_" + str(platform) + "_"
                       + startTime.replace(" ", "_"))
            html_code = ("<html>"
                         "<head></head> "
                         "<body> "
                         "<p>" + str(version) + "<br>" + str(platform) + "<br>"
                         + str(hostname) + "</p>"
                         "<ul>"
                         "  <li>Efficiency = " + str(efficiency) + " </li>"
                         "  <li>Fake rate = " + str(fake_rate) + "</li>"
                         "  <li>Clone rate = " + str(clone_rate) + "</li>"
                         "</ul>"
                         "</body>"
                         "</html>")

            with open("index.html", "w") as html_file:
                html_file.write(html_code)

            targetRootEosDir = os.path.join(wwwDirEos, dirname)
            try:
                subprocess.call([
                    "xrdcp", "-f", "index.html",
                    targetRootEosDir + "/index.html"
                ])
            except Exception as ex:
                log.warning("Error copying html files to eos: %s", ex)

            # send notification on mattermost channel
            if "MATTERMOST_HOOK" in os.environ:
                content = (
                    "The results of latest PrChecker test `" + str(options) +
                    "` are available on: https://cern.ch/lhcbpr-hlt/PerfTests/UpgradeVelo/"
                    + dirname + "\n```" + "\nEfficiency = " + str(efficiency) +
                    "\nFake rate = " + str(fake_rate) + "\nClone rate = " +
                    str(clone_rate) + "```")
                send_notification_mattermost(os.environ["MATTERMOST_HOOK"],
                                             content)
            else:
                log.warning(
                    "notifications not sent because MATTERMOST_HOOK not set")
