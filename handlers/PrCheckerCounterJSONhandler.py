import os, sys, re
import json
import ROOT
from fnmatch import fnmatchcase as match

from .BaseHandler import BaseHandler
from .gaussValidation import grepPattern

# True or False
global DEBUG
DEBUG = False


def initCounterMap():
    basedict = {
        "TrackMonitor": {
            "#Tracks",
            "#Long",
            "#Downstream",
            "#Ttrack",
            "#Upstream",
            "#Velo",
            "#VeloBackward",
        },
        "fromPrTracksV1Tracks": {"Seeding", "Velo", "Forward", "Match"},
        "CaloClusterEff": {
            "ET250",
            "ET3000",
            "Brem_ET50",
            "mergedPi0_All",
            "resolvedPi0_All",
        },
        "CaloHypo_BkgFrac": {"Electron hypos", "Photon hypos"},
        "CaloHypo_Eff": {"Electron hypo", "Photon hypo"},
    }
    return basedict


def handler_TrackMonitor(dicname, dic, _list):
    location = []
    for listname in sorted(list(_list)):
        location.append(["TrackMonitor", listname, listname])
    drawhistogram(dicname, location, dic, "mean", "{}_mean".format(dicname))
    drawhistogram(dicname, location, dic, "max", "{}_max".format(dicname))


def handler_TracksV1Track(dicname, dic, _list):
    location = []
    for listname in sorted(list(_list)):
        if listname == "Velo":
            location.append([
                "fromPr{}TracksV1TracksMerger".format(listname),
                "Nb of converted Tracks",
                listname,
            ])
        else:
            location.append([
                "fromPr{}TracksV1Tracks".format(listname),
                "Nb of converted Tracks",
                listname,
            ])
    dicname = "fromPrTracksV1Tracks"
    drawhistogram(dicname, location, dic, "sum", "{}_sum".format(dicname))


def handler_CaloClusterEff(dicname, dic, _list):
    location = []
    for listname in sorted(list(_list)):
        location.append([
            "CaloClusterEff_{}".format(listname), "reco efficiency", listname
        ])
    drawhistogram(dicname, location, dic, "efficiency",
                  "{}_efficiency".format(dicname))


def handler_CaloHypoEff_BkgFrac(dicname, dic, _list):
    location = []
    for listname in sorted(list(_list)):
        location.append([
            "CaloHypoEff_ET250",
            "Bkg fraction in {}".format(listname),
            "{}(ET250)".format(listname),
        ])
        location.append([
            "CaloHypoEff_Brem_ET50",
            "Bkg fraction in {}".format(listname),
            "{}(Brem_ET50)".format(listname),
        ])
    drawhistogram(dicname, location, dic, "efficiency", dicname)


def handler_CaloHypoEff_Eff(dicname, dic, _list):
    location = []
    for listname in sorted(list(_list)):
        location.append([
            "CaloHypoEff_ET250",
            "{} efficiency".format(listname),
            "{}(ET250)".format(listname),
        ])
    location.append([
        "CaloHypoEff_Brem_ET50",
        "Photon hypo efficiency".format(listname),
        "Photon hypo(Brem_ET50)".format(listname),
    ])
    drawhistogram(dicname, location, dic, "efficiency", dicname)


def drawhistogram(dicname, location, dic, tupname, title):
    """location(component,name,xtitle)"""
    lisnum = len(location)
    canvas = ROOT.TCanvas(dicname, dicname)
    canvas.SetGrid()
    histo = ROOT.TH1F(dicname, dicname, lisnum, 0.5, 0.5 + lisnum)
    for index, entry in zip(range(lisnum), location):
        entity = [
            c for c in dic
            if c["component"] == entry[0] and c["name"] == entry[1]
        ]
        if len(entity) == 0:
            entity = [{
                "component": entry[0],
                "entity": {
                    tupname: 0.0,
                    "empty": True
                },
                "name": entry[1],
            }]
        if DEBUG:
            print(entity, entry)
        assert len(entity) == 1
        if entity[0]["entity"]["empty"] == False:
            histo.SetBinContent(index + 1, entity[0]["entity"][tupname])
            if tupname == "mean":
                histo.SetBinError(index + 1,
                                  entity[0]["entity"]["standard_deviation"])
            elif tupname == "efficiency":
                histo.SetBinError(index + 1,
                                  entity[0]["entity"]["efficiencyErr"])
        elif entity[0]["entity"]["empty"] == True:
            """zero will not shown in the plot (1.e-20 in case)"""
            histo.SetBinContent(index + 1, 0.0)
            histo.SetBinError(index + 1, 0.0)
        else:
            print(
                "ERROR: Counter ",
                entity[0]["component"],
                " : ",
                entity[0]["name"],
                "is missing empty tuple",
            )
        histo.GetXaxis().SetBinLabel(index + 1, entry[2])
    maxYaxis = histo.GetMaximum() + histo.GetBinError(histo.GetMaximumBin())
    minYaxis = histo.GetMinimum() - histo.GetBinError(histo.GetMinimumBin())
    histo.SetMaximum(maxYaxis + (maxYaxis - minYaxis) / 18.0)
    histo.SetMinimum(minYaxis - (maxYaxis - minYaxis) / 18.0)
    histo.SetName(dicname)
    histo.SetStats(0)
    histo.SetBarWidth(0.3)
    histo.SetBarOffset(0.35)
    histo.SetFillColor(0)
    histo.SetFillStyle(0)
    histo.SetLineWidth(1)
    histo.SetLineColor(ROOT.kRed)
    histo.GetXaxis().SetNdivisions(lisnum)
    histo.GetXaxis().SetLabelFont(132)
    histo.GetXaxis().SetLabelSize(0.03)
    histo.GetXaxis().SetLabelOffset(0.02)
    histo.GetYaxis().SetLabelFont(132)
    histo.GetYaxis().SetLabelSize(0.04)
    histo.GetYaxis().SetTitleFont(132)
    histo.GetYaxis().SetTitleSize(0.06)
    histo.GetYaxis().SetDecimals()
    histo.GetYaxis().CenterTitle(True)
    # histo.GetYaxis().SetTitleOffset(0.8)
    # histo.GetYaxis().SetTitle(ytitle)
    histo.SetTitle(title)
    histo.SetMarkerStyle(20)
    histo.SetMarkerColor(ROOT.kRed)
    histo.Draw("EP")
    canvas.Write(dicname + "_" + tupname + "_canvas")


class PrCheckerCounterJSONhandler(BaseHandler):
    def __init__(self):
        super(self.__class__, self).__init__()

    def collectResultsExt(
            self,
            directory,
            project,
            version,
            platform,
            hostname,
            cpu_info,
            memoryinfo,
            startTime,
            endTime,
            options,
    ):
        logfile = os.path.join(directory, "run.log")
        jsonfile = grepPattern(
            "INFO Writing JSON file (\S+)",
            open(logfile, "r", encoding="ISO-8859-1").read(),
        )
        if DEBUG:
            print("directory", directory)
            print("logfile", logfile)
            print("jsonfile", jsonfile)

        of_full_name = os.path.join(directory, "PrCheckerCountersPlots.root")
        outputfile = ROOT.TFile(of_full_name, "recreate")

        with open(os.path.join(directory, jsonfile), "r") as inputfile:
            json_data = json.load(inputfile)
            basedict = initCounterMap()
            """TrackMonitor"""
            dicname = "TrackMonitor"
            subdic = [c for c in json_data if c["component"] == dicname]
            if DEBUG:
                print(json.dumps(subdic, ensure_ascii=False, indent=4))
            outputfile.mkdir(dicname)
            outputfile.cd(dicname)
            handler_TrackMonitor(dicname, subdic, basedict[dicname])
            """TracksV1Tracks"""
            dicname = "fromPrTracksV1Tracks"
            subdic = [
                c for c in json_data
                if match(c["component"], "fromPr*TracksV1Tracks*")
                and match(c["name"], "Nb of converted Tracks")
            ]
            if DEBUG:
                print(json.dumps(subdic, ensure_ascii=False, indent=4))
            outputfile.mkdir(dicname)
            outputfile.cd(dicname)
            handler_TracksV1Track(dicname, subdic, basedict[dicname])
            """CaloClusterEff"""
            dicname = "CaloClusterEff"
            subdic = [
                c for c in json_data if match(c["component"], dicname + "_*")
                and c["name"] == "reco efficiency"
            ]
            if DEBUG:
                print(json.dumps(subdic, ensure_ascii=False, indent=4))
            outputfile.mkdir(dicname)
            outputfile.cd(dicname)
            handler_CaloClusterEff(dicname, subdic, basedict[dicname])
            """CaloHypo_BkgFrac"""
            dicname = "CaloHypo_BkgFrac"
            subdic = [
                c for c in json_data if match(c["component"], "CaloHypoEff_*")
                and match(c["name"], "Bkg fraction*")
            ]
            if DEBUG:
                print(json.dumps(subdic, ensure_ascii=False, indent=4))
            outputfile.mkdir(dicname)
            outputfile.cd(dicname)
            handler_CaloHypoEff_BkgFrac(dicname, subdic, basedict[dicname])
            """CaloHypo_Eff"""
            dicname = "CaloHypo_Eff"
            subdic = [
                c for c in json_data if match(c["component"], "CaloHypoEff_*")
                and match(c["name"], "*efficiency")
            ]
            if DEBUG:
                print(json.dumps(subdic, ensure_ascii=False, indent=4))
            outputfile.mkdir(dicname)
            outputfile.cd(dicname)
            handler_CaloHypoEff_Eff(dicname, subdic, basedict[dicname])

        outputfile.Write()
        outputfile.Close()
        self.saveFile("PrCheckerCountersPlots", of_full_name)
