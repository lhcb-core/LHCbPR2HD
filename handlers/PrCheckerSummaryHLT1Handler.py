# Handler for summarizing the output of PrChecker2 for HLT1 jobs.
#
# It extracts the most interesting histograms and also
# produces efficiency histograms of reconstructed vs
# reconstructible tracks for different tracking categories.
#
# The 1D efficiencies are calculated usig TGraphAsymmErrors
# and Bayesian error bars
# The 2D efficiencies are calculated via TH2 and using standard error calculation.
#
# Based on: PrCheckerSummaryHLT1Handler
# author: Miroslav Saur (miroslav.saur@cern.ch)
# Version:
# 	   - 0.1 (2021/02): basic plots and efficiencies
#          - 0.2 (2022/07): keeping only forward tracks, as no other are currently used
# author: Guanyue Wan (wanguanyue@cern.ch)
# Version:
# 	   - 0.3 (2023/04): adding other tracks, including types of `Forward, Velo, Upstream` and `ForwardUTHits`, as well as categories of `Long`, `Long_fromB`, `Long_electron`, etc.
#                       also complete branch names with certain hash numbers.

import os
import sys
import argparse
import ROOT
from ROOT import gStyle
from ROOT import gROOT
from ROOT import TStyle
from ROOT import gPad
from .BaseHandler import BaseHandler
from .gaussValidation import grepPattern
from .utils import HashRemover
from collectRunResults import urlopen
import logging
import subprocess

from collections import defaultdict


def effPlotsTypes():
    """Return required types of track"""
    return ["Forward", "Velo", "Upstream", "ForwardUTHits"]


def effPlotsDict(track_type, track_category, kinematics):
    """
    Set the directory to required histograms in root file,
    the title along x&y axis of output efficiency histograms.

    Args:
            track_type (string): Type of track, within `Forward, Velo, Upstream` and `ForwardUTHits`.
            track_category (string): Category of required tracks within `Long`, `Long_fromB`, `Long_electron` etc,
                    should be different for different `track_type`s.
            kinematics (list): Required kinematic variables within `["Eta","Phi","nPV","P","Pt"]`
    """

    # directory name in the root file
    # the hash numbers are manually given for certain build version
    type_branch = {
        "Forward": "ForwardTrackChecker",
        "Velo": "VeloTrackChecker",
        "Upstream": "UpstreamTrackChecker",
        "ForwardUTHits": "ForwardUTHitsChecker",
    }[track_type]

    # kinematic varibales to plot efficiency on
    basedict = dict([(key, {}) for key in kinematics])

    Kin_units = {
        "Eta": "#eta [-]",
        "Phi": "#phi [rad?]",
        "nPV": "nPV [-]",
        "P": "p [MeV]",
        "Pt": "#it{p}_{T} [MeV]",
        "docaz": "docaz [mm]",
    }

    for variable in kinematics:
        basedict[variable][
            "path"] = f"Track/{type_branch}/{track_type}/{track_category}_{variable}"
        if track_category == "Ghosts":
            basedict[variable][
                "path"] = f"Track/{type_branch}/{track_type}/{variable}"
        basedict[variable]["unit"] = Kin_units[variable]
        basedict[variable]["unit_y"] = "Effciency [-]"

    return basedict


def getEffPlots(track_type):
    """
    Return the kinematic variables for certain category.

    Args:
            track_type (string): Type of track, within `Forward, Velo, Upstream` and `ForwardUTHits`.
    """

    kinematics = ["Eta", "Phi", "nPV", "P", "Pt"]
    if track_type == "ForwardUTHits":
        kinematics = ["Eta", "Phi", "P", "Pt"]
    return kinematics


def effPlotsCategories(track_type):
    """
    Return the required categories of different track types.

    Args:
            track_type (string): Type of track, within `Forward, Velo, Upstream` and `ForwardUTHits`.
    """
    Categories = {
        "Forward": [
            "01_long",
            "05_long_fromB",
            "07_long_electrons",
            "08_long_fromB_electrons",
            "Ghosts",
        ],
        "ForwardUTHits":
        ["01_long", "03_long_fromB_P>3GeV_Pt>0.5GeV", "Ghosts"],
        "Upstream": [
            "02_velo+UT",
            "09_long_fromB",
            "12_long_fromB_electrons",
            "Ghosts",
        ],
        "Velo":
        ["01_velo", "06_long_fromB", "09_long_fromB_electrons", "Ghosts"],
    }[track_type]

    return Categories


class PrCheckerSummaryHLT1Handler(BaseHandler):
    def __init__(self):
        super(self.__class__, self).__init__()

    def collectResultsExt(
            self,
            directory,
            project,
            version,
            platform,
            hostname,
            cpu_info,
            memoryinfo,
            startTime,
            endTime,
            options,
    ):
        logfile = os.path.join(directory, "run.log")
        rootfile = grepPattern(
            "INFO Writing ROOT histograms to: (\S+)",
            open(logfile, "r", encoding="ISO-8859-1").read(),
        )
        modifier = HashRemover.HashRemover(os.path.join(directory, rootfile))
        modifier.modify_root_file()
        inputfile = ROOT.TFile.Open(os.path.join(directory, rootfile), "read")
        outputfile = ROOT.TFile("PrCheckerSummaryHLT1.root", "recreate")

        from .utils.LHCbStyle import setLHCbStyle
        from .utils.Legend import place_legend

        setLHCbStyle()

        latex = ROOT.TLatex()
        latex.SetNDC()
        latex.SetTextSize(0.03)

        Track_Types = effPlotsTypes()
        for track_type in Track_Types:
            effPlots = getEffPlots(track_type)
            outputfile.mkdir(track_type)

            for track_category in effPlotsCategories(track_type):
                if not track_category == "Ghosts":
                    eff_dict = effPlotsDict(track_type, track_category,
                                            effPlots)

                    for effPlot in effPlots:
                        outputfile.cd(track_type)
                        effNom = inputfile.Get(eff_dict[effPlot]["path"] +
                                               "_reconstructed")
                        effDenom = inputfile.Get(eff_dict[effPlot]["path"] +
                                                 "_reconstructible")
                        canvas_eff = ROOT.TCanvas(effPlot, effPlot)
                        efficiency = ROOT.TGraphAsymmErrors()
                        efficiency.SetName(effPlot + "_tracking_efficiency")
                        efficiency.Divide(effNom, effDenom, "w")

                        efficiency.SetMarkerSize(0.5)
                        efficiency.SetLineWidth(1)
                        efficiency.SetMarkerColor(ROOT.kBlack)
                        efficiency.SetMarkerStyle(5)
                        efficiency.GetXaxis().SetLabelFont(132)
                        efficiency.GetXaxis().SetLabelSize(0.04)
                        efficiency.GetXaxis().SetTitleFont(132)
                        efficiency.GetXaxis().SetTitleSize(0.06)
                        efficiency.GetXaxis().SetTitleOffset(0.8)
                        efficiency.GetYaxis().SetLabelFont(132)
                        efficiency.GetYaxis().SetLabelSize(0.04)
                        efficiency.GetYaxis().SetTitleFont(132)
                        efficiency.GetYaxis().SetTitleSize(0.06)
                        efficiency.GetYaxis().SetDecimals()
                        efficiency.GetYaxis().SetTitleOffset(0.7)
                        efficiency.GetXaxis().SetTitle(
                            eff_dict[effPlot]["unit"])
                        efficiency.GetYaxis().SetTitle(
                            eff_dict[effPlot]["unit_y"])
                        efficiency.SetTitle(
                            "{path} reconstructed / reconstructible".format(
                                path=eff_dict[effPlot]["path"]))

                        efficiency.Draw("AP")
                        efficiency.Write(track_category + "_" + effPlot +
                                         "_tgraph")
                        canvas_eff.Write(track_category + "_" + effPlot +
                                         "_canvas")
                # the case of ghost tracks
                else:
                    eff_dict = effPlotsDict(track_type, "Ghosts", effPlots)

                    for effPlot in effPlots:
                        outputfile.cd(track_type)
                        effNom = inputfile.Get(eff_dict[effPlot]["path"] +
                                               "_Ghosts")
                        effDenom = inputfile.Get(eff_dict[effPlot]["path"] +
                                                 "_Total")
                        canvas_eff = ROOT.TCanvas(effPlot, effPlot)
                        efficiency = ROOT.TGraphAsymmErrors()
                        efficiency.SetName(effPlot + "_tracking_efficiency")
                        efficiency.Divide(effNom, effDenom, "w")

                        efficiency.SetMarkerSize(0.5)
                        efficiency.SetLineWidth(1)
                        efficiency.SetMarkerColor(ROOT.kBlack)
                        efficiency.SetMarkerStyle(5)
                        efficiency.GetXaxis().SetLabelFont(132)
                        efficiency.GetXaxis().SetLabelSize(0.04)
                        efficiency.GetXaxis().SetTitleFont(132)
                        efficiency.GetXaxis().SetTitleSize(0.06)
                        efficiency.GetXaxis().SetTitleOffset(0.8)
                        efficiency.GetYaxis().SetLabelFont(132)
                        efficiency.GetYaxis().SetLabelSize(0.04)
                        efficiency.GetYaxis().SetTitleFont(132)
                        efficiency.GetYaxis().SetTitleSize(0.06)
                        efficiency.GetYaxis().SetDecimals()
                        efficiency.GetYaxis().SetTitleOffset(0.7)
                        efficiency.GetXaxis().SetTitle(
                            eff_dict[effPlot]["unit"])
                        efficiency.GetYaxis().SetTitle(
                            eff_dict[effPlot]["unit_y"])
                        efficiency.SetTitle("{path} Ghosts / Total".format(
                            path=eff_dict[effPlot]["path"]))

                        efficiency.Draw("AP")
                        efficiency.Write(track_category + "_" + effPlot +
                                         "_tgraph")
                        canvas_eff.Write(track_category + "_" + effPlot +
                                         "_canvas")

        outputfile.Write()
        outputfile.Close()
        inputfile.Close()
        self.saveFile("PrCheckerSummaryHLT1", "PrCheckerSummaryHLT1.root")
