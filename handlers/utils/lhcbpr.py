import logging
import requests

log = logging.getLogger(__name__)


class JobNotFound(Exception):
    pass


def get_lhcbpr_application_id(name):
    """Return LHCbPR application id for a given application name."""
    r = requests.get("https://lblhcbpr.cern.ch/api/active/applications/")
    r.raise_for_status()
    data = r.json()
    assert data["count"] == len(data["results"])
    (app_id, ) = [
        app["id"] for app in data["results"]
        if app["name"].upper() == name.upper()
    ]
    return app_id


def get_job_ids(slot, build_id, application, options):
    """Return job ids for an application/options/version."""
    results = []
    url = (f"https://lblhcbpr.cern.ch/metrics/jobs?"
           f"app={application}&versions={slot}.{build_id}&options={options}&"
           f"format=json&sortby=id&exact=true")
    # In the unlikely case there are multiple pages of results, fetch all.
    while url:
        r = requests.get(url)
        r.raise_for_status()
        data = r.json()
        results += data["results"] or []
        url = "https://" + data["next"] if "next" in data else None

    assert all(x["version"] == f"{slot}.{build_id}" for x in results)
    ids = [x["id"] for x in results]
    log.debug(
        f"Found jobs {ids} for {slot}/{build_id} {application} {options}")
    return ids


def get_latest_job_id(slot, build_id, application, options):
    """Return the latest matching job (warning if multiple) or raise."""
    ids = get_job_ids(slot, build_id, application, options)
    if not ids:
        raise JobNotFound()
    elif len(ids) > 1:
        ids.sort()
        log.warning(
            f"Multiple jobs ({ids}) for {slot}.{build_id}/{options}. Taking the last."
        )
    return ids[-1]


def get_job_results(job_id):
    """Return the registered results for a job."""
    r = requests.get(
        f"https://lblhcbpr.cern.ch/api/jobs/{job_id}/results/?format=json")
    r.raise_for_status()

    data = r.json()
    assert data["count"] == len(data["results"])

    def value(result):
        dtype = result["attr"]["dtype"]
        if dtype == "Float":
            return float(result["value"])
        elif dtype == "Integer":
            return int(result["value"])
        return result["value"]

    # arrange results per name and parse value strings
    results = {
        res["attr"]["name"]: {
            "value": value(res),
            "attr": res["attr"]
        }
        for res in data["results"]
    }
    if len(results) < len(data["results"]):
        log.warning(f"Results with duplicate names ignored: {data}")
    return results


def get_latest_job_results(slot, build_id, application, options):
    """Return the results for the latest matching job or raise."""
    return get_job_results(
        get_latest_job_id(slot, build_id, application, options))
