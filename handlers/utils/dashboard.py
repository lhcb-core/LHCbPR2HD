import logging
from functools import lru_cache
from LbNightlyTools.Utils import Dashboard
from couchdb.http import ResourceNotFound

log = logging.getLogger(__name__)


@lru_cache(maxsize=None)
def get_nightly_dashboard():
    return Dashboard()


@lru_cache(maxsize=None)
def get_periodic_dashboard():
    return Dashboard(flavour="periodic")


def get_test_doc(version, options):
    """Get the document for a test from the periodic test dashboard."""
    past_tests = [
        row.doc for row in get_periodic_dashboard().db.iterview(
            "nightlies_summaries/by_app_version",
            batch=10,
            key=version,
            include_docs=True,
        )
    ]
    docs = [doc for doc in past_tests if options == doc["opt_name"]]
    if not docs:
        raise ResourceNotFound(f"Can't find test {options} for slot {version}")
    elif len(docs) > 1:
        docs.sort(key=lambda x: x["time_start"], reverse=True)
        log.warning(
            f"Multiple ({len(docs)}) tests found for test {options} for slot {version}. "
            "Taking the latest.")
    return docs[0]


def get_mr_slots_by_ref_slot(slot, build_id):
    """Return *-mr slots corresponding to a *-ref slot."""
    return [
        doc.doc for doc in get_nightly_dashboard().db.iterview(
            "merge_requests/mr_slots_by_ref_slot",
            batch=100,
            include_docs=True,
            key=[slot, build_id],
        )
    ]


def get_nightly_doc(slot, build_id):
    """Get the document from the nightly dashboard."""
    return get_nightly_dashboard().db[f"{slot}.{build_id}"]


def get_ci_test_pairs(slot, build_id):
    """Return tuples of ref build, test builds and GitLab trigger.

    If the slot corresponds to a test (-mr) build, a list with a single
    tuple is returned. Otherwise, the list of all corresponding ref-test
    pairs are returned.

    Returns:
        A list of tuples of the format
        ((ref_slot, ref_build_id), (test_slot, test_build_id), trigger)

    """
    doc = get_nightly_doc(slot, build_id)
    metadata = doc["config"]["metadata"].get("ci_test", {})
    del doc
    # it's an integration slot
    if "reference" in metadata:
        ref_slot, ref_build_id = metadata["reference"]
        log.info(f"Determined ref build to be {ref_slot}.{ref_build_id}")
        return [((ref_slot, ref_build_id), (slot, build_id),
                 metadata["trigger"])]

    # it's a reference slot
    else:
        mr_slots = [(
            doc["slot"],
            doc["build_id"],
            doc["config"]["metadata"]["ci_test"]["trigger"],
        ) for doc in get_mr_slots_by_ref_slot(slot, build_id)]

        if not mr_slots:
            log.warning(
                f"Found no corresponding test builds for {slot}.{build_id}")

        return [((slot, build_id), (test_slot, test_build_id), trigger)
                for test_slot, test_build_id, trigger in mr_slots]
