from collections import defaultdict

Velo_sel_raw = {
    "Velo_eta25":
    "velo track, 2 < eta < 5",
    "Velo_hasnoUTSeed_eta25":
    "velo track, not UT, Scifi hits, 2 < eta < 5",
    "Velo_isLong_eta25":
    "Velo track, has Scifi hits, 2 < eta < 5",
    "Velo_hasVeloUTSeed_eta25":
    "Velo track, has UT and Scifi hits, 2 < eta < 5",
    "Velo_hasVeloSeed_eta25":
    "Velo track, has Scifi hits, has no UT hits, 2 < eta < 5",
    "Velo":
    "velo track",
    "Velo_hasnoUTSeed":
    "velo track, not UT, Scifi hits",
    "Velo_isLong":
    "Velo track, has Scifi hits",
    "Velo_hasVeloUTSeed":
    "Velo track, has UT and Scifi hits",
    "Velo_hasVeloSeed":
    "Velo track, has Scifi hits, has no UT hits",
}

Upstream_sel_raw = {
    "Upstream_eta25": "Upstream track, 2 < eta < 5",
    "Upstream_hasSeed_eta25": "Upstream track, has UT hits, 2 < eta < 5",
    "Upstream_hasnoSeed_eta25": "Upstream track, has no UT hits, 2 < eta < 5",
}

Ttrack_sel_raw = {
    "Seed_eta25": "Ttrack, 2 < eta < 5",
    "Seed_hasnoVeloUT_eta25": "Ttrack, without Velo or UT hits, 2 < eta < 5",
    "Seed_isLong_eta25": "Ttrack, with Velo hits, 2 < eta < 5",
    "Seed_isDown_eta25": "Ttrack, with UT hits, 2 < eta < 5",
}

Down_sel_raw = {
    "Down_eta25": "Downstream track, 2 < eta < 5",
    "Down_hasnoVelo_eta25": "Downstream track, without Velo hits, 2 < eta < 5",
    "Down_hasVelo_eta25": "Downstream track, with Velo hits, 2 < eta < 5",
}

Forward_sel_raw = {
    "Long_eta25": "Long track, 2 < eta < 5",
    "Long_hasUT_eta25": "Long track, with UT hits, 2 < eta < 5",
    "Long_hasnoUT_eta25": "Long track, without UT hits, 2 < eta < 5",
}


def efficiencyHistoDict():
    basedict = {
        "eta": {},
        "p": {},
        "pt": {},
        "phi": {},
        "nPV": {},
        "docaz": {},
    }

    basedict["eta"]["xTitle"] = "#eta"
    basedict["eta"]["variable"] = "Eta"

    basedict["p"]["xTitle"] = "p [MeV]"
    basedict["p"]["variable"] = "P"

    basedict["pt"]["xTitle"] = "p_{T} [MeV]"
    basedict["pt"]["variable"] = "Pt"

    basedict["phi"]["xTitle"] = "#phi [rad]"
    basedict["phi"]["variable"] = "Phi"

    basedict["nPV"]["xTitle"] = "# of PVs"
    basedict["nPV"]["variable"] = "nPV"

    basedict["docaz"]["xTitle"] = "docaz [mm]"
    basedict["docaz"]["variable"] = "docaz"

    return basedict


def enrich_selections(
        mysel_raw,
):  # Give a dict as input, return a dict with enriched selections. The same function in PrCheckerEfficiencyPlots_HLT2.py.
    sel_raw = mysel_raw
    if not type(sel_raw) == dict:
        print(
            "ERROR! The function 'enrich_selections' requires a dict as input."
        )
        exit()
    # Add the "FromB, FromD, Strange" labels before "_electrons" or "_notElectrons".
    for sel_label in list(sel_raw.keys()):
        sel_content = sel_raw[sel_label]
        sel_label_FromB = sel_label + "_FromB"
        sel_label_FromD = sel_label + "_FromD"
        sel_label_Strange = sel_label + "_Strange"
        sel_content_FromB = sel_content + ", from b decay"
        sel_content_FromD = sel_content + ", from c decay"
        sel_content_Strange = sel_content + ", from s decay"
        sel_raw[sel_label_FromB] = sel_content_FromB
        sel_raw[sel_label_FromD] = sel_content_FromD
        sel_raw[sel_label_Strange] = sel_content_Strange
    # Add the "isDecay, PairProd, fromHI" labels before "_electrons" or "_notElectrons"
    for sel_label in list(sel_raw.keys()):
        sel_content = sel_raw[sel_label]
        sel_label_isDecay = sel_label + "_isDecay"
        sel_label_PairProd = sel_label + "_PairProd"
        sel_label_fromHI = sel_label + "_fromHI"
        sel_content_isDecay = sel_content + ", from Decay"
        sel_content_PairProd = sel_content + ", from Pair Production"
        sel_content_fromHI = sel_content + ", from Hadronic Interaction"
        sel_raw[sel_label_isDecay] = sel_content_isDecay
        sel_raw[sel_label_PairProd] = sel_content_PairProd
        sel_raw[sel_label_fromHI] = sel_content_fromHI
    return sel_raw


Velo_sels = enrich_selections(Velo_sel_raw)
Upstream_sels = enrich_selections(Upstream_sel_raw)
Forward_sels = enrich_selections(Forward_sel_raw)
Down_sels = enrich_selections(Down_sel_raw)
Ttrack_sels = enrich_selections(Ttrack_sel_raw)


def getCuts():
    basedict = {
        "Velo": {},
        "Upstream": {},
        "Forward": {},
        "TTrack": {},
        "Downstream": {},
        "Match": {},
        "Best": {},
        "BestLong": {},
        "BestDown": {},
    }

    basedict["Velo"] = list(Velo_sels.keys())

    basedict["Upstream"] = list(Upstream_sels.keys())

    basedict["Forward"] = list(Forward_sels.keys())
    basedict["Match"] = list(Forward_sels.keys())
    basedict["Best"] = list(Forward_sels.keys())
    basedict["BestLong"] = list(Forward_sels.keys())

    basedict["Downstream"] = list(Down_sels.keys())
    basedict["BestDown"] = list(Down_sels.keys())

    basedict["TTrack"] = list(Ttrack_sels.keys())

    return basedict


def categoriesDict():
    basedict = defaultdict(lambda: defaultdict(dict))
    # Velo tracks
    Velo_list = ["Velo"]
    for Velo_algo in Velo_list:
        for Velo_sel in list(Velo_sels.keys()):
            basedict[Velo_algo][Velo_sel]["title"] = Velo_sels[Velo_sel]

    # Up tracks
    Up_list = ["Upstream"]
    for Up_algo in Up_list:
        for Upstream_sel in list(Upstream_sels.keys()):
            basedict[Up_algo][Upstream_sel]["title"] = Upstream_sels[
                Upstream_sel]

    # Forward tracks
    Forward_list = ["Forward", "Match", "Best", "BestLong"]
    for Forward_algo in Forward_list:
        for Forward_sel in list(Forward_sels.keys()):
            basedict[Forward_algo][Forward_sel]["title"] = Forward_sels[
                Forward_sel]

    # Down tracks
    Down_list = ["Downstream", "BestDown"]
    for Down_algo in Down_list:
        for Down_sel in list(Down_sels.keys()):
            basedict[Down_algo][Down_sel]["title"] = Down_sels[Down_sel]

    # TTrack
    TTrack_list = ["TTrack"]
    for Ttrack_algo in TTrack_list:
        for Ttrack_sel in list(Ttrack_sels.keys()):
            basedict[Ttrack_algo][Ttrack_sel]["title"] = Ttrack_sels[
                Ttrack_sel]

    return basedict
