import logging
import os
import subprocess

import gitlab
import requests

from functools import lru_cache

GITLAB_URL = "https://gitlab.cern.ch/"

log = logging.getLogger(__name__)


@lru_cache(maxsize=None)
def get_gitlab_server():
    return gitlab.Gitlab(GITLAB_URL, private_token=os.getenv("GITLAB_TOKEN"))


def upload_eos_www(source, destination, baseurl=None, force=False):
    """Copy a file to EOS via xrdcp."""
    baseurl = baseurl or os.getenv("LHCBPR_WWW_EOS")
    if not baseurl:
        log.error(
            f"LHCBPR_WWW_EOS env is not set. Will not upload {source} to EOS")
    else:
        # don't use urljoin here as it can't cope with root://
        full_destination = os.path.join(baseurl, destination)
        log.info(f"Uploading {source} to {full_destination}")
        try:
            cmd = ["xrdcp"]
            if force:
                # use case is to overwrite files already present
                cmd.append("-f")
            cmd += [source, full_destination]
            log.debug("Calling {}".format(" ".join(map(repr, cmd))))
            subprocess.check_call(cmd)
        except subprocess.CalledProcessError as e:
            log.error(f"Upload failed: {e}")


def post_mattermost(message, webhook=None):
    """Post a message to a Mattermost web hook."""
    webhook = webhook or os.getenv("MATTERMOST_HOOK")
    if not webhook:
        log.error("MATTERMOST_HOOK env not set. Will not post to Mattermost")
    else:
        try:
            r = requests.post(webhook, json={"text": message})
            r.raise_for_status()
        except requests.HTTPError as e:
            log.error(f"Failed to post to Mattermost: {e}")


def post_gitlab_feedback(trigger_source,
                         message,
                         add_labels=[],
                         remove_labels=[]):
    """Post feedback to GitLab for a ci-test trigger.

    Args:
        message: The comment to post to the ci-test discussion.
        add_labels (optional): A list of labels to be assigned to the MR.
        remove_labels (optional): A list of label to be removed from the MR.

    """
    log.info(
        f"Posting GitLab feedback:\nmessage={message}\nadd_labels={add_labels}\nremove_labels={remove_labels}"
    )

    if not os.getenv("GITLAB_TOKEN"):
        log.error("GITLAB_TOKEN env not set. Will not post to GitLab")
        return

    try:
        gitlab_server = get_gitlab_server()
        project = gitlab_server.projects.get(trigger_source["project_id"])
        mr = project.mergerequests.get(trigger_source["merge_request_iid"])
        discussion = mr.discussions.get(trigger_source["discussion_id"])
        # reply to discussion
        discussion.notes.create({"body": message})
        # remove labels
        for label in remove_labels:
            log.info(f"Removing label: {label} from MR")
            try:
                mr.labels.remove(label)
            except ValueError:
                pass

        # add labels to MR (creates a project label if not existing,
        # noop if already labeled)
        for label in add_labels:
            log.info(f"Adding label: {label} to MR")
            mr.labels.append(label)

        mr.save()
    except gitlab.GitlabError as e:
        # never fail when feedback can't be posted
        log.error("Could not post feedback to gitlab")
