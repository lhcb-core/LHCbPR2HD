#########################################################################
## Handler for Muon tests in Gauss, takes a root file containing       ##
## muon test data and saves as JSON. Muon test data will be            ##
## located in the MuonTestResults folder after running runmuontest.py  ##
## MUST be run in the same directory as runmuontest.py is              ##
## @Author: R.Calladine                                                ##
## @date:   Last modified 2017-03-24                                   ##
#########################################################################

import json, os
from ROOT import TFile, TH1D, TTree, gDirectory

from .BaseHandler import BaseHandler


def labelHist(hist, xtitle, ytitle):
    hist.SetXTitle(xtitle)
    hist.GetXaxis().CenterTitle()
    hist.SetYTitle(ytitle)
    hist.GetYaxis().CenterTitle()


class MuonMoniTestHandler(BaseHandler):
    def __init__(self):
        super(self.__class__, self).__init__()

    def collectResults(self, directory):
        inroot = "MuonMoniSim_histos.root"
        inpath = os.path.join(directory, inroot)
        infile = TFile.Open(inpath)

        #####  Collects data from MuonMoniSim output  #####

        #   MuonHitChecker Histograms in the input file are numbered 1000-1019 for time multiplicities
        #                                            2000-2019 for radial multiplicites
        #  Below gets the histograms and stores them in lists

        # histID_TM = [infile.Get('MuonHitChecker/1{0:03}'.format(i)) for i in range(0, 20)]
        # histID_RM = [infile.Get('MuonHitChecker/2{0:03}'.format(i)) for i in range(0, 20)]

        #  Collects histograms for the MuonMultipleScatteringChecker and labels the axes
        hist_MuSct = []
        for i in range(1, 5):
            labelHist(
                infile.Get(
                    "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/dxdTx_MF{}"
                    .format(i)),
                "Angular Deviation [mRad]",
                "Displacement [mm]",
            )
            hist_MuSct.append(
                infile.Get(
                    "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/dxdTx_MF{}"
                    .format(i)))

            labelHist(
                infile.Get(
                    "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/dydTy_MF{}"
                    .format(i)),
                "Angular Deviation [mRad]",
                "Displacement [mm]",
            )
            hist_MuSct.append(
                infile.Get(
                    "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/dydTy_MF{}"
                    .format(i)))

            labelHist(
                infile.Get(
                    "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/pdx_MF{}"
                    .format(i)),
                "Displacement [mm]",
                "Particle Momentum [GeV/c]",
            )
            hist_MuSct.append(
                infile.Get(
                    "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/pdx_MF{}"
                    .format(i)))

            labelHist(
                infile.Get(
                    "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/pdy_MF{}"
                    .format(i)),
                "Displacement [mm]",
                "Particle Momentum [GeV/c]",
            )
            hist_MuSct.append(
                infile.Get(
                    "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/pdy_MF{}"
                    .format(i)))

            labelHist(
                infile.Get(
                    "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/pdTx_MF{}"
                    .format(i)),
                "Angular Deviation [mRad]",
                "Particle Momentum [GeV/c]",
            )
            hist_MuSct.append(
                infile.Get(
                    "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/pdTx_MF{}"
                    .format(i)))

            labelHist(
                infile.Get(
                    "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/pdTy_MF{}"
                    .format(i)),
                "Angular Deviation [mRad]",
                "Particle Momentum [GeV/c]",
            )
            hist_MuSct.append(
                infile.Get(
                    "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/pdTy_MF{}"
                    .format(i)))

        # Performs manipulation of the MuonHitChecker histograms
        # Mu_Station = 0
        # Creates a dictionary to hold the histograms, key being their titles and their values being
        # the data from the corresponding histograms
        # MuHitCheck_dict = {}

        # Adds each of the 4 histograms for each of the muon stations together
        # for n in range(0,20):
        # if n % 4 == 0:
        # Mu_Station += 1
        # TM_hist = histID_TM[n].Clone("Time_multiplicity_M{mu}".format(mu=Mu_Station))
        # RM_hist = histID_RM[n].Clone("Radial_multiplicity_M{mu}".format(mu=Mu_Station))

        # Ensures every four histograms are added together, remembering we start from 1000 or 2000
        # else:
        # TM_hist.Add(histID_TM[n],1)
        # RM_hist.Add(histID_RM[n],1)
        # if n != 0 and n % 4 == 3:
        # TM_hist.SetTitle('Time Multiplicity M{mu}'.format(mu = Mu_Station))
        # RM_hist.SetTitle('Radial Multiplicity M{mu}'.format(mu = Mu_Station))
        # MuHitCheck_dict[TM_hist.GetName()] = TM_hist
        # MuHitCheck_dict[RM_hist.GetName()] = RM_hist

        # Puts MuonMultipleScatteringChecker into a dictionary of nice format for writing to ROOT file

        print(hist_MuSct)

        MuSct_dict = {j.GetName(): j
                      for j in hist_MuSct
                      }  # dictionary of form 'name of histogram' : 'histogram'

        # Saves histograms in a root file
        outfile = TFile("MuonMoniOut.root", "NEW")

        # for key in MuHitCheck_dict:
        # MuHitCheck_dict[key].Write(key)

        for key in MuSct_dict:
            MuSct_dict[key].Write(key)

        self.saveFile("MuonMoniSimRes", "./MuonMoniOut.root")
