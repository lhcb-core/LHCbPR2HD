import logging
import os
import re

import jinja2
import requests

from .BaseHandler import BaseHandler
from .utils import dashboard
from .utils import publish

log = logging.getLogger(__name__)

NAN = float("nan")

WWW_BASE_URL = "https://cern.ch/lhcbpr-hlt/PerfTests/UpgradeThroughput"

REPORT_TEMPLATE = jinja2.Template("""
<html>
<head></head>
<body>
<p>
    slot.build_id: {{version}}<br>
    platform: {{platform}}<br>
    hostname: {{hostname}}<br>
    cpu_info: {{cpu_info}}
</p>
<ul>
    <li>Throughput = {{throughput}} Events/s</li>
    {% if trend_url %}
    <li><a href="{{trend_url}}">Trend plot</a></li>
    {% endif %}
    <li><a href="{{WWW_BASE_URL}}/{{dirname}}/tests.log">Logs</a></li>
</ul>
<object type="image/png" data="FlameBars.png"></object>
<p>
    Breakdown of the throughput rate. Shown are the algorithms that consumed
    most of the CPU time. Here's the <a href='FlameBars.pdf'>pdf</a> version.
</p>
<object type="image/svg+xml" data="flamy.svg"></object>
<p>
    The x-axis shows the stack profile population, sorted alphabetically
    (it is not the passage of time), and the y-axis shows stack depth,
    counting from zero at the bottom. Each rectangle represents a stack frame.
    The wider a frame is, the more often it was present in the stacks.
    The top edge shows what is on-CPU, and beneath it is its ancestry.
    The colors are usually not significant, picked randomly to
    differentiate frames.
</p>
<p>
    For more info see: <a href="http://www.brendangregg.com/flamegraphs.html">
    Flame Graphs additional Info</a>
</p>
<ul>
    <li><a href="{{WWW_BASE_URL}}">Show all tests</a></li>
    <li><a href="{{WWW_BASE_URL}}/index_list.php?opt={{options}}">
        Show all tests for {{options}}</a></li>
</ul>
</body>
</html>
""")


def get_throughput(file, pattern=r"Evts\/s = ([\d.]+)"):
    try:
        with open(file) as f:
            values = [
                float(match.group(1))
                for match in re.finditer(pattern, f.read())
            ]
    except FileNotFoundError:
        log.error(f"Could not open {file} to extract throughput.")
        return NAN
    if not values:
        log.error(f"No throughput match for {pattern} in {file}.")
        return NAN
    if len(values) > 1:
        log.error(f"More than one throughput match for {pattern} in {file}.")
        return NAN
    return values[0]


def get_couchdb_throughput_link(slot, build_id, options):
    """Get throughput value and report page URL from CouchDB."""
    # WARNING the gymnastics below of obtaining the throughput
    # from the run.log file instead of accessing LHCbPR is done beacause
    # the couchdb gets updated directly after a test is run.
    # This is NOT the case for LHCbPR, thus we can not guarantee that LHCbPR
    # will contain the results of the first test by the time we need to access
    # them when running this handler for the 2nd test.
    test = dashboard.get_test_doc(f"{slot}.{build_id}", options)

    # get path to the run.log file on EOS, and get rid of the https stuff in the front
    # since eos is mounted on lbhltperf01 so we can simply open it via absolute path
    # Then use that path to read the throughput from the run.log file
    run_log = test["run_log"]
    m = re.match(r"^https://.*\.cern\.ch/(/eos/.*)$", run_log)
    if m:
        throughput = get_throughput(m.group(1))
    else:
        log.error(f"run_log {run_log!r} does not match pattern")
        throughput = NAN
    # TODO should we handle the url differently?
    return throughput, test.get(
        "lhcbpr_url",
        f"https://Failed_to_retrieve_lhcbpr_link_for_{slot}.{build_id}")


def send_gitlab_feedback(
        new_throughput,
        ref_throughput,
        options,
        web_link,
        ref_web_link,
        trigger_source,
):
    throughput_change = (new_throughput - ref_throughput) / ref_throughput

    if "hlt1" in options:
        tol = 0.005
        prefix = "hlt1"
    elif "spruce" in options:
        tol = 0.05
        prefix = "spruce"
    else:
        tol = 0.01
        prefix = "hlt2"

    add_labels = []
    remove_labels = [
        prefix + "-throughput-increased", prefix + "-throughput-decreased"
    ]
    thumb = ""

    if throughput_change > tol:
        add_labels.append(prefix + "-throughput-increased")
        thumb = ":thumbsup:"
    elif throughput_change < -tol:
        add_labels.append(prefix + "-throughput-decreased")
        thumb = ":thumbsdown:"

    # ok we made it this far, we are ready to talk to GitLab
    message = (
        f"Throughput Test [{options}]({web_link}): "
        f"{new_throughput:.1f} Events/s -- change of {throughput_change:.2%} "
        f"vs. [reference]({ref_web_link}) {thumb}")

    # we only want to actually apply labels based on these two
    # each represents the current baseline for hlt1 and hlt2
    if options not in [
            "MooreOnline_hlt1_pp_forward_then_matching_no_ut",
            "Moore_hlt2_pp_thor"
    ]:
        add_labels = []
        remove_labels = []

    publish.post_gitlab_feedback(
        trigger_source,
        message,
        add_labels=add_labels,
        remove_labels=remove_labels,
    )


class ThroughputProfileHandler(BaseHandler):
    def __init__(self):
        super().__init__()

    def collectResultsExt(
            self,
            directory,
            project,
            version,
            platform,
            hostname,
            cpu_info,
            memoryinfo,
            startTime,
            endTime,
            options,
    ):
        try:
            slot, build_id = version.split(".")
            build_id = int(build_id)
        except:
            raise RuntimeError("Handler is only supported for nightly builds")

        # grab the correct files to get the throughput
        log_files = [
            os.path.join(directory, f) for f in os.listdir(directory)
            if f.endswith(".log")
        ]

        throughput = sum(
            get_throughput(f) for f in log_files if "ThroughputTest" in f)
        str_tput = "{:.1f}".format(throughput)
        self.saveFloat(
            "max_throughput",
            throughput,
            description="maximum throughput",
            group="throughput",
        )

        dirname = (
            f"Throughput_{version}_{options}_{platform}_{startTime.replace(' ', '_')}"
        )
        targetRootWebDir = os.path.join(WWW_BASE_URL, dirname)

        # concatenate log files into one file
        with open("tests.log", "w") as outfile:
            for fname in log_files:
                outfile.write("\n{sep}\n{fname}\n{sep}\n\n".format(
                    sep="=" * 80, fname=fname))
                with open(fname) as infile:
                    for line in infile:
                        outfile.write(line)

        trend_url = os.path.join(WWW_BASE_URL,
                                 f"trend_throughput_{options}_{slot}.png")
        request = requests.get(trend_url)
        if request.status_code != 200:
            trend_url = None

        with open("index.html", "w") as html_file:
            html = REPORT_TEMPLATE.render(
                version=version,
                platform=platform,
                hostname=hostname,
                cpu_info=cpu_info,
                options=options,
                throughput=str_tput,
                WWW_BASE_URL=WWW_BASE_URL,
                dirname=dirname,
                trend_url=trend_url,
            )
            html_file.write(html)
            log.debug("Generated HTML report:\n" + html)

        for filename in [
                os.path.join(directory, "flamy.svg"),
                os.path.join(directory, "FlameBars.pdf"),
                os.path.join(directory, "FlameBars.png"),
                "index.html",
                "tests.log",
        ]:
            publish.upload_eos_www(
                filename,
                os.path.join(dirname, os.path.basename(filename)),
            )

        self.saveString(
            "algousage",
            os.path.join(targetRootWebDir, "flamy.svg"),
            description="link to algo usage plot",
            group="performance",
        )

        # send notification on mattermost channel
        cpu_model = cpu_info.split(" @")[0].replace("(R)", "").replace(
            " ", "-")
        mattermost_message = (
            "The results of latest throughput test "
            f"[{options} {version} {platform} {cpu_model}]({targetRootWebDir}):\n"
            f"`Throughput = {str_tput} Events/s`")
        publish.post_mattermost(mattermost_message)
        # let's post a reply to gitlab about the throughput test result
        # The feedback needs to compare the results from the reference
        # and the -mr builds. We don't know which completes first,
        # so we must try both cases.
        # For a better treatment in the future, see LBCORE-1984
        for ref, test, trigger in dashboard.get_ci_test_pairs(slot, build_id):
            try:
                if test == (slot, build_id):
                    # The handler runs for the -mr build, so fetch the -ref results
                    new_throughput = throughput
                    web_link = targetRootWebDir
                    ref_throughput, ref_web_link = get_couchdb_throughput_link(
                        ref[0], ref[1], options)
                elif ref == (slot, build_id):
                    # The handler runs for the -ref build, so fetch the -mr results
                    ref_throughput = throughput
                    ref_web_link = targetRootWebDir
                    new_throughput, web_link = get_couchdb_throughput_link(
                        test[0], test[1], options)
                else:
                    assert False
            except dashboard.ResourceNotFound:
                # The job for the other build hasn't finished yet => do nothing.
                # The message will be posted from the other job's handler.
                log.warning(
                    "Could not fetch results for other slot, not posting reply."
                )
            else:
                send_gitlab_feedback(
                    new_throughput,
                    ref_throughput,
                    options,
                    web_link,
                    ref_web_link,
                    trigger,
                )
