import os, sys, re
from .BaseHandler import BaseHandler
""" PrCheckerHandler will parse the PrChecker2 output of a run.log file."""

ReRecoHead = r"PrChecker2(.+?)\*{4}\s(.+?)\s{2,}(\d+)\stracks.+?(\d+)"
ReRecoLine = r"PrChecker2.+?(\d{2}.+?)\s.+?(\d+).+?(\d+)\s\[.+?\]\s+(\d+).+?purity:\s*(\d+.\d+).+?hitEff:\s*(\d+.\d+)"

ReTTHead = r"PrChecker2(.+?)\*{4}\s(.+?)\s\*{4}\s*(\d+)\sghost.+?(\d+.\d+)"
ReTTLine = r"PrChecker2.+?(\d{2}.+?)[:].+?(\d+).+?(\d+.\d+).+?(\d+.\d+).+?\[.+?\].+?(\d+.\d+)\sghost"


class PrCheckerHandler(BaseHandler):
    def __init__(self, directory=""):
        super(self.__class__, self).__init__()
        self.directory = directory
        if not self.directory:
            self.directory = os.path.realpath(os.curdir)

    def collectResults(self, directory):
        print(("reading", directory + os.sep + "run.log"))
        file = open(directory + os.sep + "run.log", "r")

        # handle the cases with two header lines
        resetPrefix = True

        for i in file.readlines():
            isTT = False
            # Match the header of a reconstruction algo
            res = re.match(ReRecoHead, i)

            # didn't find anythin? is it a TT Efficiency header?
            if not res:
                res = re.match(ReTTHead, i)
                if res:
                    isTT = True

            # Have we found a header? If so let's save the numbers we want and save the category as prefix
            # Adding the Fast prefix to the forward to differentiate between the ForwardFast fit and the Forward tracking algo
            if res:
                groups = res.groups()
                if resetPrefix:
                    prefix = (groups[1] + "Fast" if
                              (groups[1] == "Forward"
                               and groups[0].find("Fast") != -1) else
                              groups[1])
                    resetPrefix = False
                else:
                    prefixtmp = prefix
                    prefix = prefix + " " + groups[1]
                    resetPrefix = True

                if not isTT:
                    self.saveInt(
                        prefix + "_tracks",
                        int(groups[2]),
                        description="",
                        group=prefix)
                    self.saveInt(
                        prefix + "_ghosts",
                        int(groups[3]),
                        description="",
                        group=prefix)
                else:
                    self.saveInt(
                        prefix + "_ghosts",
                        int(groups[2]),
                        description="",
                        group=prefix)
                    self.saveFloat(
                        prefix + "_TT/tr",
                        float(groups[3]),
                        description="",
                        group=prefix,
                    )

                if resetPrefix:
                    prefix = prefixtmp
                continue

            if not res:
                res = re.match(ReRecoLine, i)

            if not res:
                isTT = True
                res = re.match(ReTTLine, i)

            if res:
                resetPrefix = True
                groups = res.groups()
                name = prefix + "_" + groups[0]

                if not isTT:
                    self.saveInt(
                        name + "_reconstructed",
                        int(groups[1]),
                        description="",
                        group=prefix,
                    )
                    self.saveInt(
                        name + "_reconstrucible",
                        int(groups[2]),
                        description="",
                        group=prefix,
                    )
                    self.saveInt(
                        name + "_clones",
                        int(groups[3]),
                        description="",
                        group=prefix)
                    self.saveFloat(
                        name + "_purity",
                        float(groups[4]),
                        description="",
                        group=prefix)
                    self.saveFloat(
                        name + "_hitEff",
                        float(groups[5]),
                        description="",
                        group=prefix)
                else:
                    self.saveInt(
                        name + "_tr",
                        int(groups[1]),
                        description="",
                        group=prefix)
                    self.saveFloat(
                        name + "_reconstructed",
                        float(groups[2]),
                        description="",
                        group=prefix,
                    )
                    self.saveFloat(
                        name + "_reconstrucible",
                        float(groups[3]),
                        description="",
                        group=prefix,
                    )
                    self.saveFloat(
                        name + "_ghost_hits_real_track",
                        float(groups[4]),
                        description="",
                        group=prefix,
                    )

        def run(self):
            self.collectResults(self.directory)


if __name__ == "__main__":
    PrCheckerHandler().run()
