import logging
import os
import re

from .BaseHandler import BaseHandler
from .utils import dashboard
from .utils import publish

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import json
from math import isclose

plt.ioff()

log = logging.getLogger(__name__)

WWW_BASE_URL = "https://cern.ch/lhcbpr-hlt/UpgradeRateTest"
NIGHTLIES_PERIODIC_TESTS = "https://lhcb-nightlies.web.cern.ch/periodic/"
KNOWN_PROCESSES = ['hlt1', 'hlt2', 'spruce']
"""
    Writes results to: /eos/lhcb/storage/lhcbpr/www/UpgradeRateTest

    For local testing:
        - Follow the README's guidance on setting environments
        - set env variable 'LHCBPR_WWW_EOS' to '/eos/lhcb/storage/lhcbpr/www'
        - Update test_BandwidthHandler.py kwargs as detailed below
        - ./run python -m pytest --log-cli-level=DEBUG tests/test_BandwidthHandler.py

    test_BandwidthHandler.py kwargs suggestions:
        "results": 
            A path to a writable directory containing output from a BW Test (either one locally generated or a copy from one already on eos)
            Must have the correct structure, i.e.:
            /path/to/directory:
              - tmp
                - Output
                - to_eos
                - <Optional>MDF, not actually required for the handler so can be skipped to save space.
        "app-version": 
            Nightly build designation, e.g. `lhcb-master.2574`, is used to determine reference for comparisons.
            For a local job it's important to get the build type (lhcb-master/ lhcb-master-mr/ etc.) correct due to differences in handling.
        "opt": 
            Which BW Test has been run, e.g. "Moore_hlt2_and_spruce_bandwidth", used to determine selection stages to handle.
"""


def process_prmon_output(prmon_file, prmon_json_file, job_type="Hlt2"):
    """copied and modified from prmon plotting script
    https://github.com/HSF/prmon/blob/main/package/scripts/prmon_plot.py
    """

    # Load the data
    data = pd.read_csv(prmon_file, sep="\t")
    data["Time"] = pd.to_datetime(data["Time"], unit="s")
    make_prmon_plots(
        data,
        xvar="wtime",
        ylist=["vmem", "pss", "rss", "swap"],
        job_type=job_type)

    with open(prmon_json_file) as f:
        prmon_summary = json.load(f)
    max_rss = prmon_summary["Max"]["rss"] / 1024.0 / 1024.0  # In GB
    max_pss = prmon_summary["Max"]["pss"] / 1024.0 / 1024.0  # In GB

    return max_rss, max_pss


def make_prmon_plots(data, xvar, ylist, job_type):
    # Labels and output information
    legend_names = {
        "wtime": "Wall-time",
        "vmem": "Virtual Memory",
        "pss": "Proportional Set Size",
        "rss": "Resident Set Size",
        "swap": "Swap Size",
    }
    xlabel = "Wall-time"
    ylabel = "Memory"
    xunit = "s"
    yunit = "GB"
    xmultiplier = 1.0
    ymultiplier = 1.0 / 1024.0 / 1024.0  # Raw data is in kB

    # Here comes the figure and data extraction
    fig = plt.figure()
    xdata = np.array(data[xvar]) * xmultiplier
    ydlist = []
    for carg in ylist:
        ydlist.append(np.array(data[carg]) * ymultiplier)
    for cidx, cdata in enumerate(ydlist):
        plt.plot(xdata, cdata, lw=2, label=legend_names[ylist[cidx]])
    plt.legend(loc=0)
    plt.title(
        "Plot of {} vs {} for {} test".format(ylabel, xlabel, job_type),
        y=1.05)
    plt.xlabel((xlabel + " [" + xunit + "]"))
    plt.ylabel((ylabel + " [" + yunit + "]"))
    plt.tight_layout()
    fig.savefig("memory_consumption.png")


def make_dirname(version, options, platform, start_time):
    return "_".join([
        "BandwidthTest", version, options, platform,
        start_time.replace(" ", "_")
    ])


def get_info_from_comparison_build(version, options, csv_rate_table_names,
                                   process, stream_config):
    """
    Obtain info from another build given version.
    This is presumably the reference build for comparison with the current build's results.
    Copied from ThroughputProfileHandler.
    """
    test = dashboard.get_test_doc(version, options)
    dirname = make_dirname(
        test["app_version"],
        test["opt_name"],
        test["CMTCONFIG"]["platform"],
        test["time_start"],
    )
    directory = os.path.join(
        os.getenv("LHCBPR_WWW_EOS")[23:], "UpgradeRateTest", dirname)

    dfs = {
        csv_name: pd.read_csv(os.path.join(directory, csv_name))
        for csv_name in csv_rate_table_names
    }

    web_link = test.get(
        "lhcbpr_url", f"https://Failed_to_retrieve_lhcbpr_link_for_{version}")
    with open(os.path.join(directory, "message.json"), "r") as message_file:
        comp_info = json.load(message_file)
        tot_rate = comp_info[process][stream_config]["total_rate"]
        tot_bandwidth = comp_info[process][stream_config]["total_bandwidth"]

    return (dfs, web_link, dirname, tot_rate, tot_bandwidth)


def compare_column(ref_df, new_df, compare_col):
    newdf = pd.concat([ref_df[compare_col], new_df[compare_col]], axis=1)
    newdf.columns = ["Ref", "New"]
    newdf.fillna(
        0.0, inplace=True
    )  # Cases where a row is missing e.g. new line/line removed
    newdf["Change (%)"] = (newdf["New"] - newdf["Ref"]) / newdf["Ref"] * 1e2
    newdf = newdf.reset_index()
    return newdf


def make_comparison_page(dfs_for_comparison, ref_version, new_version):
    # dfs_for_comparison = {version: {csv_name: df for each csv} for each version}
    success = True
    n_lines_added, n_lines_removed = -1, -1
    rates_have_changed = False

    def highlight_vals(val):
        if val > 0:
            return "background-color: red"
        if val < 0:
            return "background-color: green"
        else:
            return ""

    html_tables = []
    for bw_table_name, new_bw_df in dfs_for_comparison[new_version].items():
        ref_bw_df = dfs_for_comparison[ref_version][bw_table_name]
        for name, df in zip(["new", "ref"], [new_bw_df, ref_bw_df]):
            if "Stream" not in df.columns and "Line" not in df.columns:
                log.warning(
                    f'Expected "Stream" or "Line" to be one of the rate table columns in the "{name}" df. Found {df.columns}'
                )
                success = False
            continue

        is_table_per_line = ("Line" in new_bw_df.columns
                             )  # tables instead have 'Stream' if per stream
        rate_col, bw_col = "Rate (kHz)", "Total Bandwidth (GB/s)"
        columns_to_compare = (
            [rate_col, bw_col] if is_table_per_line else new_bw_df.columns[2:]
        )  # First two cols are a count and the stream name

        name_col = "Line" if is_table_per_line else "Stream"
        if name_col != new_bw_df.columns[1]:
            log.warning(
                f'Expected 1st column of df "{name_col}" != the actual 1st col "{new_bw_df.columns[1]}". Comparison will fail.'
            )
            success = False
            continue

        # Handle duplicated indices
        new_bw_df.set_index(name_col, inplace=True)
        ref_bw_df.set_index(name_col, inplace=True)
        if new_bw_df.index.duplicated().any() or ref_bw_df.index.duplicated(
        ).any():
            # Append a number starting from 0 to the duplicated indices to make them differ
            # Probably happens when there are multiple lines with the same name in different streams e.g. LumiLine
            for df in [new_bw_df, ref_bw_df]:
                df.index = df.index + '_' + df.groupby(
                    level=0).cumcount().astype(str)
                df.rename_axis(
                    name_col,
                    inplace=True)  # Index name was lost in the groupby above

        rows = list(set(new_bw_df.index).union(set(ref_bw_df.index)))
        comparison_df = pd.DataFrame({name_col: rows})
        for col_to_compare in columns_to_compare:
            # Compare each column, make a small sub-df with 3 cols (old, new, diff) for that column, then merge the 3 into comparison df
            old_new_diff_subdf = compare_column(ref_bw_df, new_bw_df,
                                                col_to_compare)
            comparison_df = pd.merge(
                comparison_df,
                old_new_diff_subdf,
                on=[name_col],
                suffixes=(f"{col_to_compare}_ref", f"{col_to_compare}_new"),
            )

        # Now arrange the columns so that e.g. the comparison column has 3 sub-columns under it: old, new and diff
        # name_col is only column that doesn't have sub-columns
        multi_columns = [(name_col, "")] + pd.MultiIndex.from_product(
            [columns_to_compare, ["Ref", "New", "Change (%)"]]).to_list()
        comparison_df.columns = pd.MultiIndex.from_tuples(multi_columns)
        if name_col == "Line":
            # Table will be too large; show only those that have changed
            bw_chg_thres = 10.0  # There are always %-level fluctuations in raw bank sizes - try to avoid those.
            comparison_df = comparison_df[
                (comparison_df[rate_col]["Ref"] != comparison_df[rate_col]
                 ["New"]) |
                (np.abs(comparison_df[bw_col]["Change (%)"]) > bw_chg_thres)]

        # Apply highlighting and formatting
        styler = comparison_df.style
        for column in columns_to_compare:
            styler = styler.applymap(
                highlight_vals, subset=[(column, "Change (%)")])
            styler = styler.format("{:.2f}", subset=[(column, "Change (%)")])
            styler = styler.format(
                "{:.3g}", subset=[(column, "New"), (column, "Ref")])

        # Work out what to call the table
        if is_table_per_line:
            table_name_for_html = "rates per line"
        else:
            table_name_for_html = "rates per stream"

        # Finally - how many new lines and have any rates changed?
        if is_table_per_line:
            n_lines_added = 0
            for line in new_bw_df.index:
                if line not in ref_bw_df.index:
                    n_lines_added += 1

            n_lines_removed = 0
            for line in ref_bw_df.index:
                if line not in new_bw_df.index:
                    n_lines_removed += 1

            rates_have_changed = bool(len(comparison_df))

        html_tables.append((table_name_for_html,
                            styler.set_table_attributes("border=1").to_html()))

    if len(html_tables) > 2 or len(html_tables) == 0:
        success = False
        log.warning(
            f'Expected to find a per-line and a per-stream table. Found {len(html_tables)} tables to compare.'
        )

    # Put together the html page
    html_str = f"""
        <p>
            Comparison between {ref_version} and {new_version} under different streaming configurations. <br>
            <b>NB:</b> Due to the nature of the ci-testing framework, this comparison includes <b>all</b> changes to the branch being tested between the {ref_version} and when the ci-test for {new_version} was triggered.
            Therefore the changes reported here can, in general, show more than what is within the MR that triggered {new_version}. <br>
            <b>NB:</b> Raw bank sizes (and thus bandwidths) exhibit per-cent-level random fluctuations. To keep this comparison page short and relevant, changes in bandwidths <b>per line</b> are only shown if > {bw_chg_thres}%.
            Any change in line rate will be shown. The comparison of all streams should always be present below.
        </p>
        <p style="color:{'green' if success else 'red'}">
            <b>{'All comparison tables were made successfully' if success else 'The comparison page failed to build cleanly. Please see warnings in logfile.'}</b>
        </p>
    """
    for table_descr, html_table in html_tables:
        html_str += f"""
            <p>
                Changes in {table_descr}:
            </p>
        """
        html_str += html_table

    return html_str, (n_lines_added, n_lines_removed), rates_have_changed


def rate_tolerance(process):
    return {"hlt1": 1e6, "hlt2": 1000, "spruce": 500}[process]  # Hz


def failure_msg(options, version, web_link, specific_failure, subtest_str):
    message = (
        f"Bandwidth test {subtest_str}[{options} {version}]({web_link}): "
        f":warning: Error in test: {specific_failure}"
        " Please see the pages linked above and the Jenkins logs for more information."
        f" (Jenkins logs: find the test in the [nightlies pages]({NIGHTLIES_PERIODIC_TESTS}) and follow links to the Jenkins console output)."
    )
    return message


def send_failure_msg_to_gitlab(options,
                               version,
                               web_link,
                               msg_trigger,
                               specific_failure,
                               subtest_str=""):
    message = failure_msg(options, version, web_link, specific_failure,
                          subtest_str)
    publish.post_gitlab_feedback(msg_trigger, message)


def send_comparison_failure_to_gitlab(process, stream_config, options, version,
                                      web_link, msg_trigger):
    specific = (
        "comparison with ref build failed. The rest of the test is probably OK, "
        "check the pages via the link. More info in log")
    send_failure_msg_to_gitlab(
        options,
        version,
        web_link,
        msg_trigger,
        specific_failure=specific,
        subtest_str=f"{process.upper()} ({stream_config}) ")


def send_failure_msg_to_mattermost(options,
                                   version,
                                   web_link,
                                   specific_failure=""):
    message = failure_msg(
        options, version, web_link, specific_failure, subtest_str="").replace(
            ":warning:", ":alarm:")  # `warning` not supported on Mattermost
    log.info(f"Posting Mattermost feedback:\n message={message}")
    publish.post_mattermost(message)


def _err_code_to_summary_msg(err_code: int, fatals_in_log: bool, process: str,
                             for_mattermost: bool) -> str:
    """Parse error code and thus format summary message to be printed 
    in Gitlab or on Mattermost - they have different emojis."""
    GOOD_TEST_MSG = ":bluetick: All sub-jobs in this test exited successfully.\n"
    BAD_TEST_MSG = ":alarm: **There were errors in some of the sub-jobs of this test; please see the logs.** :alarm:\n"

    if fatals_in_log:
        err_code = 99  # Arbitrary non-zero

    if err_code == 0:
        status_msg = GOOD_TEST_MSG
    elif err_code == 1 and process == "hlt2":
        # Temporarily allow this status code due to "Misodered large cluster" errors in data we can't get rid of
        status_msg = GOOD_TEST_MSG
    else:
        status_msg = BAD_TEST_MSG

    if for_mattermost:
        return status_msg
    else:
        return status_msg.replace(":bluetick:",
                                  ":ballot_box_with_check:").replace(
                                      ":alarm:", ":warning:")


def send_gitlab_feedback(n_line_changes, rates_have_changed, tot_rate_new,
                         tot_rate_ref, tot_bandwidth_new, tot_bandwidth_ref,
                         options, web_link_new, web_link_ref, trigger, process,
                         stream_config, single_test_info, fatals_in_log):
    """
    Post info to GitLab MR for a single test i.e. one process
    and one stream configuration.

    Arguments:
        n_line_changes: tuple: (number of new lines added in this MR,
                                number of lines removed in this MR)
        rates_have_changed: bool, True if any line rates have changed
        tot_rate_new: total rate from MR's branch
        tot_rate_ref: total rate from reference build
        tot_bandwidth_new: total bandwidth from MR's branch
        tot_bandwidth_ref: total bandwidth from reference build
        options: the name of this test
        web_link_new: web link of test build
        web_link_ref: web link of reference build
        trigger: note in MR that triggers this test
        process: either 'hlt1', 'hlt2' or 'spruce'
        stream_config: key for streaming configuration e.g. 'production'
        single_test_info: dict of info from bandwidth test for only this process and stream_config
        fatals_in_log: bool, if True overwrite status code to bad.
    """

    def safe_comp_str(new, ref, unit):
        diff = new - ref
        comp_str = f"{new:.2f} {unit} - change of {diff:.2f} {unit} ({{percentage}})"
        if isclose(ref, 0.0):
            return comp_str.format(percentage="N/A %")
        else:
            return comp_str.format(percentage=f"{(diff/ref):.2%}")

    rate_comp_str = safe_comp_str(tot_rate_new, tot_rate_ref, "kHz")
    bw_comp_str = safe_comp_str(tot_bandwidth_new, tot_bandwidth_ref, "GB/s")
    status_msg = _err_code_to_summary_msg(
        single_test_info["code"], fatals_in_log, process, for_mattermost=False)

    message = (
        f"Bandwidth test : {process.upper()} ({stream_config}) : [{options}]({web_link_new}): "
        f"Total rate = {rate_comp_str} "
        f"vs. [reference]({web_link_ref}). "
        f"Total bandwidth = {bw_comp_str} "
        f"vs. [reference]({web_link_ref}). "
        f"New lines added: {n_line_changes[0]}. Lines removed: {n_line_changes[1]}. "
        f"Lines with rate of 0 Hz: {single_test_info['n_low_rate']}. "
        f"Lines with rate > {rate_tolerance(process)} Hz: {single_test_info['n_high_rate']}. "
    )
    if rates_have_changed:
        message += "**Line rates have changed: please check comparison in webpages to verify.**\n"
    message += status_msg

    publish.post_gitlab_feedback(trigger, message)


def send_mattermost_feedback(options, version, web_link, process,
                             stream_config, single_test_info, fatals_in_log):
    """
    Post info to Mattermost LHCbPR throughout channel for a single test
    i.e. one process and one stream configuration.

    Arguments:
        options: the name of this test
        version: the CI/nightly test slot.build_id of test
        web_link: web link of test build
        process: either `hlt1`, `hlt2` or `spruce`
        single_test_info: a dictionary containing at least the following variables (for this process and stream_config):
        exit code: whether all sub-jobs were successful
        n_low_rate: number of lines which have a rate of 0 Hz
        n_high_rate: number of lines with rate above tolerance
        tot_rate: total rate from test
        tot_bandwidth: total bandwidth from test
        fatals_in_log: bool, if True overwrite status code to bad.
    """
    pfx = f"{process.upper()} ({stream_config})"
    test_msg = _err_code_to_summary_msg(
        single_test_info["code"], fatals_in_log, process, for_mattermost=True)
    message = (
        "The results of latest bandwidth test "
        f"[{options} {version}]({web_link}):\n"
        f"`{pfx} Total rate = {single_test_info['total_rate']:.1f} kHz`. `{pfx} Total bandwidth = {single_test_info['total_bandwidth']:.1f} GB/s`.\n"
        f"{pfx} Lines with rate of 0 Hz: {single_test_info['n_low_rate']}. "
        f"{pfx} Lines with rate > {rate_tolerance(process)} Hz: {single_test_info['n_high_rate']}.\n"
    ) + test_msg
    log.info(f"Posting Mattermost feedback:\n message={message}")
    publish.post_mattermost(message)


def copy_hlt2_outputs_to_eos(directory):
    from datetime import datetime
    to_eos_path = os.path.join(directory, 'tmp', 'to_eos')
    files_to_copy = [
        os.path.join(to_eos_path, f) for f in os.listdir(to_eos_path)
    ]

    before_copy = datetime.now()
    for f in files_to_copy:
        publish.upload_eos_www(
            f,
            os.path.join("current_hlt2_output", os.path.basename(f)),
            baseurl=os.path.join(
                os.getenv("LHCBPR_WWW_EOS"), "UpgradeRateTest"),
            force=True)
    log.info("Copied Moore_hlt2_bandwidth outputs to eos. This took " +
             str(datetime.now() - before_copy))
    return


class BandwidthTestHandler(BaseHandler):
    def __init__(self):
        super().__init__()

    def collectResultsExt(
            self,
            directory,
            project,
            version,
            platform,
            hostname,
            cpu_info,
            memoryinfo,
            startTime,
            endTime,
            options,
    ):
        try:
            slot, build_id = version.split(".")
            build_id = int(build_id)
        except:
            raise RuntimeError("Handler is only supported for nightly builds")

        job_type = '_'.join(options.replace('_bandwidth', '').split('_')[1:])

        dirname = make_dirname(version, options, platform, startTime)
        targetRootWebDir = os.path.join(WWW_BASE_URL, dirname)

        def version_str(slot_and_build_id):
            return ".".join([str(s) for s in slot_and_build_id])

        base_dir = os.path.join(directory, "tmp/Output")

        def full_output_path(end_path):
            return os.path.join(base_dir, end_path)

        def list_outputs():
            return os.listdir(base_dir)

        # process `prmon` output to monitor memory consumption
        prmon_file = os.path.join(directory, "prmon.txt")
        prmon_json_file = os.path.join(directory, "prmon.json")
        if os.path.exists(prmon_file) and os.path.exists(prmon_json_file):
            monitor_memory = True
            max_rss, max_pss = process_prmon_output(
                prmon_file, prmon_json_file, job_type=job_type)
            max_rss = f"{max_rss:.2f}"
            max_pss = f"{max_pss:.2f}"
        else:
            monitor_memory = False
            log.warning(
                "No prmon output files found. Seems this test was not wrapped with `prmon` yet."
            )
            max_rss = "N/A"
            max_pss = "N/A"

        with open(full_output_path("message.json"), "r") as message_file:
            test_info_all = json.load(message_file)

        all_stream_configs = {
            k: list(v.keys())
            for k, v in test_info_all.items() if k in KNOWN_PROCESSES
        }
        try:
            html_page_built_correctly = (
                test_info_all["make_html_page"]["code"] == 0)
        except KeyError:
            log.warning(
                "\"make_html_page\" error code key not found in message.json. Something wrong."
            )
            html_page_built_correctly = False

        edited_files = []

        if slot.endswith("-mr"):
            compare_str = f'<li><a href="{WWW_BASE_URL}/{dirname}/{{process}}__{{stream_config}}__comparisons.html"> Comparisons with reference build</a></li>'
        else:
            compare_str = ""

        def _replace(fname, process, stream_config, edited_files):

            replacements = {
                f"{process}__{stream_config}__comparison":
                compare_str.format(
                    process=process, stream_config=stream_config),
                "dirname":
                dirname,
                "start_time":
                startTime,
                "end_time":
                endTime,
                "version":
                version,
                "platform":
                platform,
                "hostname":
                hostname,
                "cpu_info":
                cpu_info,
                "max_rss":
                max_rss,
                "max_pss":
                max_pss,
            }
            with open(fname, "w") as new_html_file:
                with open(full_output_path(fname), "r") as old_html_file:
                    # Will fail if fname not there, as it should
                    content = old_html_file.read()
                    for old, new in replacements.items():
                        content = content.replace(f"$${old}$$", new)
                    new_html_file.write(content)
            edited_files.append(fname)

        main_page = "index.html"
        try:
            _replace(
                fname=main_page,
                process="",
                stream_config="",
                edited_files=edited_files)
            for process, stream_configs in all_stream_configs.items():
                for stream_config in stream_configs:
                    _replace(
                        fname=f"{process}__{stream_config}__{main_page}",
                        process=process,
                        stream_config=stream_config,
                        edited_files=edited_files)
                    _replace(
                        fname=f"{process}__{stream_config}__all_rates.html",
                        process=process,
                        stream_config=stream_config,
                        edited_files=edited_files)
        except FileNotFoundError:
            log.exception(
                "Failed to find one of the key bandwidth test pages - must've been failures. See log."
            )
            fail_msg = (
                "test page failed to build at all - likely an error in running Moore. "
                "Please look for ERROR/FATAL messages and/or exceptions near the top of the log"
            )
            send_failure_msg_to_mattermost(
                options, version, targetRootWebDir, specific_failure=fail_msg)
            for _, test, trigger in dashboard.get_ci_test_pairs(
                    slot, build_id):
                if test == (slot, build_id):
                    send_failure_msg_to_gitlab(
                        options,
                        version,
                        targetRootWebDir,
                        trigger,
                        specific_failure=fail_msg)
                else:
                    log.info(
                        "Not sending failure msg for ref build - will get failure msg for test build"
                    )
            return

        # TODO: re-evaluate the uploading of .root files once "what we do with the monitoring histograms in the BW Tests" is more fleshed out
        extensions = [".html", ".csv", ".json", ".txt", ".png", ".root"]
        unedited_files = []
        for ext in extensions:
            unedited_files += [
                full_output_path(f) for f in list_outputs() if f.endswith(ext)
            ]

        log_files = [
            os.path.join(directory, f) for f in os.listdir(directory)
            if f.endswith(".log")
        ]
        errors_maybe_missed = [
            "segmentation violation", "Traceback (most recent call last)",
            "FATAL"
        ]
        fatals_in_log = False
        for fl in log_files:
            with open(fl, "r") as f:
                txt = f.readlines()
                fatals_in_log |= any([
                    err in line for err in errors_maybe_missed for line in txt
                ])

        unedited_files += log_files
        # replace old html(s) (picked up by list_outputs()) with new, edited version
        files_to_upload = []
        for fl in unedited_files:
            if all([edited_file not in fl for edited_file in edited_files]):
                files_to_upload.append(fl)
        files_to_upload += edited_files
        for f in files_to_upload:
            log.info(f"Found file {f} to upload.")

        def upload_file(diro, fname):
            publish.upload_eos_www(
                fname,
                os.path.join(diro, os.path.basename(fname)),
                baseurl=os.path.join(
                    os.getenv("LHCBPR_WWW_EOS"), "UpgradeRateTest"),
            )

        for file in files_to_upload:
            upload_file(dirname, file)

        if monitor_memory:
            for mon_file in [
                    prmon_file, prmon_json_file, "memory_consumption.png"
            ]:
                upload_file(dirname, mon_file)

        def gitlab_msg_only_for_test_build(msg):
            for _, test, trigger in dashboard.get_ci_test_pairs(
                    slot, build_id):
                if test == (slot, build_id):
                    send_failure_msg_to_gitlab(
                        options,
                        version,
                        targetRootWebDir,
                        trigger,
                        specific_failure=msg)
                else:
                    log.info(
                        "Not sending failure msg for ref build - will get failure msg for test build"
                    )
            return

        try:
            for process, stream_configs in all_stream_configs.items():
                for stream_config in stream_configs:
                    this_test_info = test_info_all[process][stream_config]
                    for metric_name in ["total_rate", "total_bandwidth"]:
                        self.saveFloat(
                            f"{process}__{stream_config}__{metric_name}",
                            this_test_info[metric_name],
                            description=
                            f"{metric_name} of {process} {stream_config} lines",
                            group=f'{process}__{stream_config}__{metric_name}',
                        )
                    send_mattermost_feedback(
                        options=options,
                        version=version,
                        web_link=targetRootWebDir,
                        process=process,
                        stream_config=stream_config,
                        single_test_info=this_test_info,
                        fatals_in_log=fatals_in_log)
        except KeyError:
            log.exception(
                f"Failed to save test results for {process} {stream_config}.")
            fail_msg = (
                "found no measured bandwidths - likely an error in running Moore. "
                "Please look for ERROR/FATAL messages and/or exceptions near the top of the log."
            )
            send_failure_msg_to_mattermost(
                options, version, targetRootWebDir, specific_failure=fail_msg)

            gitlab_msg_only_for_test_build(fail_msg)
            return

        # Send error messages if the HTML pages didnt build correctly
        # Dont return though - the rest might work OK.
        if not html_page_built_correctly:
            html_fail_msg = (
                "errors in the building of the html pages. Check the pages "
                "and check the logs.")
            send_failure_msg_to_mattermost(
                options,
                version,
                targetRootWebDir,
                specific_failure=html_fail_msg)
            gitlab_msg_only_for_test_build(html_fail_msg)

        if options == "Moore_hlt2_bandwidth" and slot == "lhcb-master":
            hlt2_test_info = test_info_all['hlt2']['production']
            if hlt2_test_info['code'] == 0:
                log.info("Copying hlt2 outputs to eos.")
                copy_hlt2_outputs_to_eos(directory)
            elif hlt2_test_info['code'] == 1:
                log.info(
                    "Error code 1 on the HLT2 test - we temporarily allow this code. Copying hlt2 outputs to eos."
                )
                copy_hlt2_outputs_to_eos(directory)
            else:
                log.info("Not copying test outputs to eos as test failed.")

        # Post GitLab MR feedback; a bit more complex as comparing to a reference build.
        def report_comparison(process, stream_config, ref, test, trigger):
            csv_rate_table_names = [
                f for f in list_outputs()
                if (f.endswith(".csv")
                    and f"{process}__{stream_config}__rates" in f)
            ]
            this_test_info = test_info_all[process][stream_config]

            ref_version = version_str(ref)
            new_version = version_str(test)
            dfs_for_comparison = {ref_version: {}, new_version: {}}
            # Instance below as 0 to prevent picking up values from last loop iteration
            # (can happen if we fail to find the reference)
            tot_rate_new, tot_rate_ref = 0.0, 0.0
            tot_bandwidth_new, tot_bandwidth_ref = 0.0, 0.0

            comparison_fail_msg = "Error getting the data from comparison build/preparing to build comparison page."
            if test == (slot, build_id):
                # The current build is the *test*, not the reference build
                web_link_new = targetRootWebDir
                dirname_new = dirname
                tot_rate_new = this_test_info['total_rate']
                tot_bandwidth_new = this_test_info['total_bandwidth']
                csv_table_version = new_version

                # The build we're comparing to is therefore the reference build
                try:
                    (
                        dfs_for_comparison[ref_version],
                        web_link_ref,
                        _,
                        tot_rate_ref,
                        tot_bandwidth_ref,
                    ) = get_info_from_comparison_build(ref_version, options,
                                                       csv_rate_table_names,
                                                       process, stream_config)
                except:
                    log.exception(comparison_fail_msg)

            elif ref == (slot, build_id):
                # The current build is the *reference*, not the test build
                # This is an edge-case, See: https://gitlab.cern.ch/lhcb-datapkg/PRConfig/-/issues/32#note_8418534
                web_link_ref = targetRootWebDir
                tot_rate_ref = this_test_info['total_rate']
                tot_bandwidth_ref = this_test_info['total_bandwidth']
                csv_table_version = ref_version

                # The build we're comparing to is therefore the test build
                try:
                    (
                        dfs_for_comparison[new_version],
                        web_link_new,
                        dirname_new,
                        tot_rate_new,
                        tot_bandwidth_new,
                    ) = get_info_from_comparison_build(new_version, options,
                                                       csv_rate_table_names,
                                                       process, stream_config)
                except:
                    log.exception(comparison_fail_msg)

            else:
                raise RuntimeError(
                    f"{slot} {build_id} does not correspond to a ref or test build."
                )

            for csv_name in csv_rate_table_names:
                log.info(f"Trying comparison with {csv_name}.")
                dfs_for_comparison[csv_table_version][csv_name] = pd.read_csv(
                    full_output_path(csv_name))

            # Exceptions caught above will likely cause exceptions below
            # Only want the failure message to be sent once.
            try:
                html_str, n_line_changes, rates_have_changed = make_comparison_page(
                    dfs_for_comparison, ref_version, new_version)
                page_name = f"{process}__{stream_config}__comparisons.html"
                with open(page_name, "w") as html_file:
                    html_file.write(html_str)
                # always upload comparison table to test dirs
                upload_file(dirname_new, page_name)
                if test == (slot, build_id):
                    send_gitlab_feedback(
                        n_line_changes, rates_have_changed, tot_rate_new,
                        tot_rate_ref, tot_bandwidth_new, tot_bandwidth_ref,
                        options, web_link_new, web_link_ref, trigger, process,
                        stream_config, this_test_info, fatals_in_log)
                else:
                    log.info(
                        "Not sending msg for ref build - msg only required on testing MR."
                    )
            except:
                log.exception("Failed to make comparison page.")
                if test == (slot, build_id):
                    send_comparison_failure_to_gitlab(
                        process, stream_config, options, version,
                        targetRootWebDir, trigger)
                else:
                    log.info(
                        "Not sending failure message for ref build - comparison likely failed because the test builds haven't occurred yet."
                    )
            return

        for process, stream_configs in all_stream_configs.items():
            for stream_config in stream_configs:
                for ref, test, trigger in dashboard.get_ci_test_pairs(
                        slot, build_id):
                    report_comparison(process, stream_config, ref, test,
                                      trigger)
