#########################################################################
## Handler for Muon tests in Gauss, takes a root file containing       ##
## muon test data and saves as JSON. Muon test data will be            ##
## located in the MuonTestResults folder after running runmuontest.py  ##
## MUST be run in the same directory as runmuontest.py is              ##
## @Author: R.Calladine                                                ##
## @date:   Last modified 2017-03-24                                   ##
#########################################################################

import json, os
from ROOT import TFile, TH1D, TTree, gDirectory

from .BaseHandler import BaseHandler


def labelHist(hist, xtitle, ytitle, phys_list):
    hist.SetXTitle(xtitle)
    hist.GetXaxis().CenterTitle()
    hist.SetYTitle(ytitle)
    hist.GetYaxis().CenterTitle()


def getHist(in_file, directory, out_file, phys_list):
    inroot = in_file

    inpath = os.path.join(directory, inroot)

    print("Opening rootfile" + inpath)

    infile = TFile.Open(inpath)

    hist_MuSct = []
    for i in range(1, 5):
        labelHist(
            infile.Get(
                "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/dxdTx_MF{}"
                .format(i)),
            "Angular Deviation [mRad]",
            "Displacement [mm]",
            phys_list,
        )
        hist_MuSct.append(
            infile.Get(
                "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/dxdTx_MF{}"
                .format(i)))

        labelHist(
            infile.Get(
                "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/dydTy_MF{}"
                .format(i)),
            "Angular Deviation [mRad]",
            "Displacement [mm]",
            phys_list,
        )
        hist_MuSct.append(
            infile.Get(
                "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/dydTy_MF{}"
                .format(i)))

        labelHist(
            infile.Get(
                "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/pdx_MF{}"
                .format(i)),
            "Displacement [mm]",
            "Particle Momentum [GeV/c]",
            phys_list,
        )
        hist_MuSct.append(
            infile.Get(
                "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/pdx_MF{}"
                .format(i)))

        labelHist(
            infile.Get(
                "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/pdy_MF{}"
                .format(i)),
            "Displacement [mm]",
            "Particle Momentum [GeV/c]",
            phys_list,
        )
        hist_MuSct.append(
            infile.Get(
                "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/pdy_MF{}"
                .format(i)))

        labelHist(
            infile.Get(
                "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/pdTx_MF{}"
                .format(i)),
            "Angular Deviation [mRad]",
            "Particle Momentum [GeV/c]",
            phys_list,
        )
        hist_MuSct.append(
            infile.Get(
                "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/pdTx_MF{}"
                .format(i)))

        labelHist(
            infile.Get(
                "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/pdTy_MF{}"
                .format(i)),
            "Angular Deviation [mRad]",
            "Particle Momentum [GeV/c]",
            phys_list,
        )
        hist_MuSct.append(
            infile.Get(
                "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/pdTy_MF{}"
                .format(i)))

        labelHist(
            infile.Get(
                "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/dxdTx_MF{}_prof"
                .format(i)),
            "Angular Deviation [mRad]",
            "Displacement [mm]",
            phys_list,
        )
        hist_MuSct.append(
            infile.Get(
                "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/dxdTx_MF{}_prof"
                .format(i)))

        labelHist(
            infile.Get(
                "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/dydTy_MF{}_prof"
                .format(i)),
            "Angular Deviation [mRad]",
            "Displacement [mm]",
            phys_list,
        )
        hist_MuSct.append(
            infile.Get(
                "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/dydTy_MF{}_prof"
                .format(i)))

        labelHist(
            infile.Get(
                "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/pdx_MF{}_prof"
                .format(i)),
            "Displacement [mm]",
            "Particle Momentum [GeV/c]",
            phys_list,
        )
        hist_MuSct.append(
            infile.Get(
                "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/pdx_MF{}_prof"
                .format(i)))

        labelHist(
            infile.Get(
                "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/pdy_MF{}_prof"
                .format(i)),
            "Displacement [mm]",
            "Particle Momentum [GeV/c]",
            phys_list,
        )
        hist_MuSct.append(
            infile.Get(
                "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/pdy_MF{}_prof"
                .format(i)))

        labelHist(
            infile.Get(
                "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/pdTx_MF{}_prof"
                .format(i)),
            "Angular Deviation [mRad]",
            "Particle Momentum [GeV/c]",
            phys_list,
        )
        hist_MuSct.append(
            infile.Get(
                "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/pdTx_MF{}_prof"
                .format(i)))

        labelHist(
            infile.Get(
                "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/pdTy_MF{}_prof"
                .format(i)),
            "Angular Deviation [mRad]",
            "Particle Momentum [GeV/c]",
            phys_list,
        )
        hist_MuSct.append(
            infile.Get(
                "MuonMultipleScatteringChecker/MuonMultipleScatteringTest/pdTy_MF{}_prof"
                .format(i)))

    for histogram in hist_MuSct:
        old_name = histogram.GetName()
        histogram.SetName(phys_list + "_" + old_name)

    MuSct_dict = {j.GetName(): j
                  for j in hist_MuSct
                  }  # dictionary of form 'name of histogram' : 'histogram'

    out_file.mkdir(phys_list)
    out_file.cd(phys_list)

    for key in MuSct_dict:
        MuSct_dict[key].Write(key)


class MuonMoniPLHandler(BaseHandler):
    def __init__(self):
        super(self.__class__, self).__init__()

    def collectResults(self, directory):
        # Placeholder for holding histograms

        prefix = "MuonTestResults/MuonMoniSim_"
        physics_lists = ["EmOpt1", "EmNoCuts", "EmNoCutsNoLHCb"]
        postfix = ".root"

        present_lists = []

        for i in physics_lists:
            root_loc = prefix + i + postfix
            file_loc = os.path.join(directory, root_loc)

            if os.path.isfile(file_loc) == True:
                present_lists.append(i)
                print("\n Found file : " + file_loc + "\n")

        assert len(present_lists) != 0

        outfile = TFile("MuonMoniOut.root", "RECREATE")

        for G4list in present_lists:
            getHist(prefix + G4list + postfix, directory, outfile, G4list)

        # #save your file
        self.saveFile("MuonMoniSimRes", "./MuonMoniOut.root")
