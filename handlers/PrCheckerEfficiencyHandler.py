#!/usr/bin/python

# Script for accessing histograms of reconstructible and
# reconstructed tracks for different tracking categories
# created by PrChecker2
#
# The efficency is calculated usig TGraphAsymmErrors
# and Bayesian error bars
#
# author: Dorothea vom Bruch (dorothea.vom.bruch@cern.ch)
# date:   10/2018
#

import os
import ROOT
from .BaseHandler import BaseHandler
from collectRunResults import urlopen
import logging
import subprocess
from collectRunResults import send_notification_mattermost

log = logging.getLogger(__name__)


def getEfficiencyHistoNames():
    return ["eta", "p", "pt", "phi", "nPV"]


def getTrackers():
    return ["Velo", "Upstream", "Forward"]


def getOriginFolders():
    basedict = {"Velo": {}, "Upstream": {}, "Forward": {}}

    basedict["Velo"]["folder"] = "VeloMCChecker/"
    basedict["Upstream"]["folder"] = "UpMCChecker/"
    basedict["Forward"]["folder"] = "ForwardMCChecker/"

    return basedict


def getGhostHistoNames():
    return ["eta", "nPV"]


class PrCheckerEfficiencyHandler(BaseHandler):
    def __init__(self):
        super(self.__class__, self).__init__()

    def collectResultsExt(
            self,
            directory,
            project,
            version,
            platform,
            hostname,
            cpu_info,
            memoryinfo,
            startTime,
            endTime,
            options,
    ):
        f = ROOT.TFile.Open(
            os.path.join(directory, "PrCheckerPlots.root"), "read")
        outputfile = ROOT.TFile("efficiency_plots.root", "recreate")

        if "EFFICIENCY_PLOTS_PRCHECKER_REFERENCE" not in os.environ:
            raise Exception(
                "Environment variable with the reference file is not set. Check node configuration"
            )
        else:
            file_ref = urlopen(
                os.environ["EFFICIENCY_PLOTS_PRCHECKER_REFERENCE"])
            file_ = open("./efficiency_plots_ref.root", "wb")
            file_.write(file_ref.read())
            file_.close()
            f_ref = ROOT.TFile.Open(
                os.path.join("efficiency_plots_ref.root"), "read")

        if not f_ref:
            raise Exception(
                "Couldn't get the file with the reference distribution")

        from .utils.LHCbStyle import setLHCbStyle
        from .utils.ConfigHistos import (
            efficiencyHistoDict,
            ghostHistoDict,
            categoriesDict,
            getCuts,
        )
        from .utils.Legend import place_legend

        setLHCbStyle()

        latex = ROOT.TLatex()
        latex.SetNDC()
        latex.SetTextSize(0.04)

        efficiencyHistoDict = efficiencyHistoDict()
        efficiencyHistos = getEfficiencyHistoNames()
        ghostHistos = getGhostHistoNames()
        ghostHistoDict = ghostHistoDict()
        categories = categoriesDict()
        cuts = getCuts()
        trackers = getTrackers()
        folders = getOriginFolders()

        for tracker in trackers:
            outputfile.cd()
            trackerDir = outputfile.mkdir(tracker)
            trackerDir.cd()

            for cut in cuts[tracker]:
                cutDir = trackerDir.mkdir(cut)
                cutDir.cd()
                folder = folders[tracker]["folder"]
                print(folder)
                histoBaseName = "Track/" + folder + tracker + "/" + cut + "_"

                # calculate efficiency
                for histo in efficiencyHistos:
                    title = ("efficiency vs. " + histo + ", " +
                             categories[tracker][cut]["title"])
                    name = "efficiency vs. " + histo
                    canvas = ROOT.TCanvas(name, title)
                    ROOT.gPad.SetTicks()
                    # get efficiency for not electrons category
                    histoName = (histoBaseName + "notElectrons_" +
                                 efficiencyHistoDict[histo]["variable"])
                    print("not electrons: " + histoName)
                    numeratorName = histoName + "_reconstructed"
                    numerator = f.Get(numeratorName)
                    denominatorName = histoName + "_reconstructible"
                    denominator = f.Get(denominatorName)
                    if numerator.GetEntries() == 0 or denominator.GetEntries(
                    ) == 0:
                        continue

                    numerator.Sumw2()
                    denominator.Sumw2()

                    g_efficiency_notElectrons = ROOT.TGraphAsymmErrors()
                    g_efficiency_notElectrons.SetName(
                        "g_efficiency_notElectrons")
                    g_efficiency_notElectrons.Divide(numerator, denominator,
                                                     "w")
                    g_efficiency_notElectrons.SetTitle("not electrons")

                    # get efficiency for electrons category
                    if categories[tracker][cut]["plotElectrons"]:
                        histoName = (histoBaseName + "electrons_" +
                                     efficiencyHistoDict[histo]["variable"])
                        print("electrons: " + histoName)
                        numeratorName = histoName + "_reconstructed"
                        numerator = f.Get(numeratorName)
                        denominatorName = histoName + "_reconstructible"
                        denominator = f.Get(denominatorName)
                        if numerator.GetEntries(
                        ) == 0 or denominator.GetEntries() == 0:
                            continue

                        numerator.Sumw2()
                        denominator.Sumw2()

                        g_efficiency_electrons = ROOT.TGraphAsymmErrors()
                        g_efficiency_electrons.SetName(
                            "g_efficiency_electrons")
                        g_efficiency_electrons.Divide(numerator, denominator,
                                                      "w")
                        g_efficiency_electrons.SetTitle("electrons")
                        g_efficiency_electrons.SetMarkerColor(ROOT.kAzure - 3)
                        g_efficiency_electrons.SetMarkerStyle(24)
                        g_efficiency_electrons.SetLineColor(ROOT.kAzure - 3)

                    # draw them both
                    mg = ROOT.TMultiGraph()
                    mg.Add(g_efficiency_notElectrons)
                    if categories[tracker][cut]["plotElectrons"]:
                        mg.Add(g_efficiency_electrons)

                    mg.Draw("ap")
                    xtitle = efficiencyHistoDict[histo]["xTitle"]
                    mg.GetXaxis().SetTitle(xtitle)
                    mg.GetYaxis().SetTitle("efficiency")
                    mg.GetYaxis().SetRangeUser(0, 1)

                    if categories[tracker][cut]["plotElectrons"]:
                        canvas.PlaceLegend()

                    mg.SetName("efficiency vs. " + histo + " ref")
                    mg.SetTitle(tracker + " " + cut)
                    mg.Write()
                    canvas.Write()

                    mg_ref = f_ref.Get(tracker + "/" + cut + "/" + name +
                                       " ref")
                    for graph in mg_ref.GetListOfGraphs():
                        graph.SetMarkerStyle(25)
                        graph.SetMarkerSize(2)
                        graph.SetTitle(graph.GetTitle() + " ref")
                        graph.Draw("psame")

                    canvas.PlaceLegend()
                    canvas.SetName(canvas.GetName() + " with ref")
                    canvas.Write()

                    if (tracker == "Forward"
                            and cut == "Long_eta25_triggerNumbers"
                            and histo == "pt"):
                        latex.DrawLatex(
                            0.45, 0.3,
                            "Long, 2 < eta < 5, p > 3 GeV, pt > 500 MeV")
                        canvas.SaveAs("forward_long_eta25_effpt.png")

                    # add plot description
                    _root_str = ROOT.TNamed()
                    _root_str.SetTitle(tracker + " " + cut)
                    _root_str.SetName(canvas.GetName() + "__description")
                    _root_str.Write()
                    _root_str.SetName(canvas.GetName().replace(
                        " with ref", "") + "__description")
                    _root_str.Write()

            # calculate ghost rate
            histoBaseName = "Track/" + folder + tracker + "/"
            for histo in ghostHistos:
                trackerDir.cd()
                title = "ghost rate vs " + histo
                canvas = ROOT.TCanvas(title, title)
                ROOT.gPad.SetTicks()
                numeratorName = (histoBaseName +
                                 ghostHistoDict[histo]["variable"] + "_Ghosts")
                denominatorName = (
                    histoBaseName + ghostHistoDict[histo]["variable"] +
                    "_Total")
                print("ghost histo: " + histoBaseName)
                numerator = f.Get(numeratorName)
                denominator = f.Get(denominatorName)
                numerator.Sumw2()
                denominator.Sumw2()

                g_efficiency = ROOT.TGraphAsymmErrors()
                g_efficiency.SetName(title + " ref")
                g_efficiency.Divide(numerator, denominator, "opt")

                xtitle = ghostHistoDict[histo]["xTitle"]
                g_efficiency.GetXaxis().SetTitle(xtitle)
                g_efficiency.GetYaxis().SetTitle("ghost rate")
                g_efficiency.Write()
                g_efficiency.Draw("ap")

                canvas.Write()

                g_efficiency_ref = f_ref.Get(tracker + "/" + title + " ref")
                g_efficiency_ref.SetMarkerStyle(25)
                g_efficiency_ref.SetMarkerSize(2)
                g_efficiency_ref.Draw("psame")

                g_efficiency.SetName(g_efficiency.GetName().replace(
                    " ref", ""))
                canvas.PlaceLegend()
                canvas.SetName(canvas.GetName() + " with ref")
                canvas.Write()

                # add plot description
                _root_str = ROOT.TNamed()
                _root_str.SetTitle(tracker + " " + cut)
                _root_str.SetName(canvas.GetName() + "__description")
                _root_str.Write()
                _root_str.SetName(g_efficiency.GetName() + "__description")
                _root_str.Write()

        outputfile.Write()
        outputfile.Close()
        f.Close()
        self.saveFile("PrCheckerEfficiency", "./efficiency_plots.root")

        # send plot to EOS
        wwwDirEos = os.environ.get("LHCBPR_WWW_EOS")
        if wwwDirEos == None:
            raise Exception(
                "No web dir on EOS defined, will not run extraction")
        else:
            time = (str(startTime).replace(" +0200", "").replace(
                " +0100", "").replace(" ", "_").replace("-", "").replace(
                    ":", ""))
            dirname = "efficiency"
            targetRootEosDir = os.path.join(wwwDirEos, dirname)
            try:
                subprocess.call([
                    "xrdcp",
                    "-f",
                    "forward_long_eta25_effpt.png",
                    targetRootEosDir + "/forward_long_eta25_effpt_" +
                    str(version) + "_" + str(platform) + "_" + time + ".png",
                ])
            except Exception as ex:
                log.warning("Error copying to eos: %s", ex)

            self.saveString(
                "forward_long_eta25_effpt",
                "cern.ch/lhcbpr-hlt/PerfTests/UpgradeVelo/" + dirname +
                "/forward_long_eta25_effpt_" + str(version) + "_" +
                str(platform) + "_" + time + ".png",
                description="link to forward_long_eta25_effpt plot",
                group="prchecker_efficiency_plots",
            )
