import os
from .BaseHandler import BaseHandler


class BooleMoniROOTFileHandler(BaseHandler):
    def __init__(self):
        super(self.__class__, self).__init__()

    def collectResults(self, directory):
        files = [
            "PR-UPG-SpillOver25ns-FT-1000ev-histos.root",
            "Boole-histos.root",
            "PR-Run3-Integration-Spillover-1000ev-histos.root",
            "PR-Run3-Integration-noSpillover-1000ev-histos.root",
            "PR-2024-Integration-Spillover-1000ev-histos.root",
            "PR-2024-Integration-noSpillover-1000ev-histos.root"
        ]

        fileFound = False

        for f in files:
            print("Checking for ", f)
            if os.path.isfile(os.path.join(directory, f)):
                fileFound = True
                self.saveFile("BooleROOTMoniOutput", os.path.join(
                    directory, f))

        if not fileFound:
            raise Exception(
                "Could not locate any supported monitoring histograms ROOT files"
            )
