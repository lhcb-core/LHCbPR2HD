#!/usr/bin/env python

import sys
import collectRunResults
import argparse
from datetime import datetime, timedelta
import copy


def main(args):
    description = """Test handlers: you need to set a directory with job results and a list of handlers"""
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument(
        "-r",
        "--results",
        default=".",
        help=
        "Directory which contains results, default is the current directory",
    )
    parser.add_argument(
        "-l",
        "--list-handlers",
        dest="handlers",
        help="The list of handlers (comma separated.)",
        required=True,
    )
    parser.add_argument(
        "--app-name",
        dest="app_name",
        default="DummyApp",
        help="Application name")
    parser.add_argument(
        "--app-version",
        dest="app_version",
        default="dummy-app.1",
        help="Application version")
    parser.add_argument(
        "--platform",
        dest="job_platform",
        default="x86_64-centos7-gcc62-opt",
        help="Job Platform",
    )
    parser.add_argument(
        "--host",
        dest="job_host",
        default="dummy-host",
        help="Job Machine hostname")
    parser.add_argument(
        "--opt", dest="job_opt", default="dummy-opt", help="Job Opt Name")
    parser.add_argument(
        "--opt-cont",
        dest="job_opt_cont",
        default="dummy-exec-content",
        help="Job Opt Content",
    )

    job_time = datetime.now()
    time_fmt = "%Y-%m-%d %H:%M:%S +0200"
    job_start_str = job_time.strftime(time_fmt)

    parser.add_argument(
        "--start-time",
        dest="job_begin",
        default=job_start_str,
        help='Job start time in "%%Y-%%m-%%d %%H:%%M:%%S +0200", e.g. {0}'.
        format(job_start_str),
    )
    parser.add_argument(
        "--run-time", dest="run_time", default=2, help="Job run time (hr)")

    parser.add_argument(
        "--build_week",
        dest="build_week",
        default=False,
        action="store_true",
        help=
        "Should we build a week of data (1 data point per day) duplicating this test?",
    )

    options = parser.parse_args(args)

    job_time = datetime.strptime(job_start_str, time_fmt)
    job_time = job_time + timedelta(hours=options.run_time)
    job_end_str = job_time.strftime(time_fmt)

    params = ["myapp"]
    params += ["--app-name", str(options.app_name).upper()]
    params += ["--app-version", options.app_version]
    params += ["--app-version-datetime", job_start_str]
    params += ["--exec-name", "dummy-exec"]
    params += ["--exec-content", "dummy-exec-content"]
    params += ["--opt-name", options.job_opt]
    params += ["--opt-content", options.job_opt_cont]
    params += ["-s", job_start_str]
    params += ["-e", job_end_str]
    params += ["-p", options.job_host]
    params += ["-c", options.job_platform]
    params += ["-l", options.handlers]
    params += ["-r", options.results]
    params += ["-u", ""]
    params += ["-m", ""]
    params += ["--debug"]

    if not options.build_week:
        sys.argv = params
        print(sys.argv)
        collectRunResults.main()
    else:
        i = 0
        arg_time = datetime.strptime(job_start_str, time_fmt)
        start_time = arg_time - timedelta(days=7)
        while i < 7:
            i = i + 1
            new_start_time = start_time + timedelta(days=i)
            new_end_time = new_start_time + timedelta(hours=options.run_time)
            new_params = copy.deepcopy(params)
            # Change the app version id so it's unique
            new_params[4] = new_params[4] + "." + str(i)
            # Change the start time
            new_params[16] = new_start_time.strftime(time_fmt)
            # Change the end time
            new_params[18] = new_end_time.strftime(time_fmt)

            sys.argv = new_params
            print(sys.argv)
            collectRunResults.main()


if __name__ == "__main__":
    main(sys.argv[1:])
